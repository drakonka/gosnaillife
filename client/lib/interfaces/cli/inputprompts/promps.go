package inputprompts

import (
	"fmt"
	"github.com/manifoldco/promptui"
	"strings"
)

// PromptForString asks the user to enter a string and returns it.
func PromptForString(label string, validation func(i string) error) (input string, err error) {
	prompt := promptui.Prompt{
		Label:    label,
		Validate: validation,
	}

	input, err = prompt.Run()
	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return input, err
	}
	return input, err
}

// PromptSelection asks the uesr to select from a slice of updates and returns the selected item.
func PromptSelection(label string, items []string) (selection string, err error) {
	prompt := promptui.Select{
		Label: label,
		Items: items,
	}

	_, selection, err = prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return "", err
	}
	return selection, err
}

// PromptSelectionWithTemplate asks the uesr to make a selection with a template.
func PromptSelectionWithTemplate(label string, items interface{}, templates *promptui.SelectTemplates) (idx int, selection string, err error) {
	prompt := promptui.Select{
		Label:     label,
		Items:     items,
		Templates: templates,
	}
	idx, selection, err = prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return -1, "", err
	}
	return idx, selection, err
}

// PromptForEmail asks the user to input an email and returns it.
func PromptForEmail() (e string, err error) {
	emailPrompt := promptui.Prompt{
		Label: "Email",
	}
	e, err = emailPrompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return e, err
	}
	e = strings.TrimSpace(e)
	return e, err
}

// PromptForPass asks the user to input a password and masks the input with '*'s
func PromptForPass() (p string, err error) {
	emailPrompt := promptui.Prompt{
		Label: "Password",
		Mask:  '*',
	}
	p, err = emailPrompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return p, err
	}
	p = strings.TrimSpace(p)
	return p, err
}

// PromptYesNo aks the user to pick a yes or no response.
func PromptYesNo(label string, validation func(i string) error) (input string, err error) {
	prompt := promptui.Prompt{
		Label:     label,
		Validate:  validation,
		IsConfirm: true,
	}

	input, err = prompt.Run()
	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return input, err
	}
	return input, err
}
