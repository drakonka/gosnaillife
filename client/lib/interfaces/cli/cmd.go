package cli

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"strings"
)

// Cmd is a Cobra command and any subcommands of it.
type Cmd struct {
	*cobra.Command
	subcommands map[string]*Cmd
}

// NewCmd creates a new command and adds it to the AllCmds command map.
func NewCmd(cmd *cobra.Command) *Cmd {
	c := &Cmd{cmd, make(map[string]*Cmd)}
	AllCmds[c.Name()] = c
	return c
}

// AddSubCommand adds a subcommand to a main command.
func (c *Cmd) AddSubCommand(sc *Cmd) {
	c.AddCommand(sc.Command)
	c.subcommands[sc.Name()] = sc
}

// TryGetSubCommand attempts to find a subcommand by name.
func (c *Cmd) TryGetSubCommand(name string) (sc *Cmd, err error) {
	name = strings.TrimSpace(name)
	if sc, ok := c.subcommands[name]; ok {
		return sc, err
	}
	msg := fmt.Sprintf("Subcommand %s not found", name)
	err = errors.New(msg)
	return sc, err
}
