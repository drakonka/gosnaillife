package commands

import (
	"bufio"
	"context"
	"fmt"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"io/ioutil"
	"os"
	"os/exec"
	"runtime/debug"
	"sync"
	"testing"
	"time"
)

var mutex *sync.RWMutex

func recovery(t *testing.T) {
	if r := recover(); r != nil {
		t.Errorf("%s: %s", r, debug.Stack())
	}
}

func startServer(ctx context.Context, wg *sync.WaitGroup, procStart chan int) {
	mutex = &sync.RWMutex{}
	subproc = exec.Command("snaillifesrv", "serve", "--frozenWorld", "--test", "--migrations", "/client/lib/interfaces/cli/commands/testdata/migrations")
	subproc.Stdout = os.Stdout
	err := subproc.Start()
	if err != nil {
		fmt.Printf("\nError starting server: %s", err)
		procStart <- procStartFailed
	} else {
		checkProcessStart(ctx, subproc, wg, procStart, 10)
		checkProcessKill(ctx, subproc, wg)
	}
}

func checkProcessStart(ctx context.Context, subproc *exec.Cmd, wg *sync.WaitGroup, psc chan int, attemptsLeft int) {
	for attemptsLeft > 0 {
		fmt.Printf("\ncheckProcessStart attempt %d", attemptsLeft)
		mutex.RLock()
		defer mutex.RUnlock()
		if subproc.Process == nil && attemptsLeft == 0 {
			psc <- procStartFailed
			return
		} else if subproc.Process != nil {
			fmt.Println("\nHouston, we have a process")
			// Sleep for a few seconds to make sure server startup is done.
			time.Sleep(time.Second * 5)
			psc <- procStartDone
			return
		}
		attemptsLeft--
		time.Sleep(time.Second * 1)
	}
}

func checkProcessKill(ctx context.Context, subproc *exec.Cmd, wg *sync.WaitGroup) {
	time.AfterFunc(1*time.Second, func() {
		fmt.Println("Checking for kill")
		select {
		case <-ctx.Done():
			fmt.Println("Killing server process")
			mutex.Lock()
			if subproc != nil && subproc.Process != nil {
				subproc.Process.Signal(os.Interrupt)
			} else {
				fmt.Println("Have we already killed?")
			}
			mutex.Unlock()
			wg.Done()
		default:
			time.Sleep(time.Second * 1)
			checkProcessKill(ctx, subproc, wg)
		}
	})
}

func populateNewTempFile(prefix string, lines ...string) (*testing2.FileScanBundle, error) {
	fsb := testing2.FileScanBundle{}
	tmpDir := os.TempDir()
	file, err := ioutil.TempFile(tmpDir, prefix)
	fsb.File = file
	if err != nil {
		return nil, err
	}
	for _, l := range lines {
		_, err = file.WriteString(l + "\n")
		if err != nil {
			os.Remove(file.Name())
			return nil, err
		}
	}
	file.Seek(0, 0)
	scanner := bufio.NewScanner(file)
	fsb.Scanner = scanner
	return &fsb, nil
}
