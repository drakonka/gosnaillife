package commands

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/inputprompts"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	"strings"
)

var makeStableCobraC = &cobra.Command{
	Use:   "makestable",
	Short: "Start a stable.",
	Long:  `Start your very own snail stable.`,
	Run: func(cmd *cobra.Command, args []string) {
		startStable(nil)
	},
}

func startStable(fsb *testing.FileScanBundle) error {
	if len(env.AuthenticatedUser.AuthHeaders) == 0 {
		msg := notLoggedInMsg
		err := &cliErr{msg}
		error2.HandleErr(err, "")
		return err
	}

	owner, err := findOwner(env.AuthenticatedUser.Id, "Id", "Stables", "FirstName", "LastName")
	if err != nil {
		msg := fmt.Sprintf("Error finding owner: %s", err.Error())
		fmt.Println(msg)
		// We need to prompt the user to create a new owner
		fmt.Println("You need to create a new owner!")
		owner, err = createOwner(fsb)
		if err != nil {
			error2.HandleErr(err, "MakeStable")
			return err
		}
	}
	env.Owner = owner
	// We need to check if the owner already has a stable
	if len(owner.Stables) == 0 {
		// Create a new stable
		stable, err := createStable(fsb)
		if err == nil {
			env.Owner.Stables = append(env.Owner.Stables, stable)
			msg := fmt.Sprintf("Stable %s created!", stable.Name)
			fmt.Println(msg)
			fmt.Println(stable)
		} else {
			error2.HandleErr(err, "MakeStable")
			return err
		}
	} else {
		msg := fmt.Sprintf("You already have a stable named %s", owner.Stables[0].Name)
		fmt.Println(msg)
		return &cliErr{msg}
	}
	return nil
}

func createStable(fsb *testing.FileScanBundle) (*stable.Stable, error) {
	var name, doContinue string
	var err error
	if fsb == nil {
		name, err = inputprompts.PromptForString("Stable Name", nil)
		if err != nil {
			return nil, err
		}

		msg := fmt.Sprintf("Your stable will be known as '%s'. Do you want to continue?", name)
		selection := []string{"Yes", "No", "Cancel"}
		doContinue, _ = inputprompts.PromptSelection(msg, selection)
	} else {
		scanner := fsb.Scanner

		scanner.Scan()
		name = scanner.Text()
		scanner.Scan()
		doContinue = scanner.Text()

		if err := scanner.Err(); err != nil {
			fmt.Println("Owner creation failed: " + err.Error())
			return nil, err
		}
	}
	doContinue = strings.ToUpper(doContinue)
	if doContinue == yesMsg {
		return doCreateStable(name)
	} else if doContinue == noMsg {
		return createStable(fsb)
	}
	return nil, &cliErr{"Operation cancelled by user"}
}

func doCreateStable(name string) (*stable.Stable, error) {
	s := stable.Stable{}
	s.OwnerId = env.Owner.Id
	s.Name = strings.TrimSpace(name)
	fmt.Println("Making stable!")
	surl := viper.GetString("server_url")
	url := fmt.Sprintf("%s/api/stables", surl)
	oReq := restapi.StableReq{
		Stable: s,
	}
	oReq.AuthProvider = viper.GetString("auth_provider")

	j, err := json.Marshal(oReq)
	if err != nil {
		fmt.Printf("\n Error: %s", err)
		return &s, err
	}
	postSpec := string(j)
	ap := viper.GetString("auth_provider")

	res, err := http.PostAuthenticatedAndGetRes(url, postSpec, ap, env.AuthenticatedUser.AuthHeaders)
	if err != nil {
		error2.HandleErr(err, "Make stable [stable creation]")
		return nil, err
	} else if res.StatusCode != 200 {
		msg := fmt.Sprintf("%d - %s", res.StatusCode, res.Status)
		err = &cliErr{msg}
		return nil, err
	}
	// Retrieve created stable
	stableRes, err := restapi.GetStableRes(res)

	var createdStable *stable.Stable
	if len(stableRes.Models()) == 1 {
		createdStable = stableRes.Models()[0].(*stable.Stable)
	}
	return createdStable, err
}
