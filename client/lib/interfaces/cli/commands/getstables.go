package commands

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli"
	stable2 "gitlab.com/drakonka/gosnaillife/common/domain/stable"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	http2 "net/http"
	"strconv"
	"strings"
)

var getStablesCobraC = &cobra.Command{
	Use:   "getstables",
	Short: "Get all stables belonging to an owner.",
	Long:  `Get some information about stables belonging to an owner.`,
	Run: func(cmd *cobra.Command, args []string) {
		getStables(nil, args)
	},
}

func initGetStablesCobraCParmas() {
	getStablesCobraC.Flags().StringVarP(&getStablesCParams.ownerId, "ownerid", "o", "", "ID of owner whose stables to retrieve")
	getStablesCobraC.Flags().StringSliceVarP(&getStablesCParams.fieldsToGet, "fields", "f", []string{}, "Fields to retrieve (all by default)")
	cli.NewCmd(getStablesCobraC)

}

type getStablesCobraCParams struct {
	ownerId     string
	fieldsToGet []string
}

var getStablesCParams = getStablesCobraCParams{}

func getStables(fsb *testing.FileScanBundle, args []string) error {
	var oId = getStablesCParams.ownerId
	var fields = getStablesCParams.fieldsToGet

	if oId == "" {
		msg := "You must specify either the --ownerid parameter to find an owner."
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}
	var stables []*stable2.Stable
	var err error
	if oId != "" {
		id, _ := strconv.Atoi(oId)
		stables, err = findStables(id, fields...)
	} else {
		// NOT IMPLEMENTED YET
		panic("findOwner by user email is not yet implemented")
	}
	if err != nil {
		error2.HandleErr(err, "")
		return &cliErr{msg: err.Error()}
	}
	if len(stables) == 0 {
		msg := "Could not find stables."
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}
	fmt.Printf("\n Found %d!", len(stables))
	for _, stable := range stables {
		st := structs.New(stable)
		stableMap := st.Map()

		if len(fields) == 0 {
			for k, v := range stableMap {
				fmt.Printf("\n%s - %v", k, v)
			}
		} else {
			for _, n := range fields {
				val := stableMap[n]
				fmt.Printf("\n %s - %v", n, val)
			}
		}
	}
	getStablesCParams = getStablesCobraCParams{}
	return nil
}

func findStables(ownerId int, fields ...string) ([]*stable2.Stable, error) {
	surl := viper.GetString("server_url")
	f := strings.Join(fields, ",")

	url := fmt.Sprintf("%s/api/owners/%d/stables?fields=%s", surl, ownerId, f)
	fmt.Println(url)
	ap := viper.GetString("auth_provider")

	res, err := http.GetAuthenticatedAndGetRes(url, ap, env.AuthenticatedUser.AuthHeaders)
	stables, err := processStableResponse(err, res)
	if err == nil {
		if len(fields) == 0 || util.StrIndexOf(fields, "jars") > -1 {
			for _, stable := range stables {
				jars, _ := findJarsByStableId(stable.Id)
				stable.Jars = jars
			}
		}
	}
	return stables, err
}

func processStableResponse(e error, res *http2.Response) (stables []*stable2.Stable, err error) {
	if e != nil {
		error2.HandleErr(err, "getStable")
		return nil, e
	}

	stableRes, err := restapi.GetStableRes(res)
	if err == nil && res.StatusCode == http2.StatusOK {
		for _, m := range stableRes.Models() {
			stable := m.(*stable2.Stable)
			stables = append(stables, stable)
		}
		return stables, err
	} else if err != nil {
		err = &cliErr{fmt.Sprintf("No stable was found: %v", err)}
	}
	error2.HandleErr(err, "getStables")
	return nil, err
}
