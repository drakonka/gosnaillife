package commands

import (
	"encoding/json"
	"fmt"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/inputprompts"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/output"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	"strconv"
)

var findSnailCobraC = &cobra.Command{
	Use:   "findsnail",
	Short: "Find a snail.",
	Long:  `Go snail hunting in your region..`,
	Run: func(cmd *cobra.Command, args []string) {
		err := findSnail(nil)
		if err != nil {
			fmt.Println(err)
		}
	},
}

func findSnail(fsb *testing.FileScanBundle) error {
	if len(env.AuthenticatedUser.AuthHeaders) == 0 {
		msg := notLoggedInMsg
		err := &cliErr{msg}
		error2.HandleErr(err, "")
		return err
	}
	if env.Owner == nil {
		err := findOrCreateOwner(fsb)
		if err != nil {
			return err
		}
	}
	err := findOrCreateStable(fsb)
	if err != nil {
		return err
	}

	err = findOrCreateJar(fsb)
	if err != nil {
		return err
	}

	newSnail, err := createSnail(fsb)
	if newSnail != nil && err == nil {
		output.PrintCompleteSnailStats(*newSnail)

		jarSelection := []*jar.Jar{}
		for _, jar := range env.Owner.Stables[0].Jars {
			jarSelection = append(jarSelection, jar)
		}
		template := promptui.SelectTemplates{
			Active:   `{{ .Id | green | bold }} {{ .Name | green | bold }}`,
			Inactive: `{{ .Id }} {{ .Name }}`,
			Selected: `{{ ">" | green | bold }} {{ .Name | green | bold}}`,
			Details: `Details:
{{ .Size }}`,
		}
		jarSelection = append(jarSelection, nil)
		var idx int
		if fsb == nil {
			idx, _, _ = inputprompts.PromptSelectionWithTemplate("Select jar", jarSelection, &template)
		} else {
			scanner := fsb.Scanner
			scanner.Scan()
			idx, _ = strconv.Atoi(scanner.Text())
			/*jarIdx, _ := strconv.Atoi(scanner.Text())
			for i, jar := range jarSelection {
				if jar.Id == jarIdx {
					idx = i
					break
				}
			} */
		}
		if idx == -1 {
			return nil
		}

		jar := jarSelection[idx]
		// which jar should the snail be saved to; to start with put it in first jar
		newSnail.PutInJar(jar.Id, jar.Width, jar.Depth)
		newSnail.CurrentJarId = jar.Id
		fieldsToUpdate := map[string]string{
			"ParentJarId":  fmt.Sprintf("%d", jar.Id),
			"CurrentJarId": fmt.Sprintf("%d", jar.Id),
			"PosX":         fmt.Sprintf("%d", newSnail.PosX),
			"PosY":         fmt.Sprintf("%d", newSnail.PosY),
			"PosZ":         fmt.Sprintf("%d", newSnail.PosZ),
		}
		var finalSnail *snail.Snail
		finalSnail, err = doUpdate(newSnail, fieldsToUpdate, fsb)
		if err != nil {
			fmt.Printf("\nError during snail creation: %v", err)
			return err
		}
		print("Snail placed in jar!")
		output.PrintCompleteSnailStats(*finalSnail)
	}

	return err
}

func createSnail(fsb *testing.FileScanBundle) (*snail.Snail, error) {
	fmt.Println("Making Snail!")
	// create owner!nil
	surl := viper.GetString("server_url")
	url := fmt.Sprintf("%s/api/snails", surl)
	oReq := restapi.SnailReq{}
	oReq.AuthProvider = viper.GetString("auth_provider")

	j, err := json.Marshal(oReq)
	if err != nil {
		fmt.Printf("\n Error: %s", err)
		return nil, err
	}
	postSpec := string(j)
	ap := viper.GetString("auth_provider")

	res, err := http.PostAuthenticatedAndGetRes(url, postSpec, ap, env.AuthenticatedUser.AuthHeaders)
	if err != nil {
		error2.HandleErr(err, "Make snail")
		return nil, err
	}

	snailRes, err := restapi.GetSnailRes(res)

	if snailRes.HttpStatus().StatusCode != 200 {
		msg := fmt.Sprintf("%d - %s", snailRes.HttpStatus().StatusCode, snailRes.HttpStatus().Message)
		err = &cliErr{msg}
		return nil, err
	}

	var createdSnail *snail.Snail
	if len(snailRes.Models()) == 1 {
		createdSnail = snailRes.Models()[0].(*snail.Snail)
	}
	return createdSnail, err
}
