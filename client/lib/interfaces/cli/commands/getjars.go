package commands

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/common/util/testing"
	http2 "net/http"
	"strconv"
	"strings"
)

var getJarsCobraC = &cobra.Command{
	Use:   "getjars",
	Short: "Get all jars belonging to an owner (or stable).",
	Long:  `Get some information about jars belonging to an owner or stable.`,
	Run: func(cmd *cobra.Command, args []string) {
		getJars(nil)
	},
}

func initGetJarsCobraCParams() {
	getJarsCobraC.Flags().StringVarP(&getJarsCParams.ownerId, "ownerid", "o", "", "ID of owner whose jars to retrieve")
	getJarsCobraC.Flags().StringVarP(&getJarsCParams.stableId, "stableid", "s", "", "ID of stable whose jars to retrieve")
	getJarsCobraC.Flags().StringSliceVarP(&getJarsCParams.fieldsToGet, "fields", "f", []string{}, "Fields to retrieve (all by default)")
	cli.NewCmd(getJarsCobraC)

}

type getJarsCobraCParams struct {
	ownerId     string
	stableId    string
	fieldsToGet []string
}

var getJarsCParams = getJarsCobraCParams{}

func getJars(fsb *testing.FileScanBundle) error {
	var oId = getJarsCParams.ownerId
	var sId = getJarsCParams.stableId
	var fields = getJarsCParams.fieldsToGet

	if oId == "" && sId == "" {
		msg := "You must specify either the --ownerid or --stableid parameters to find jars."
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}

	var jars []*jar.Jar
	var err error
	if oId != "" {
		jars, err = findJarsByOwnerId(oId, fields...)
	} else if sId != "" {
		id, _ := strconv.Atoi(sId)
		jars, err = findJarsByStableId(id, fields...)
	}
	if err != nil {
		error2.HandleErr(err, "")
		return &cliErr{msg: err.Error()}
	}
	if len(jars) == 0 {
		msg := "Could not find any jars."
		fmt.Println(msg)
		return &cliErr{msg: msg}
	}
	printJarInfo(jars, fields)
	return nil
}

func printJarInfo(jars []*jar.Jar, fields []string) {
	fmt.Printf("\n Found %d jars!", len(jars))
	for _, j := range jars {
		js := structs.New(j)
		jarMap := js.Map()

		if len(fields) == 0 {
			for k, v := range jarMap {
				fmt.Printf("\n%s - %v", k, v)
			}
		} else {
			for _, n := range fields {
				val := jarMap[n]
				fmt.Printf("\n %s - %v", n, val)
			}
		}
	}
}

func findJarsByOwnerId(ownerId string, fields ...string) ([]*jar.Jar, error) {
	surl := viper.GetString("server_url")
	f := strings.Join(fields, ",")

	url := fmt.Sprintf("%s/api/owners/%s/jars?fields=%s", surl, ownerId, f)
	fmt.Println(url)
	ap := viper.GetString("auth_provider")

	res, err := http.GetAuthenticatedAndGetRes(url, ap, env.AuthenticatedUser.AuthHeaders)
	return processJarResponse(err, res)
}

func findJarsByStableId(stableId int, fields ...string) ([]*jar.Jar, error) {
	surl := viper.GetString("server_url")
	f := strings.Join(fields, ",")

	url := fmt.Sprintf("%s/api/stables/%d/jars?fields=%s", surl, stableId, f)
	fmt.Println(url)
	ap := viper.GetString("auth_provider")

	res, err := http.GetAuthenticatedAndGetRes(url, ap, env.AuthenticatedUser.AuthHeaders)
	jars, err := processJarResponse(err, res)
	if err == nil {
		if len(fields) == 0 || util.StrIndexOf(fields, "snails") > -1 {
			for _, j := range jars {
				common.Log.Debugf("Finding snails for jar %d", j.Id)
				snails, _ := findSnailsByJarId(j.Id)
				j.Snails = snails
			}
		}
	}
	return jars, err
}

func processJarResponse(e error, res *http2.Response) (jars []*jar.Jar, err error) {
	if e != nil {
		error2.HandleErr(err, "getJars")
		return nil, e
	}

	jarRes, err := restapi.GetJarRes(res)
	if err == nil && res.StatusCode == http2.StatusOK {
		for _, m := range jarRes.Models() {
			jar := m.(*jar.Jar)
			jars = append(jars, jar)
		}
		return jars, err
	} else if err != nil {
		err = &cliErr{fmt.Sprintf("No jar was found: %v", err)}
	}
	error2.HandleErr(err, "getJars")
	return nil, err
}
