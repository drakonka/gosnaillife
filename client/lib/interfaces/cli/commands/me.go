package commands

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/client/lib/interfaces/cli/output"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
)

var meCobraC = &cobra.Command{
	Use:   "me",
	Short: "User info.",
	Long:  `View information about yourself.`,
	Run: func(cmd *cobra.Command, args []string) {
		viewMe()
	},
}

func viewMe() error {
	if len(env.AuthenticatedUser.AuthHeaders) == 0 {
		msg := notLoggedInMsg
		fmt.Println(msg)
		return &cliErr{msg}
	}
	// Print info about logged in user

	fmt.Println("\n--- USER ---")
	fmt.Printf("\nUser ID: %s", env.AuthenticatedUser.Id)
	fmt.Printf("\nUser Email: %s", env.AuthenticatedUser.Email)
	if env.Owner == nil {
		return nil
	}
	fmt.Println("\n\n --- OWNER ---")
	fmt.Printf("\nOwner %s %s", env.Owner.FirstName, env.Owner.LastName)
	fmt.Printf("\nOwner ID: %d", env.Owner.Id)
	fmt.Printf("\nOwner Description: %s", env.Owner.Description)
	fmt.Printf("\nOwner SEK: %d", env.Owner.Sek)
	if env.Owner.Stables == nil || len(env.Owner.Stables) == 0 {
		fmt.Println("\n\nStables: None")
		return nil
	}
	printStableInfo()
	// Print info about owner
	return nil
}

func printStableInfo() {
	fmt.Println("\n\n --- STABLES ---")
	for _, s := range env.Owner.Stables {
		fmt.Printf("Stable Name: %s (%d)", s.Name, s.Id)
		fmt.Printf("\nStable Brand: %s", s.Brand)
		fmt.Printf("\nStable Description: %s", s.Description)
		printStableJarInfo(s)
	}
}

func printStableJarInfo(s *stable.Stable) {
	fmt.Println("\n\n --- JARS ---")

	for _, j := range s.Jars {
		fmt.Printf("\nJar Name: %s (%d)", j.Name, j.Id)
		fmt.Printf("\nJar Description: %s", j.Description)
		fmt.Printf("\nJar Dimensions: %d x %d x %d (%s)", j.Width, j.Depth, j.Height, j.SizeLabel)
		fmt.Printf("%s", j.Ascii)
		printSnailInfo(j)
	}
}

func printSnailInfo(j *jar.Jar) {
	fmt.Println("\n\n --- SNAILS ---")
	for _, s := range j.Snails {
		output.PrintCompleteSnailStats(s)
	}
}
