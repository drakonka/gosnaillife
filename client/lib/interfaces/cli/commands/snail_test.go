package commands

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/common/util"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"os"
	"reflect"
	"strconv"
	"testing"
)

func TestFindSnail(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		name       string
		email      string
		pass       string
		stableName string
		jarName    string
		jarSizeIdx string
		jarConfRes string
		jarIdx     string
		jarId      int
		err        error
	}{
		{
			name:   "CreateSnail",
			email:  "user2WithOwnerStableJar@test.com",
			pass:   "testpass",
			jarIdx: "0",
			jarId:  6,
		},
		{
			name:       "CreateSnailWithoutJarAndDeclineJarConf",
			email:      "user2WithOwnerStable@test.com",
			pass:       "testpass",
			jarName:    "Testidy Test",
			jarSizeIdx: "0",
			jarConfRes: "No",
			jarIdx:     "0",
			jarId:      0,
			err:        new(cliErr),
		},
		{
			name:       "CreateSnailWithoutJar",
			email:      "user2WithOwnerStable@test.com",
			pass:       "testpass",
			jarName:    "Testidy Test",
			jarSizeIdx: "0",
			jarConfRes: "Yes",
			jarId:      -1,
			jarIdx:     "0",
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestFindSnail Test Case %s.", tc.name)
		t.Run(tc.name, func(t *testing.T) {
			logoutUser()
			var stdInFile []string

			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.email)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.pass)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.stableName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.jarName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.jarSizeIdx)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.jarConfRes)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.jarIdx)
			fsb, err := populateNewTempFile("stdin", stdInFile...)
			if err != nil {
				t.Fatal(err)
				return
			}
			// Prep by logging in
			err = loginUser(fsb)
			if err != nil {
				t.Fatal(err)
				fsb.File.Close()
				os.Remove(fsb.File.Name())
				return
			}

			err = findSnail(fsb)
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("SnailTest", tc.name, testerr))
			} else if tc.err == nil {
				// Check that a snail was made
				id := tc.jarId
				if id == -1 {
					id = env.Owner.Stables[0].Jars[0].Id
				}
				snails, err := findSnailsByJarId(id, "id")
				if err != nil {
					t.Error(testing2.TCFailedMsg("SnailTest", tc.name, err))
				} else if len(snails) != 1 {
					t.Error(testing2.TCFailedMsg("SnailTest", tc.name, fmt.Sprintf("expected 1 snail, got %d", len(snails))))
				}
			}

			fsb.File.Close()
			os.Remove(fsb.File.Name())

			env.AuthenticatedUser.AuthHeaders = nil
		})

	}
}

func TestUpdateSnail(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname        string
		email         string
		pass          string
		snailIdx      int
		fieldToUpdate string
		newFieldVal   string
		err           error
	}{
		{
			tcname:        "UpdateSnailSuccess",
			email:         "user3WithOwnerStableJarSnail@test.com",
			pass:          "testpass",
			snailIdx:      0,
			fieldToUpdate: "Name",
			newFieldVal:   "newname",
			err:           nil,
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestUpdateSnail Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			logoutUser()
			var stdInFile []string

			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.email)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.pass)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, strconv.Itoa(tc.snailIdx))
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.fieldToUpdate)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.newFieldVal)

			fsb, err := populateNewTempFile("stdin", stdInFile...)
			if err != nil {
				t.Fatal(err)
				return
			}
			// Prep by logging in
			err = loginUser(fsb)
			if err != nil {
				t.Fatal(err)
				fsb.File.Close()
				os.Remove(fsb.File.Name())
				return
			}
			snail, err := updateSnail(fsb)
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("SnailTest", tc.tcname, testerr))
			} else {
				val := reflect.ValueOf(*snail)
				field := val.FieldByName(tc.fieldToUpdate)
				field_val := field.String()
				if tc.newFieldVal != field_val {
					ctx := fmt.Sprintf("expected field %s to be %s, got %s", tc.fieldToUpdate, tc.newFieldVal, field_val)
					t.Error(testing2.TCFailedMsg("SnailTest", tc.tcname, ctx))

				}
			}

			fsb.File.Close()
			os.Remove(fsb.File.Name())

			env.AuthenticatedUser.AuthHeaders = nil
		})
	}
}
