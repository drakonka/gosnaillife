package commands

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/common/util"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"os"
	"strconv"
	"testing"
)

func TestGetJars(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname      string
		email       string
		pass        string
		ownerId     string
		stableId    string
		fieldsToGet []string
		wantedErr   error
	}{
		{
			tcname:      "GetOwnJarsWithAllFieldsByOwnerId",
			email:       "user1WithOwnerStableJar@test.com",
			pass:        "testpass",
			ownerId:     "2",
			fieldsToGet: []string{},
			wantedErr:   nil,
		},
		{
			tcname:      "GetOwnJarsWithNameByOwnerId",
			email:       "user1WithOwnerStableJar@test.com",
			pass:        "testpass",
			ownerId:     "2",
			fieldsToGet: []string{"Name"},
			wantedErr:   nil,
		},
		{
			tcname:      "JarsNotFoundByOwnerId",
			email:       "user1WithOwnerStableJar@test.com",
			pass:        "testpass",
			ownerId:     "7",
			fieldsToGet: []string{},
			wantedErr:   new(cliErr),
		},
		{
			tcname:      "GetJarsWithAllFieldsByStableId",
			email:       "user1@test.com",
			pass:        "testpass",
			stableId:    "6",
			fieldsToGet: []string{},
			wantedErr:   nil,
		},
		{
			tcname:      "GetJarsWithNameByStableId",
			email:       "user1@test.com",
			pass:        "testpass",
			stableId:    "6",
			fieldsToGet: []string{"Name"},
			wantedErr:   nil,
		},
		{
			tcname:      "JarsNotFoundByStableId",
			email:       "user1@test.com",
			pass:        "testpass",
			stableId:    "9233",
			fieldsToGet: []string{},
			wantedErr:   new(cliErr),
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestGetJarsByOwnerId Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			logoutUser()
			getJarsCParams = getJarsCobraCParams{}
			var stdInFile []string

			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.email)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.pass)

			fsb, err := populateNewTempFile("stdin", stdInFile...)
			if err != nil {
				t.Fatal(err)
				return
			}

			if len(tc.ownerId) > 0 {
				getJarsCParams.ownerId = tc.ownerId
			}
			if len(tc.stableId) > 0 {
				getJarsCParams.stableId = tc.stableId
			}
			getJarsCParams.fieldsToGet = tc.fieldsToGet
			// Prep by logging in
			err = loginUser(fsb)
			if err != nil {
				t.Fatal(err)
				fsb.File.Close()
				os.Remove(fsb.File.Name())
				return
			}
			err = getJars(fsb)
			testerr := testing2.TestErrorType(tc.wantedErr, err)
			if testerr != nil {
				t.Errorf("%s failed - %v", tc.tcname, testerr)
			}
		})
	}
}

func TestMakeJar(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname         string
		email          string
		pass           string
		jarName        string
		jarIdx         int
		ownerFirstName string
		ownerLastName  string
		ownerConfRes   string
		stableName     string
		stableConfRes  string
		jarConfRes     string
		err            error
	}{
		{
			tcname:        "NeedNewStableMake",
			email:         "user2WithOwner@test.com",
			pass:          "testpass",
			stableName:    "New Stable",
			stableConfRes: "Yes",
			jarName:       "Cool Stable",
			jarIdx:        0,
			jarConfRes:    "Yes",
			err:           nil,
		},
		{
			tcname:         "NeedNewStableAndOwnerMake",
			email:          "user3@test.com",
			pass:           "testpass",
			ownerFirstName: "Loki",
			ownerLastName:  "Thorshammer",
			ownerConfRes:   "Yes",
			stableName:     "Smitherson Snails",
			stableConfRes:  "Yes",
			jarName:        "Loki's Jar",
			jarIdx:         0,
			jarConfRes:     "Yes",
			err:            nil,
		},
		{
			tcname:     "MakeJar",
			email:      "user2WithOwnerStable@test.com",
			pass:       "testpass",
			jarName:    "Cool Jar",
			jarIdx:     1,
			jarConfRes: "No",
			err:        new(cliErr),
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestMakeJar Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			logoutUser()
			var stdInFile []string

			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.email)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.pass)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerFirstName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerLastName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.ownerConfRes)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.stableName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.stableConfRes)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.jarName)
			stdInFile = util.AppendIfNotWhitespace(stdInFile, strconv.Itoa(tc.jarIdx))
			stdInFile = util.AppendIfNotWhitespace(stdInFile, tc.jarConfRes)

			fsb, err := populateNewTempFile("stdin", stdInFile...)
			if err != nil {
				t.Fatal(err)
				return
			}
			// Prep by logging in
			err = loginUser(fsb)
			if err != nil {
				t.Fatal(err)
				fsb.File.Close()
				os.Remove(fsb.File.Name())
				return
			}
			err = makeJar(fsb)
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Errorf("%s failed - %v", tc.tcname, testerr)
			}

			fsb.File.Close()
			os.Remove(fsb.File.Name())
			env.AuthenticatedUser.AuthHeaders = nil
		})
	}
}
