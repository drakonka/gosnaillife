package commands

import (
	"context"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure"
	"gitlab.com/drakonka/gosnaillife/client/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"os"
	"os/exec"
	"sync"
	"testing"
	"time"
)

const (
	procStartDone = iota
	procStartFailed
)

var subproc *exec.Cmd

func TestMain(m *testing.M) {
	ret := 1
	defer func() {
		os.Exit(ret)
	}()
	defer testing2.TimeElapsed("Command Tests", testing2.LenientThreshold)()

	fmt.Println("Starting command tests")
	var mainWg sync.WaitGroup
	infrastructure.Init()
	ctx, cancel := context.WithCancel(context.Background())

	procStart := make(chan int)
	go startServer(ctx, &mainWg, procStart)

	attemptsLeft := 10
	var ps int

WAIT:
	for attemptsLeft > 0 {
		fmt.Printf("\nWaiting for server start. Attempts left: %d", attemptsLeft)
		select {
		case ps = <-procStart:
			break WAIT
		default:
			attemptsLeft--
			time.Sleep(time.Second * 1)
		}

	}

	if ps == procStartDone {
		time.Sleep(time.Second * 5)
		ret = m.Run()
		mainWg.Add(1)
		fmt.Println("\nTests done, calling cancel")
		cancel()
		mainWg.Wait()
	} else {
		cancel()
	}
}

func TestLogin(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname string
		email  string
		pass   string
		err    error
	}{
		{
			tcname: "SuccessfulLogin",
			email:  "paper@tigers.com",
			pass:   "testpass",
			err:    nil,
		},
		{
			tcname: "BadPasswordLogin",
			email:  "paper@tigers.com",
			pass:   "wat",
			err:    new(cliErr),
		},
		{
			tcname: "BadUsernameLogin",
			email:  "wat@test.com",
			pass:   "testpass",
			err:    new(cliErr),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.tcname, func(t *testing.T) {

			fsb, err := populateNewTempFile("stdin", tc.email, tc.pass)
			if err != nil {
				t.Fatal(err)
				return
			}
			err = loginUser(fsb)
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("CommandsTest", tc.tcname, testerr))
			}
			env.AuthenticatedUser.AuthHeaders = nil
			fsb.File.Close()
			os.Remove(fsb.File.Name())
		})
	}
}

func TestRegister(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname string
		email  string
		pass   string
		err    error
	}{
		{
			tcname: "SuccessfulRegistration",
			email:  "registertest@test.com",
			pass:   "r3gist3red",
			err:    nil,
		},
		{
			tcname: "BadEmailRegistration",
			email:  "badtest",
			pass:   "wat",
			err:    new(validator.ValidationError),
		},
		{
			tcname: "DuplicateRegistration",
			email:  "registertest@test.com",
			pass:   "registeringagain",
			err:    new(cliErr),
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestLogin Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			fsb, err := populateNewTempFile("stdin", tc.email, tc.pass)
			if err != nil {
				t.Fatal(err)
				return
			}
			err = registerUser(fsb)
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("CommandsTest", tc.tcname, testerr))

			}
			fsb.File.Close()
			os.Remove(fsb.File.Name())
		})
	}
}

func TestGetAuthStatus(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname     string
		email      string
		pass       string
		isLoggedIn bool
	}{
		{
			tcname:     "IsAuthenticated",
			email:      "user1@test.com",
			pass:       "testpass",
			isLoggedIn: true,
		},
		{
			tcname:     "IsNotAuthenticated",
			email:      "badtest@test.com",
			pass:       "wat",
			isLoggedIn: false,
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestGetAuthStatus Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			doLogin(tc.email, tc.pass)
			isLoggedIn := getAuthStatus()
			if tc.isLoggedIn != isLoggedIn {
				t.Error(testing2.TCFailedMsg("CommandsTest", tc.tcname, fmt.Sprintf("expected login %t, got %t", tc.isLoggedIn, isLoggedIn)))

			}
			env.AuthenticatedUser.AuthHeaders = nil
		})
	}
}

func TestLogout(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname     string
		loginFirst bool
		err        error
	}{
		{
			tcname:     "LogoutSuccess",
			loginFirst: true,
			err:        nil,
		},
		{
			tcname:     "LogoutNotLoggedin",
			loginFirst: false,
			err:        new(cliErr),
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestLogout Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			var err error
			if tc.loginFirst {
				var fsb *testing2.FileScanBundle
				fsb, err = populateNewTempFile("stdin", "user1@test.com", "testpass")
				if err != nil {
					t.Fatal(err)
					return
				}
				err = loginUser(fsb)
				if err != nil {
					t.Fatal(err)
					fsb.File.Close()
					os.Remove(fsb.File.Name())
					return
				}
				fsb.File.Close()
				os.Remove(fsb.File.Name())
			}
			err = logoutUser()
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Errorf("%s failed - %v", tc.tcname, testerr)
			}
			env.AuthenticatedUser.AuthHeaders = nil
		})
	}
}

func TestMe(t *testing.T) {
	defer recovery(t)

	testCases := []struct {
		tcname string
		email  string
		pass   string
		err    error
	}{
		{
			tcname: "LegitUser",
			email:  "user1@test.com",
			pass:   "testpass",
			err:    nil,
		},
		{
			tcname: "UserWithOwnerStableJar",
			email:  "user2WithOwnerStableJar@test.com",
			pass:   "testpass",
			err:    nil,
		},
		{
			tcname: "UserWithOwnerstableJarSnail",
			email:  "user3WithOwnerStableJarSnail@test.com",
			pass:   "testpass",
			err:    nil,
		},
		{
			tcname: "IsNotAuthenticated",
			err:    new(cliErr),
		},
	}
	for _, tc := range testCases {
		fmt.Printf("Running TestLogout Test Case %s.", tc.tcname)
		t.Run(tc.tcname, func(t *testing.T) {
			var err error
			if tc.email != "" && tc.pass != "" {
				var fsb *testing2.FileScanBundle
				fsb, err = populateNewTempFile("stdin", tc.email, tc.pass)
				if err != nil {
					t.Fatal(err)
				}
				loginUser(fsb)
			}
			err = viewMe()
			testerr := testing2.TestErrorType(tc.err, err)
			if testerr != nil {
				t.Error(testing2.TCFailedMsg("CommandsTest", tc.tcname, testerr))
			}
			env.AuthenticatedUser.AuthHeaders = nil
		})
	}
}
