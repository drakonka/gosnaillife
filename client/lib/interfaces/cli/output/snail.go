package output

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
)

var doNotPrint = []string{"Tag", "Anonymous"}

func PrintCompleteSnailStats(snail snail.Snail) {
	heading := fmt.Sprintf("--- %s (%d) ---", snail.Name, snail.Id)
	fmt.Println("")
	for i := 0; i < len(heading); i++ {
		fmt.Printf("-")
	}
	fmt.Println("")
	fmt.Println(heading)
	for i := 0; i < len(heading); i++ {
		fmt.Printf("-")
	}
	fmt.Printf("\nAge: %f days", snail.AgeDays())
	fmt.Printf("\nCurrent Jar: %d", snail.CurrentJarId)
	fmt.Printf("\nParent Jar: %d", snail.ParentJarId)

	// Organs may not exist or may not be loaded
	if len(snail.Organs) > 0 {
		fmt.Printf("\n\n* Organs")
		for _, organ := range snail.Organs {
			fmt.Printf("\n\n  OrganTypeId: %s", organ.OrganTypeId)
			fmt.Printf("\n  MaxEfficiency: %d", organ.MaxEfficiency)
			fmt.Printf("\n  CurrentEfficiencyPerc: %d", organ.CurrentEfficiencyPerc)
			fmt.Printf("\n  WeightMg: %f", organ.WeightMg)
		}
	}
}
