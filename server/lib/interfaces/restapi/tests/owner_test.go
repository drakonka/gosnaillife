package tests

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/go-errors/errors"
	"github.com/golang/mock/gomock"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	owner2 "gitlab.com/drakonka/gosnaillife/common/domain/owner"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	restapi2 "gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	owner3 "gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/owner"
	"io"
	http3 "net/http"
	"testing"
)

func TestCreateOwner(t *testing.T) {

	testCases := []struct {
		tcname             string
		firstname          string
		lastname           string
		expectedHttpstatus http.HttpStatus
		authReturnErr      error
		idToReturn         int64
	}{
		{"SuccessfulCreation",
			"Percy",
			"Jackson",
			http.HttpStatus{StatusCode: 200},
			nil,
			1,
		},
		{
			"FailedAuthCreation",
			"test@test.com",
			"testpass",
			http.HttpStatus{StatusCode: http3.StatusBadRequest, Message: "Unauthorized"},
			errors.New("Unauthorized yo"),
			0,
		},
		{
			"FailedOtherCreation",
			"test@test.com",
			"testpass",
			http.HttpStatus{StatusCode: http3.StatusUnauthorized, Message: "Unauthorized"},
			nil,
			0,
		},
	}
	for _, tc := range testCases {
		fmt.Printf("Running TestCreateOwner Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			mockProvider, mockDb := setMockProviderAndMockDb(ctrl, app)

			mockProvider.EXPECT().Authenticate(gomock.AssignableToTypeOf(&http3.Request{})).Return(tc.authReturnErr)
			mockDb.EXPECT().Insert(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(make(map[string]string))).Return(tc.idToReturn, nil)

			// Create owner request
			owner := owner2.Owner{
				FirstName: tc.firstname,
				LastName:  tc.lastname,
			}
			ownerReq := restapi2.OwnerReq{
				Owner: owner,
			}
			ownerReq.AuthProvider = "MockProvider"
			res, err := makeRequest(&ownerReq, t, func(writer http3.ResponseWriter, request *http3.Request) {
				owner3.CreateOwner(writer, request, app)
			})
			if err != nil {
				t.Fatal("Could not make request: " + err.Error())
				return
			}
			fmt.Printf("Owner res: %v", res)
			insertedOwners, err := restapi2.GetOwnersFromOwnerRes(res.Result())
			var insertedOwner *owner2.Owner
			if len(insertedOwners) == 1 {
				insertedOwner = &insertedOwners[0]
			}
			if tc.idToReturn > 0 {
				if err != nil {
					t.Errorf("Failed %s: %v", tc.tcname, err)
				} else if insertedOwner.Id != int(tc.idToReturn) {
					t.Errorf("Failed %s: Owners don't match!", tc.tcname)
				}
			} else if tc.idToReturn <= 0 && insertedOwner != nil {
				t.Errorf("Failed %s: Expected invalid owner, but got ID %d", tc.tcname, int(tc.idToReturn))
			}
		})
	}
}

func TestUpdateOwner(t *testing.T) {
	testCases := []struct {
		tcname             string
		ownerId            int
		fieldsToUpdate     map[string]string
		expectedStatusCode int
		errToReturn        error
	}{
		{
			"SuccessfulUpdate",
			2,
			map[string]string{"firstname": "newfirstname", "lastname": "newlastname"},
			http3.StatusOK,
			nil,
		},
		{
			"FailedValidationUpdate",
			2,
			map[string]string{"firstname": "newfirstname", "lastname": "a"},
			http3.StatusBadRequest,
			nil,
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestUpdateOwner Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			mockProvider, mockDb := setMockProviderAndMockDb(ctrl, app)

			mockProvider.EXPECT().Authenticate(gomock.AssignableToTypeOf(&http3.Request{})).Return(nil)
			mockDb.EXPECT().Update(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(make(map[string]string))).Return(tc.errToReturn)

			originalOwner := owner2.Owner{Id: tc.ownerId}
			s := structs.New(originalOwner)
			s.TagName = "db"

			ownerMap := s.Map()

			var rowRes databases.RowResult
			rowRes = ownerMap
			mockDb.EXPECT().GetRow(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf([]string{})).Return(rowRes, nil).MaxTimes(2)

			mockDb.EXPECT().GetRows(
				gomock.Any(),
				gomock.Any(),
				gomock.Any(),
				gomock.Any())
			ownerReq := restapi2.OwnerReq{}
			ownerReq.FieldsToUpdate = tc.fieldsToUpdate
			ownerReq.AuthProvider = "MockProvider"

			res, err := makeRequest(&ownerReq, t, func(writer http3.ResponseWriter, request *http3.Request) {
				owner3.Update(writer, request, app)
			})
			if err != nil {
				t.Fatal("Could not make request: " + err.Error())
				return
			}
			ownerRes, err := restapi2.GetOwnerRes(res.Result())
			if err != nil {
				t.Errorf("Error: %s", err.Error())
			} else if ownerRes.HttpStatus().StatusCode != tc.expectedStatusCode {
				t.Errorf("Unexpected status code; expected %d, got %d: %s", tc.expectedStatusCode, ownerRes.HttpStatus().StatusCode, ownerRes.HttpStatus().Message)
			}
		})
	}
}

func TestGetStables(t *testing.T) {

	testCases := []struct {
		tcname        string
		authreturnerr error
		want          []databases.RowResult
	}{
		{"StablesFound",
			nil,
			[]databases.RowResult{
				{"Name": "The One"},
			},
		},
		{
			"StablesNotFound",
			errors.New("Invalid scope"),
			[]databases.RowResult{},
		},
	}

	url := "http://localhost:42039/api/owners/1/stables?fields=name"

	for _, tc := range testCases {
		fmt.Printf("Running TestGetStables - %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			ctrl := gomock.NewController(t)
			app := env.NewApplication()
			mockProvider, mockDb := setMockProviderAndMockDb(ctrl, app)
			var reader io.Reader
			request, err := http3.NewRequest("GET", url, reader)
			request.Header.Add("auth_provider", "MockProvider")
			fmt.Println("Preparing mock provider")
			mockProvider.EXPECT().Authenticate(gomock.AssignableToTypeOf(&http3.Request{})).Return(tc.authreturnerr)

			mockDb.EXPECT().GetRows(
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf(""),
				gomock.AssignableToTypeOf([]interface{}{}),
				gomock.AssignableToTypeOf([]string{}),
			).Return(tc.want, nil)

			res, err := makeGetRowsRequest(url, func(writer http3.ResponseWriter, request *http3.Request) {
				owner3.GetStables(writer, request, app)
			})
			if err != nil {
				t.Fatal(err)
			}
			if res.Body != nil {
				defer res.Body.Close()
			}

			stables, err := restapi2.GetStablesFromStableRes(res)

			wantedStables := getWantedStables(tc, t)

			if !cmp.Equal(
				stables,
				wantedStables,
				cmp.Options{
					cmpopts.IgnoreUnexported(stable.Stable{}),
					cmpopts.IgnoreUnexported(domain.Model{}),
					cmpopts.IgnoreFields(stable.Stable{}, "Jars"),
				}) {
				t.Errorf("\n Error getting stable; wanted: %v, got %v", wantedStables, stables)
			}
		})
	}
}

func getWantedStables(tc struct {
	tcname        string
	authreturnerr error
	want          []databases.RowResult
}, t *testing.T) []stable.Stable {

	var wantedStables []stable.Stable
	for _, s := range tc.want {
		stable, err := stable.GetStableFromRowResult(s)
		if err != nil {
			t.Error("Failed getting owner from row result")
		}
		wantedStables = append(wantedStables, *stable)
	}

	return wantedStables
}

func TestGetJarsWithFields(t *testing.T) {

	testCases := []struct {
		tcname        string
		authreturnerr error
		want          []databases.RowResult
	}{
		{"JarsFound",
			nil,
			[]databases.RowResult{
				{"Name": "The One"},
			},
		},
		{
			"JarsNotFound",
			errors.New("Invalid scope"),
			[]databases.RowResult{},
		},
	}
	url := "http://localhost:42039/api/owners/1/jars?fields=name"
	testGetJars(testCases, t, url, owner3.GetJars)
}

func TestGetSnails(t *testing.T) {
	testCases := []struct {
		tcname        string
		authreturnerr error
		want          []databases.RowResult
	}{
		{"SnailsFound",
			nil,
			[]databases.RowResult{
				{"Name": "Snail One"},
			},
		},
		{
			"SnailsNotFound",
			errors.New("Invalid scope"),
			[]databases.RowResult{},
		},
	}

	testGetSnails(testCases, t, owner3.GetSnails)
}
