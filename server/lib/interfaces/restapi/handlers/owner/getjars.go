package owner

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func GetJars(w http.ResponseWriter, r *http.Request, app *env.Application) {
	handlers.GetModels(w, r, app, findJars)
}

func findJars(ownerId string, fields []string, app *env.Application) (restapi.Responder, error) {
	// First we need to get all stables belonging to the owner
	sr, err := stable.NewRepo(app.Database)
	if err != nil {
		return nil, err
	}
	stables, err := sr.GetMany("owner_id=?", []interface{}{ownerId}, []string{"Id"})
	var models []repo2.Model
	for _, s := range stables {
		stable := s.(*stable.Stable)
		repo, _ := jar.NewRepo(app.Database)
		jars, err := repo.GetMany("stable_id=?", []interface{}{stable.Id}, fields)
		if err != nil {
			continue
		}
		for _, j := range jars {
			jar := j.(*jar.Jar)
			models = append(models, jar)
		}

	}
	jarRes := &restapi.ExportedJarRes{}

	var status http2.HttpStatus
	if len(models) == 0 {
		status.StatusCode = http.StatusNoContent
		status.Message = "This owner doesn't have any jars."
	} else {
		jarRes.SetModels(models)
		status.StatusCode = http.StatusOK
	}
	jarRes.SetHttpStatus(status)
	return jarRes, err
}
