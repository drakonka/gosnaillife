package owner

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func GetStables(w http.ResponseWriter, r *http.Request, app *env.Application) {
	handlers.GetModels(w, r, app, findStables)
}

func findStables(ownerId string, fields []string, app *env.Application) (restapi.Responder, error) {
	stableRes := &restapi.ExportedStableRes{}
	repo, err := stable.NewRepo(app.Database)
	stables, err := repo.GetMany("owner_id=?", []interface{}{ownerId}, fields)
	var status http2.HttpStatus
	if len(stables) == 0 {
		status.StatusCode = http.StatusNoContent
		status.Message = "This owner doesn't have any stables."
	} else {
		var models []repo2.Model
		for _, s := range stables {
			stable := s.(*stable.Stable)
			models = append(models, stable)
		}
		stableRes.SetModels(models)
		status.StatusCode = http.StatusOK
	}
	stableRes.SetHttpStatus(status)
	return stableRes, err
}
