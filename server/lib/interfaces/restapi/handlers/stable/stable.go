package stable

import (
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func getStableReq(r *http.Request) (restapi.StableReq, error) {
	var stable restapi.StableReq
	err := handlers.GetModelReq(r, &stable)
	return stable, err
}
