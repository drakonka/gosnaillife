package user

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"net/http"
)

func TestAuth(w http.ResponseWriter, r *http.Request, app *env.Application) {
	ap := r.Header.Get("auth_provider")
	statusCode := http.StatusUnauthorized
	if ap != "" {

		fmt.Println("Getting Auth Status")

		authErr := app.Authenticator.AuthenticateUser(ap, r)
		if authErr == nil {
			statusCode = http.StatusOK
		}
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(statusCode)
}
