package user

import (
	"encoding/json"
	"fmt"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func CreateUser(w http.ResponseWriter, r *http.Request, app *env.Application) {
	user, err := getUserReq(r)
	handlers.HandleReqErr(err, w)

	userRes, err := registerUser(user, app)

	error2.HandleErr(err, "CreateUserErr")

	handlers.WriteResponse(w, &userRes)

}

func registerUser(user restapi.UserReq, app *env.Application) (userRes restapi.UserRes, err error) {
	var provider auth.Provider
	provider, err = app.Authenticator.FindProvider(user.AuthProvider)
	if err != nil {
		userRes.Httpstatus.StatusCode = http.StatusUnprocessableEntity
		userRes.Httpstatus.Message = fmt.Sprintf("Could not find requested auth provider %s + %v", user.AuthProvider, err)
	}
	provider.SetResToMarshalTo(&restapi.UserRes{})
	var resBody []byte
	if err == nil {
		resBody, err = provider.Register(user.Username, user.Password)
		if err == nil {
			err = json.Unmarshal(resBody, &userRes)
		}
	}
	return userRes, err
}
