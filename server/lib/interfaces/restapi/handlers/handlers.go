package handlers

import (
	"encoding/json"
	"fmt"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"html"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request, app *env.Application) {
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}

func HandleReqErr(err error, w http.ResponseWriter) {
	if err == nil {
		return
	}
	error2.HandleErr(err, "HandleReqErr")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(422) // unprocessable entity
	if err := json.NewEncoder(w).Encode(err); err != nil {
		error2.HandleErr(err, "CreateUser")
	}
}

func GetModels(w http.ResponseWriter, r *http.Request, app *env.Application, findF func(string, []string, *env.Application) (restapi.Responder, error)) {
	ap := r.Header.Get("auth_provider")
	authErr := app.Authenticator.AuthenticateUser(ap, r)

	if authErr != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	id, fields := GetIdAndRequestedFields(r)
	jarsRes, err := findF(id, fields, app)
	if err != nil {
		error2.HandleErr(err, "GetJarsErr")
	}

	WriteResponse(w, jarsRes)
}
