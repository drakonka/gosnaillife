package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

func GetModelReq(r *http.Request, requester restapi.Requester) error {
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		return err
	}
	defer r.Body.Close()
	err = json.Unmarshal(body, &requester)
	return err
}

func GetIdAndRequestedFields(r *http.Request) (string, []string) {
	vars := mux.Vars(r)
	queryparams := r.URL.Query()
	id := vars["id"]
	var fields []string
	if f, ok := queryparams["fields"]; ok {
		for _, v := range f {
			fields = append(strings.Split(v, ","))
		}
	}
	return id, fields
}

func CreateModel(model repo.Model, responder restapi.Responder, r repo.Repo) restapi.Responder {
	if r == nil {
		responder.SetHttpStatus(http2.HttpStatus{StatusCode: http.StatusBadRequest, Message: "Repo not found"})
		return responder
	}
	id, err := r.InsertOne(model)
	var statusDesc string
	if err != nil {
		statusDesc = err.Error()
	}
	model.SetId(id)
	responder.SetModels([]repo.Model{model})
	if id.(int) > 0 && err == nil {
		responder.SetHttpStatus(http2.HttpStatus{StatusCode: http.StatusOK})
	} else if id.(int) <= 0 {
		responder.SetHttpStatus(http2.HttpStatus{StatusCode: http.StatusBadRequest, Message: statusDesc})
	} else if err != nil {
		responder.SetHttpStatus(http2.HttpStatus{StatusCode: http.StatusNotAcceptable, Message: statusDesc})
	}
	return responder
}

// bodyAllowedForStatus reports whether a given response status code
// permits a body. See RFC 7230, section 3.3.
func BodyAllowedForStatus(status int) bool {
	switch {
	case status >= 100 && status <= 199:
		return false
	case status == 204:
		return false
	case status == 304:
		return false
	}
	return true
}
