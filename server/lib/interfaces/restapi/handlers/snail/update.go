package snail

import (
	"github.com/gorilla/mux"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"net/http"
)

func Update(w http.ResponseWriter, r *http.Request, app *env.Application) {

	// Authenticate
	ap := r.Header.Get("auth_provider")
	authErr := app.Authenticator.AuthenticateUser(ap, r)

	if authErr != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	vars := mux.Vars(r)
	id, _ := vars["id"]

	res := restapi.ExportedSnailRes{}

	var sc int
	var msg string

	// Try to update
	sc, msg = updateSnail(r, id, &res, app.Database)
	status := http2.HttpStatus{StatusCode: sc, Message: msg}
	res.SetHttpStatus(status)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(sc)
	if err := res.JsonEncode(w); err != nil {
		error2.HandleErr(err, "UpdateSnailErr")
		return
	}
}

func updateSnail(r *http.Request, id string, res *restapi.ExportedSnailRes, db databases.Database) (int, string) {
	repo, err := snail.NewRepo(db)

	var sc int
	var msg string

	req, err := getSnailReq(r)
	if err != nil {
		sc = http.StatusBadRequest
		msg = "Could not decode POST body"
		return sc, msg
	}

	defer r.Body.Close()
	err = repo.UpdateOne(id, req.FieldsToUpdate)
	if err != nil {
		sc = http.StatusBadRequest
		msg = "Could not update snail: " + err.Error()
		return sc, msg
	}

	// Retrieve updated owner again and include in owner res
	updatedSnail, err := repo.GetOne(id, []string{})
	if err != nil {
		sc = http.StatusInternalServerError
		msg = "Could not retrieve just updated owner..."
		return sc, msg
	}
	res.SetModels([]repo2.Model{updatedSnail.(*snail.Snail)})
	sc = http.StatusOK
	return sc, msg
}
