package snail

import (
	"fmt"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	snail2 "gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func CreateSnail(w http.ResponseWriter, r *http.Request, app *env.Application) {
	common.Log.Info("Creating snail")
	// Authenticate
	ap := r.Header.Get("auth_provider")
	authErr := app.Authenticator.AuthenticateUser(ap, r)

	if authErr != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	snailReq, err := getSnailReq(r)
	if err != nil {
		handlers.HandleReqErr(err, w)
		return
	}
	snailRes := createSnail(snailReq, app)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(snailRes.HttpStatus().StatusCode)
	if handlers.BodyAllowedForStatus(snailRes.HttpStatus().StatusCode) {
		err = snailRes.JsonEncode(w)
		if err != nil {
			fmt.Printf("\n Error when encoding res: %v", err)
		}
	}
}

func createSnail(snailReq restapi.SnailReq, app *env.Application) (snailRes restapi.ExportedSnailRes) {
	snail := snailReq.Snail
	emptySnail := snail2.Snail{}
	eRes := restapi.ExportedSnailRes{}
	if cmp.Equal(snail, emptySnail, cmpopts.IgnoreUnexported(domain.Model{}), cmpopts.IgnoreTypes(map[string]organism.Gene{})) {
		repo, err := snail2.NewRepo(app.Database)
		s, err := snail2.GenerateRandomSnail(0, repo)
		if err != nil {
			status := http2.HttpStatus{StatusCode: http.StatusNotAcceptable, Message: "Couldn't generate a random snail"}
			eRes.SetHttpStatus(status)
			return eRes
		}
		snail = *s
	}

	repo, err := app.GetOrMakeRepo("snail", snail2.NewRepo)
	if err != nil {
		status := http2.HttpStatus{StatusCode: http.StatusInternalServerError, Message: "Couldn't find or generate repo"}
		eRes.SetHttpStatus(status)
		return eRes
	}

	res := handlers.CreateModel(&snail, &eRes, repo)
	return *res.(*restapi.ExportedSnailRes)
}
