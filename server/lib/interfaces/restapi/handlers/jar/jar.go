package jar

import (
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"net/http"
)

func getJarReq(r *http.Request) (restapi.JarReq, error) {
	var jar restapi.JarReq
	err := handlers.GetModelReq(r, &jar)
	return jar, err
}
