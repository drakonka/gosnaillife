package restapi

import (
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/jar"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/owner"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/snail"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/stable"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/restapi/handlers/user"
	"net/http"
)

// An API route
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
	Queries     map[string]string
}

// Slice of all API routes
type Routes []Route

func getRoutes(app *env.Application) Routes {
	return Routes{
		Route{
			Name:    "Index",
			Method:  "GET",
			Pattern: "/",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				handlers.Index(writer, request, app)
			},
		},
		Route{
			Name:    "CreateUser",
			Method:  "POST",
			Pattern: "/api/users",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				user.CreateUser(writer, request, app)
			},
		},
		Route{
			Name:    "LoginUser",
			Method:  "POST",
			Pattern: "/api/session",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				user.LoginUser(writer, request, app)
			},
		},
		Route{
			Name:    "GetOwner",
			Method:  "GET",
			Pattern: "/api/users/{id}/owners",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				user.GetOwner(writer, request, app)
			},
		},
		Route{
			Name:    "GetStables",
			Method:  "GET",
			Pattern: "/api/owners/{id}/stables",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				owner.GetStables(writer, request, app)
			},
		},
		Route{
			Name:    "GetJarsByOwnerId",
			Method:  "GET",
			Pattern: "/api/owners/{id}/jars",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				owner.GetJars(writer, request, app)
			},
		},
		Route{
			Name:    "GetJarsByStableId",
			Method:  "GET",
			Pattern: "/api/stables/{id}/jars",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				stable.GetJars(writer, request, app)
			},
		},
		Route{
			Name:    "GetSnailsByOwnerId",
			Method:  "GET",
			Pattern: "/api/owners/{id}/snails",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				owner.GetSnails(writer, request, app)
			},
		},
		Route{
			Name:    "GetSnailsByJarId",
			Method:  "GET",
			Pattern: "/api/jars/{id}/snails",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				jar.GetSnails(writer, request, app)
			},
		},
		Route{
			Name:    "TestAuth",
			Method:  "GET",
			Pattern: "/api/session/status",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				user.TestAuth(writer, request, app)
			},
		},
		Route{
			Name:    "CreateOwner",
			Method:  "POST",
			Pattern: "/api/owners",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				owner.CreateOwner(writer, request, app)
			},
		},
		Route{
			Name:    "CreateStable",
			Method:  "POST",
			Pattern: "/api/stables",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				stable.CreateStable(writer, request, app)
			},
		},
		Route{
			Name:    "CreateJar",
			Method:  "POST",
			Pattern: "/api/jars",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				jar.CreateJar(writer, request, app)
			},
		},
		Route{
			Name:    "CreateSnail",
			Method:  "POST",
			Pattern: "/api/snails",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				snail.CreateSnail(writer, request, app)
			},
		},
		Route{
			Name:    "UpdateOwner",
			Method:  "POST",
			Pattern: "/api/owners/{id}",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				owner.Update(writer, request, app)
			},
		},
		Route{
			Name:    "UpdateSnail",
			Method:  "POST",
			Pattern: "/api/snails/{id}",
			HandlerFunc: func(writer http.ResponseWriter, request *http.Request) {
				snail.Update(writer, request, app)
			},
		},
	}
}
