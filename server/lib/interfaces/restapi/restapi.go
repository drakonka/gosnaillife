package restapi

import (
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
)

type app struct {
	env.Application
}
