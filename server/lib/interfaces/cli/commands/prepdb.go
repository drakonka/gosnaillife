package commands

import (
	"errors"
	"github.com/spf13/cobra"
)

var prepDbCommand = &cobra.Command{
	Use:   "prepdb",
	Short: "Prepare project database",
	Long:  "Creates a db and tables for the project where needed",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("requires at least one arg")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		prepDb(args[0])
	},
}

func prepDb(preptype string) {
	// Create snaillife db
	if preptype == "reset" {
		resetDb()
	} else if preptype == "fix" {
		checkAndFixDb()
	}
}

func resetDb() {

}

func checkAndFixDb() {

}
