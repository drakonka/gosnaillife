package commands

import (
	"github.com/spf13/cobra"
)

// RootCmd is the main command to start the server
var RootCmd = &cobra.Command{
	Use: "snaillife",
}

// ServerPort is the port on which the server will run
var (
	ServerPort int
	TestMode   bool
)

func init() {
	RootCmd.AddCommand(serveCommand)
	RootCmd.AddCommand(prepDbCommand)
	serveCommand.Flags().IntVar(&ServerPort, "port", 49101, "Port to start server on")
	serveCommand.Flags().String("migrations", "", "Location to migrations to run relative to root")
	serveCommand.Flags().BoolVar(&TestMode, "test", false, "Set to true to start in test mode")
	serveCommand.Flags().Bool("skipOsExit", false, "Whether to exit the application after cleanup")
	serveCommand.Flags().Bool("skipDefaultMigrations", false, "Whether to skip default DB migrations")
	serveCommand.Flags().Bool("skipDefaultSeed", false, "Whether to skip default test DB seed.")
	serveCommand.Flags().Bool("frozenWorld", false, "Start server but not world updates.")
}
