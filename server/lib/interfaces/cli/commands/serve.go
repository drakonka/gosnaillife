package commands

import (
	"fmt"
	"github.com/go-errors/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/domain/world"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/tests"
	"golang.org/x/crypto/bcrypt"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
)

var serveCmdArgs []string
var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start server",
	Long:  "Starts the SnailLife server",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("\n\nNum of args: %d", len(args))
		if TestMode {
			os.Setenv("SNAILLIFE_TESTMODE", "true")
		}
		cmd.Flags().Visit(getArgs)
		app, err := infrastructure.Init(common.CLI)
		if err != nil {
			fmt.Println(err)
			os.Exit(-1)
		}

		startServer(serveCmdArgs, app, &sync.RWMutex{})
	},
}

func getArgs(f *pflag.Flag) {
	serveCmdArgs = append(serveCmdArgs, fmt.Sprintf("--%s", f.Name), f.Value.String())
}

func startServer(args []string, app *env.Application, mx *sync.RWMutex) error {
	var err error
	var skipOsExit bool
	var testMode bool
	var skipDefaultMigrations bool
	var skipDefaultSeed bool
	var frozenWorld bool
	var migrationsDir string
	port := ServerPort
	for i, arg := range args {
		switch arg {
		case "--port":
			p, err := strconv.Atoi(args[i+1])
			if err != nil {
				return err
			}

			port = p
			i++
		case "--test":
			testMode = true
			os.Setenv("SNAILLIFE_TESTMODE", "true")
		case "--skipOsExit":
			skipOsExit = true
		case "--migrations":
			migrationsDir = args[i+1]
			i++
		case "--skipDefaultMigrations":
			skipDefaultMigrations = true
		case "--skipDefaultSeed":
			skipDefaultSeed = true
		case "--frozenWorld":
			frozenWorld = true
		}
	}

	mx.RLock()
	quitting := app.Quitting
	mx.RUnlock()
	if testMode && !quitting {
		common.Log.Info("Prepping test mode")
		err = prepTestMode(app, skipOsExit, skipDefaultMigrations, skipDefaultSeed, migrationsDir)
		if err != nil {
			return err
		}
	}

	common.Log.Info("Starting SnailLife server.")
	mx.RLock()
	quitting = app.Quitting
	mx.RUnlock()
	if !frozenWorld && !quitting {
		mx.Lock()
		qc := app.NewQuitChannel()
		mx.Unlock()
		common.Log.Info("Starting world updates")
		w, err := world.NewWorld(qc)
		if err != nil {
			return err
		}
		err = w.Start(app)
		if err != nil {
			return err
		}

	}
	common.Log.Info("Starting http server")
	if quitting {
		return errors.New("http: Server closed")
	}
	return app.HttpServer.StartServer(port, mx)
}

func prepTestMode(app *env.Application, skipOsExit bool, skipDefaultMigrations bool, skipDefaultSeed bool, migrationsDir string) error {
	fmt.Println("Starting in test mode")
	tu := tests.NewTestUtil()
	tu.SkipOsExit = skipOsExit
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT, syscall.SIGKILL, syscall.SIGQUIT, syscall.SIGSTOP)
	go func() {
		fmt.Println("Waiting for test srv termination")
		<-c
		tu.DoCleanup()
	}()

	if app.Configuration == nil {
		var err error
		app, err = infrastructure.Init(common.CLI)
		if err != nil {
			return err
		}
	}
	app.HttpServer.RegisterOnShutdown(tu.DoCleanup)
	infrastructure.ReloadDb(app)
	if _, err := tu.CreateTestDB(app.Configuration.DBconfig.DBName); err != nil {
		return err
	}
	if err := tu.RunDbMigrations(skipDefaultMigrations, migrationsDir); err != nil {
		common.Log.Errorf("Failed to run DB migrations: %v", err)
		return err
	}
	if !skipDefaultSeed {
		if err := seed(app.Configuration.DBconfig.DBName, tu); err != nil {
			common.Log.Errorf("Failed to seed test db: %v", err)
			return err
		}
	}

	return nil
}

func seed(dbName string, tu *tests.TestUtil) error {
	common.Log.Infof("Seeding default db in dbname: %s", dbName)
	user1hash, _ := bcrypt.GenerateFromPassword([]byte("testpass"), bcrypt.DefaultCost)
	user2hash, _ := bcrypt.GenerateFromPassword([]byte("kimtest"), bcrypt.DefaultCost)
	user3hash, _ := bcrypt.GenerateFromPassword([]byte("emmatest"), bcrypt.DefaultCost)
	user4hash, _ := bcrypt.GenerateFromPassword([]byte("othertest"), bcrypt.DefaultCost)
	user5hash, _ := bcrypt.GenerateFromPassword([]byte("lokitest"), bcrypt.DefaultCost)
	user6hash, err := bcrypt.GenerateFromPassword([]byte("samtest"), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	err = tu.Seed(dbName, "users", []map[string]string{
		{"email": "paper@tigers.com", "password": string(user1hash)},
		{"email": "kim@test.com", "password": string(user2hash)},
		{"email": "emma@test.com", "password": string(user3hash)},
		{"email": "other@test.com", "password": string(user4hash)},
		{"email": "loki@test.com", "password": string(user5hash)},
		{"email": "sam@test.com", "password": string(user6hash)},
	})

	if err != nil {
		return err
	}

	err = tu.Seed(dbName, "stables", []map[string]string{
		{"owner_id": "2", "name": "The One"},
	})

	if err != nil {
		return err
	}

	err = tu.Seed(dbName, "owners", []map[string]string{
		{"user_id": "1", "firstname": "Paper", "lastname": "Tiger"},
		{"user_id": "3", "firstname": "Emma", "lastname": "Test"},
	})

	if err != nil {
		return err
	}

	err = tu.Seed(dbName, "jars", []map[string]string{
		{"stable_id": "1", "name": "Paper Jar"},
	})

	return err
}
