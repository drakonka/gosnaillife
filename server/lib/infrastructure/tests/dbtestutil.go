package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose"
	"gitlab.com/drakonka/gosnaillife/common"
	jar2 "gitlab.com/drakonka/gosnaillife/common/domain/jar"
	owner2 "gitlab.com/drakonka/gosnaillife/common/domain/owner"
	snail2 "gitlab.com/drakonka/gosnaillife/common/domain/snail"
	gene2 "gitlab.com/drakonka/gosnaillife/common/domain/snail/gene"
	organ2 "gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	stable2 "gitlab.com/drakonka/gosnaillife/common/domain/stable"
	user2 "gitlab.com/drakonka/gosnaillife/common/domain/user"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases/mysql"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

type TestUtil struct {
	SqlxDb     *sqlx.DB
	dbs        map[string]databases.Database
	SkipOsExit bool
	closed     bool
}

var testUtilInstance *TestUtil

func NewTestUtil() *TestUtil {
	if testUtilInstance != nil {
		return testUtilInstance
	}
	return &TestUtil{dbs: make(map[string]databases.Database)}
}

func (tu *TestUtil) RunDbMigrations(skipDefaultMigrations bool, migrationsDir string) error {
	common.Log.Info("Running database migrations")
	var err error
	if !skipDefaultMigrations {
		dir := getMigrationsPath("/migrations")
		common.Log.Infof("Running default migrations from dir %s", dir)
		goose.SetDialect("mysql")
		err = goose.Run("up", tu.SqlxDb.DB, dir)
		if err != nil {
			return err
		}
	}
	if len(migrationsDir) > 0 {
		// We rely on this being a path relative to the project root
		dir := getMigrationsPath(migrationsDir)
		goose.SetDialect("mysql")
		err = goose.Run("up", tu.SqlxDb.DB, dir)
	}
	return err
}

func getProjectRoot() string {
	_, b, _, _ := runtime.Caller(0)
	folders := strings.Split(b, "/")
	folders = folders[:len(folders)-4]
	path := strings.Join(folders, "/")
	return path
}

func getMigrationsPath(pathToMigrations string) string {
	path := getProjectRoot()
	basepath := filepath.Join(filepath.Dir(path), pathToMigrations)
	return basepath
}

func (tu *TestUtil) seed(db databases.Database, tablename string, rows []map[string]string) error {
	for _, row := range rows {
		_, err := db.Insert(tablename, row)
		if err != nil {
			return err
		}
	}
	return nil
}

func (tu *TestUtil) loadSqlxDb(dbConfig env.DBconfig) {
	if tu.SqlxDb == nil {
		connectconf := fmt.Sprintf("%s:%s@tcp(%s:%s)/?parseTime=true",
			dbConfig.DBUser,
			dbConfig.DBPass,
			dbConfig.DBHost,
			dbConfig.DBPort)
		db, err := sqlx.Open("mysql", connectconf)
		if err != nil {
			fmt.Println("error")
			fmt.Println(err)
		}
		tu.SqlxDb = db
	}
}

func (tu *TestUtil) CreateTestDB(dbname string, schemas ...string) (databases.Database, error) {
	app, err := infrastructure.Init(common.CLI)
	if err != nil {
		return nil, err
	}
	dbc := app.Configuration.DBconfig
	tu.loadSqlxDb(dbc)
	// if db already exists, delete it
	tu.DeleteTestDB(false, dbname)
	// Create db
	query := fmt.Sprintf("CREATE DATABASE %s;", dbname)
	_, err = tu.SqlxDb.Exec(query)
	if err != nil {
		return nil, err
	}

	_, err = tu.SqlxDb.Exec("USE " + dbname)
	if err != nil {
		return nil, err
	}

	for _, schema := range schemas {
		// Create test table
		query = schema
		_, err = tu.SqlxDb.Exec(query)
		if err != nil {
			return nil, err
		}
	}

	database := &mysql.MySql{}
	database.Configure(dbc.DBUser, dbc.DBPass, dbc.DBHost, dbc.DBPort, dbname)
	tu.dbs[dbname] = database
	database.Connect()
	return database, nil
}

func (tu *TestUtil) Seed(dbname string, tablename string, rows []map[string]string) error {
	if db, ok := tu.dbs[dbname]; ok {
		for _, row := range rows {
			_, err := db.Insert(tablename, row)
			if err != nil {
				return err
			}
		}
		return nil
	}
	return errors.Errorf("Could not find database by name %v", dbname)
}

func (tu *TestUtil) DeleteTestDB(close bool, dbnames ...string) {
	for _, dbname := range dbnames {
		query := fmt.Sprintf("DROP DATABASE %s;", dbname)
		_, err := tu.SqlxDb.Exec(query)
		if err != nil {
			fmt.Println("Delete error: " + err.Error())
		}
	}
	if close {
		tu.SqlxDb.Close()
	}
}

func (tu *TestUtil) DeleteAllTestDBs() {
	for dbname := range tu.dbs {
		query := fmt.Sprintf("DROP DATABASE %s;", dbname)
		_, err := tu.SqlxDb.Exec(query)
		if err != nil {
			fmt.Println("Delete error: " + err.Error())
		} else {
			delete(tu.dbs, dbname)
		}
	}
	tu.SqlxDb.Close()
}

func (tu *TestUtil) DoCleanup() {
	if tu.closed {
		fmt.Println("TU already cleaned and closed")
		return
	}
	fmt.Println("TestUtil Running test srv cleanup")
	tu.closed = true
	tu.DeleteAllTestDBs()
	if !tu.SkipOsExit {
		os.Exit(1)
	}
}

func (tu *TestUtil) PrepRepos(app *env.Application, db databases.Database) {
	app.Repos = make(map[string]repo.Repo)
	user, _ := user2.NewRepo(db)
	app.Repos["user"] = user
	owner, _ := owner2.NewRepo(db)
	app.Repos["owner"] = owner
	stable, _ := stable2.NewRepo(db)
	app.Repos["stable"] = stable
	jar, _ := jar2.NewRepo(db)
	app.Repos["jar"] = jar
	snail, _ := snail2.NewRepo(db)
	app.Repos["snail"] = snail
	organ, _ := organ2.NewRepo(db)
	app.Repos["organ"] = organ
	gene, _ := gene2.NewRepo(db)
	app.Repos["gene"] = gene
}
