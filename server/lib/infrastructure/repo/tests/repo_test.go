package tests

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/jmoiron/sqlx/reflectx"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/tests"
	"os"
	"reflect"
	"strconv"
	"strings"
	"testing"
)

var testdbname = "sltestdb"
var testtblname = "test_table"

var testTblSchema = fmt.Sprintf(`CREATE TABLE %s(
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(20), PRIMARY KEY (id),
description VARCHAR(100));`, testtblname)

var database databases.Database

func TestMain(m *testing.M) {
	tu := tests.NewTestUtil()
	db, err := tu.CreateTestDB(testdbname, testTblSchema)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	database = db
	ret := m.Run()

	tu.DeleteTestDB(true, testdbname)
	os.Exit(ret)
}

type TestItemSchema struct {
	Id          int    `db:"id"`
	Name        string `db:"name"`
	Description string `db:"description"`
}

func TestRepoSaveOne(t *testing.T) {
	schematc1 := TestItemSchema{}
	schematc1.Name = "SaveNameOne"
	schematc2 := TestItemSchema{2, "SaveNameTwo", "Description one"}
	schematc3 := TestItemSchema{2, "SaveNameThree", "Description 2"}
	testCases := []struct {
		tcname string
		item   TestItem
		want   string
	}{
		{"IDNotSpecified", TestItem{&schematc1, NewTestItemRepo(database)}, ""},
		{"IDSpecified", TestItem{&schematc2, NewTestItemRepo(database)}, ""},
		{"IDDuplicated", TestItem{&schematc3, NewTestItemRepo(database)}, "Duplicate entry"},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestRepoGetMany test case %s.", tc.tcname)

		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			err := tc.item.Save()

			if tc.want == "" && err != nil {
				t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, err.Error()))

			} else if err != nil && !strings.Contains(err.Error(), tc.want) {
				t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, fmt.Sprintf("expected error containing %s, got %s", tc.want, err.Error())))

			}
		})
	}
}

func TestRepoGetOne(t *testing.T) {
	testCases := []struct {
		tcname      string
		pkey        string
		desiredCols []string
		want        map[string]string
	}{
		{"GetEntireRow", "2", nil, map[string]string{"id": "2", "name": "SaveNameTwo"}},
		{"GetOnlyName", "1", []string{"name"}, map[string]string{"id": "1", "name": "SaveNameOne"}},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestRepoGetOne test case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {

			var item TestItem
			repo := NewTestItemRepo(database)
			item, err := repo.GetOne(tc.pkey, tc.desiredCols)
			if err != nil {
				t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, err.Error()))

			} else {
				id, _ := strconv.Atoi(tc.want["id"])
				if item.schema.Id != id || item.schema.Name != tc.want["name"] {
					errMsg := fmt.Sprintf(`Unexpected values;
					Wanted %s, %s;
					Received %d, %s`, tc.want["id"], tc.want["name"], item.schema.Id, item.schema.Name)
					t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, errMsg))

				}
			}
		})
	}
}

func TestRepoGetMany(t *testing.T) {
	testCases := []struct {
		tcname      string
		where       string
		params      []interface{}
		desiredCols []string
		wantId      int
	}{
		{"WhereIdGTROne", "id > ?", []interface{}{1}, nil, 2},
		{"WhereNoRowsMatch", "id = ?", []interface{}{"Wat"}, nil, 0},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestRepoGetMany test case %s.", tc.tcname)

		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			var items []TestItem
			repo := NewTestItemRepo(database)
			items, err := repo.GetMany(tc.where, tc.params, tc.desiredCols)
			if err != nil {
				t.Errorf("Test case %s failed: %s", tc.tcname, err.Error())
			} else {
				if tc.wantId == 0 && len(items) > 0 {
					for i := 0; i < len(items); i++ {
						item := items[i]
						if item.schema.Id != tc.wantId {
							errMsg := fmt.Sprintf(`Unexpected values;
							Wanted %d;
							Received %d`, tc.wantId, item.schema.Id)
							t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, errMsg))

						}
					}
				}
			}
		})
	}
}

func TestRepoUpdateOne(t *testing.T) {
	testCases := []struct {
		tcname         string
		pkey           string
		fieldsToUpdate map[string]string
	}{
		{"UpdateAllFields", "1", nil},
		{"UpdateOneField", "1", map[string]string{"description": "Edited description"}},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestRepoUpdateOne test case %s.", tc.tcname)

		t.Run(fmt.Sprintf("%s", tc.tcname), func(t *testing.T) {
			repo := NewTestItemRepo(database)
			item1, err := repo.GetOne(tc.pkey, nil)
			if err != nil {
				t.Errorf("Error getting item: %s", err)
			}
			item1.schema.Name = "Edited name"
			item1.schema.Description = "Edited description"
			item1structmap := structs.Map(item1.schema)
			err = item1.Update(tc.fieldsToUpdate)
			if err != nil {
				t.Errorf("Test case %s failed: %s", tc.tcname, err)
			} else {
				item2, _ := repo.GetOne(tc.pkey, nil)
				item2structmap := structs.Map(item2.schema)
				tm := getTypeMap(item2.schema)

				// Loop through typemap, compare modified fields to retrieved
				for dbColName, v := range tm.Names {
					structVarName := v.Field.Name

					msg := ""
					if tc.fieldsToUpdate == nil {
						msg = checkUpdateAllResult(structVarName, item1structmap, item2structmap)

					} else if wantedVal, ok := tc.fieldsToUpdate[dbColName]; ok {
						msg = checkUpdateOneResult(structVarName, wantedVal, item2structmap)
					}
					if msg != "" {
						t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, msg))
					}
				}
			}
		})
	}

}

func checkUpdateAllResult(structVarName string, item1structmap map[string]interface{}, item2structmap map[string]interface{}) (msg string) {
	item1structval := item1structmap[structVarName]
	item1convertedval := fmt.Sprintf("%v", item1structval)

	item2structVal := item2structmap[structVarName]
	item2convertedVal := fmt.Sprintf("%v", item2structVal)

	if item2convertedVal != item1convertedval {
		msg = fmt.Sprintf("Expected: %s, Actual: %s", item1convertedval, item2convertedVal)
	}
	return msg
}

func checkUpdateOneResult(structVarName string, wantedVal string, item2structmap map[string]interface{}) (msg string) {
	item2structVal := item2structmap[structVarName]
	item2convertedVal := fmt.Sprintf("%v", item2structVal)
	if item2structVal == nil || item2convertedVal != wantedVal {
		msg = fmt.Sprintf("Expected: %s, Actual: %s", wantedVal, item2convertedVal)
	}
	return msg
}

func getTypeMap(schema interface{}) (tm *reflectx.StructMap) {
	nameMapper := strings.ToLower
	mapper := reflectx.NewMapperFunc("db", nameMapper)
	t := reflect.TypeOf(schema)
	tm = mapper.TypeMap(t)
	return tm
}
