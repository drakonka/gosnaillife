package auth

import (
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/auth/util"
	"net/http"
)

// Provider represents a single authentication provider, like MySQL or Auth0
type Provider interface {
	Configure(credentials map[string]interface{}) error
	Register(email, password string) (resBody []byte, err error)
	Login(username, password string) (userRes []byte, err error)
	Logout(token string) (resBody []byte, err error)
	Authenticate(request *http.Request) error
	SetResToMarshalTo(res util.Responder)
}
