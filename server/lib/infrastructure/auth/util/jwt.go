package util

import (
	"fmt"
	"github.com/auth0-community/go-auth0"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
	"net/http"
	"strings"
)

// CheckJwt validates the provided JSON Web Token
func CheckJwt(r *http.Request, domain string) error {
	common.Log.Info("Checking JWT")
	client := auth0.NewJWKClient(auth0.JWKClientOptions{URI: domain + ".well-known/jwks.json"}, nil)
	audience := []string{"https://snaillife"}

	configuration := auth0.NewConfiguration(client, audience, domain, jose.RS256)
	validator := auth0.NewValidator(configuration, nil)
	token, err := validator.ValidateRequest(r)

	if err != nil {
		return err
	}
	scopeValid := checkScope(r, validator, token)
	if scopeValid == true {
		// If the token is valid and we have the right scope, we'll pass through the middleware
		return nil
	}
	err = errors.New("Invalid scope")
	return err

}

func checkScope(r *http.Request, validator *auth0.JWTValidator, token *jwt.JSONWebToken) bool {
	claims := map[string]interface{}{}
	err := validator.Claims(r, token, &claims)

	if err != nil {
		fmt.Println(err)
		return false
	}

	if strings.Contains(claims["scope"].(string), "read:users") {
		return true
	}
	return false
}
