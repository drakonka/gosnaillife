package util

import (
	"fmt"
	"github.com/go-errors/errors"
	http2 "gitlab.com/drakonka/gosnaillife/common/util/http"
	"net/http"
	"strings"
)

// HandleAuthErr takes an authentication error and returns an Httpstatus
func HandleAuthErr(err error, note string) http2.HttpStatus {
	var httpstatus http2.HttpStatus
	if err == nil {
		httpstatus.StatusCode = 200
		return httpstatus
	}
	if strings.Contains(err.Error(), "hashedPassword is not the hash of the given password") ||
		strings.Contains(err.Error(), "hashedSecret too short to be a bcrypted password") {
		err = errors.New("Your credentials could not be found")
		httpstatus.StatusCode = http.StatusUnauthorized
	} else {
		httpstatus.StatusCode = http.StatusInternalServerError
	}
	fmt.Println(note + " " + err.Error())
	httpstatus.Message = handleErrorCode(err.Error())
	return httpstatus
}

func handleErrorCode(msg string) string {
	err := msg
	if strings.Contains(msg, "Error 1406") {
		err = "Username or password was too long"
	}
	return err
}
