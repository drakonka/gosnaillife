package util

import "gitlab.com/drakonka/gosnaillife/common/util/http"

type Responder interface {
	SetId(id string)
	SetHttpStatus(s http.HttpStatus)
	SetAccessToken(t string)
}
