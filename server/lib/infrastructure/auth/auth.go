package auth

import (
	"gitlab.com/drakonka/gosnaillife/common"
)

// NewAuthenticator returns a new authenticator.
func NewAuthenticator(clientType common.ClientType) *Authenticator {
	a := Authenticator{}
	a.Providers = make(map[string]Provider)

	return &a
}
