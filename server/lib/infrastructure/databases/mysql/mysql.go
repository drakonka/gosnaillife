package mysql

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"gitlab.com/drakonka/gosnaillife/common"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
)

// MySql contains connection details to a MySQL database.
type MySql struct {
	user       User
	connection Connection
	connected  bool
	db         *sqlx.DB
	tx         *sql.Tx
}

// User contains credentials to a MySQL user.
type User struct {
	name string
	pass string
}

// Connection contains MySQL database host and name
type Connection struct {
	dbhost string
	dbport string
	dbname string
}

// MySqlError is an error related to a MySql operation
type MySqlError struct {
	msg string
}

// Configure sets the MySQL auth and database details.
func (mysql *MySql) Configure(username, userpass, dbhost, dbport, dbname string) {
	mysql.user = User{username, userpass}
	mysql.connection = Connection{dbhost, dbport, dbname}
}

// Connect establishes a connection to the MySQL database.
func (mysql *MySql) Connect() error {
	if mysql.connected {
		return nil
	}
	common.Log.Debug("Connecting to database " + mysql.connection.dbname)
	u := mysql.user
	c := mysql.connection

	connectconf := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true",
		u.name,
		u.pass,
		c.dbhost,
		c.dbport,
		c.dbname)
	db, err := sqlx.Open("mysql", connectconf)
	if err != nil {
		msg := fmt.Sprintf("Connection error: %s", err)
		common.Log.Error(msg)
		return err
	}

	err = mysql.checkDB(db)
	if err != nil {
		msg := fmt.Sprintf("Connection is bad: %s", err)
		common.Log.Error(msg)
		return err
	}

	mysql.db = db
	mysql.connected = true
	return err
}

// Disconnect closes the database and reports it as disconnected.
func (mysql *MySql) Disconnect() error {
	if mysql.db == nil {
		msg := "Disconnect error: mysql.db is nil. Never connected?"
		err := fmt.Errorf(msg)
		mysql.connected = false
		return err
	}
	err := mysql.db.Close()
	if err != nil {
		fmt.Println("Disconnect error: " + err.Error())
	}
	mysql.connected = false
	return err
}

func closeStmtIfExists(stmt *sql.Stmt) {
	if stmt != nil {
		err := stmt.Close()
		error2.HandleErr(err, "Close Stmt")
	}
}

// PrepAndExecStmt prepares a statement and executes it on the database.
func (mysql *MySql) PrepAndExecStmt(query string, vals ...interface{}) (int64, error) {
	var tx *sqlx.Tx
	var err error
	if mysql.tx == nil {
		tx, err = mysql.db.Beginx()
		if err != nil {
			error2.HandleErr(err, "Beginx error")
			return -1, err
		}
	}

	var stmt *sql.Stmt
	if tx != nil {
		stmt, err = tx.Prepare(query)
	} else {
		stmt, err = mysql.tx.Prepare(query)
	}
	defer closeStmtIfExists(stmt)
	if err != nil {
		error2.HandleErr(err, "Prepare Error.")
		if tx != nil {
			err = tx.Rollback()
		}
		return -1, err
	}

	res, err := stmt.Exec(vals...)
	if err != nil {
		error2.HandleErr(err, "Exec Error.")
		if tx != nil {
			tx.Rollback()
		}
		return -1, err
	} else if res == nil {
		err = errors.New("The response is nil!")
		error2.HandleErr(err, "Exec Error.")
		if tx != nil {
			tx.Rollback()
		}
		return -1, err
	} else {
		if tx != nil {
			err = tx.Commit()
			if err != nil {
				error2.HandleErr(err, "Commit Error.")
			}
		}
	}
	id, err := res.LastInsertId()
	return id, err
}

// PrepAndQueryStmt prepares and statement and runs Queryx on the database.
func (mysql *MySql) PrepAndQueryStmt(query string, vals ...interface{}) (m map[string]interface{}, err error) {
	stmt, err := mysql.db.Preparex(query)
	if err != nil {
		error2.HandleErr(err, "Prepare Error.")
		return m, err
	}
	defer stmt.Close()
	rows, err := stmt.Queryx(vals...)
	error2.HandleErr(err, "Query:")
	defer rows.Close()
	columns, _ := rows.Columns()

	for rows.Next() {
		// Create a slice of interface{}s for each column
		cols := make([]interface{}, len(columns))
		// Create slice of pointers for each item
		colPointers := make([]interface{}, len(columns))

		for i := range cols {
			colPointers[i] = &cols[i]
		}

		// Scan rows into column pointers
		err := rows.Scan(colPointers...)
		error2.HandleErr(err, "Scan row")

		// Create map of string: inerface{}
		m = make(map[string]interface{})

		// Loop through all columns and store in map
		for i, colName := range columns {
			val := colPointers[i].(*interface{})
			m[colName] = fmt.Sprintf("%s", *val)
		}
	}
	return m, err
}

func (mysql *MySql) checkDB(db *sqlx.DB) error {
	err := db.Ping()
	return err
}

func (mysql *MySql) getPrimaryKey(tablename string) (pkey string) {
	query := fmt.Sprintf(`SHOW KEYS FROM %s WHERE Key_name = 'PRIMARY'`, tablename)

	m, err := mysql.PrepAndQueryStmt(query)
	error2.HandleErr(err, "getPrimaryKey:")
	pkey = m["Column_name"].(string)
	return pkey
}

func (mysql *MySql) Transact(f func(*sql.Tx) error) (err error) {
	tx, err := mysql.db.Begin()
	if err != nil {
		return
	}
	mysql.tx = tx
	defer func() {
		if p := recover(); p != nil {
			tx.Rollback()
			mysql.tx = nil
			panic(p)
		} else if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit()
		}
		mysql.tx = nil
	}()
	err = f(tx)
	return err
}

func escapeStr(s string) string {
	var escapedS string
	for _, c := range s {
		escapedS = escapedS + string(c)
		if c == '\'' {
			escapedS = escapedS + "'"
		}
	}
	return escapedS
}

func (e *MySqlError) Error() string {
	return e.msg
}
