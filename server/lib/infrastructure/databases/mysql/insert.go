package mysql

import (
	"fmt"
	"github.com/go-errors/errors"
	"strings"
)

// Insert inserts creates and saves a row into the designated db table.
func (mysql *MySql) Insert(tablename string, fields map[string]string) (int64, error) {
	err := mysql.Connect()
	if err != nil {
		return -1, err
	}
	fieldQuant := len(fields)
	if fieldQuant < 1 {
		err = errors.New("Nothing to insert")
		return -1, err
	}

	keyQ, valQ, vals := prepInsertQuery(fields)
	query := fmt.Sprintf("INSERT INTO %s(%s) VALUES(%s)", tablename, keyQ, valQ)
	id, err := mysql.PrepAndExecStmt(query, vals...)
	return id, err
}

func prepInsertQuery(fields map[string]string) (string, string, []interface{}) {
	keyQ := ""
	valQ := ""
	vals := []interface{}{}
	keys := make([]string, 0, len(fields))
	valSymbols := make([]string, 0, len(fields))
	for k, v := range fields {
		keys = append(keys, k)
		vals = append(vals, v)
		valSymbols = append(valSymbols, "?")
	}
	keyQ = strings.Join(keys, ",")
	valQ = strings.Join(valSymbols, ",")
	return keyQ, valQ, vals
}
