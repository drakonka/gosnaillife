-- +goose Up
CREATE TABLE foo (
  foo_id int UNSIGNED NOT NULL AUTO_INCREMENT,
  name varchar(50),
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  PRIMARY KEY(foo_id)
);

INSERT INTO foo VALUES
(10, 'Test Name', current_time, current_time),
(11, 'Test Name 2', current_time, current_time);

-- +goose Down
DROP TABLE foo;