package tests

import (
	"database/sql"
	"fmt"
	mysql2 "github.com/go-sql-driver/mysql"
	"gitlab.com/drakonka/gosnaillife/common"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure"
	mysql3 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases/mysql"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/tests"
	"os"
	"testing"
)

const (
	tblName        = "foo"
	migrationsPath = "/server/lib/infrastructure/databases/mysql/tests/migrations"
)

func TestMain(m *testing.M) {
	defer testing2.TimeElapsed("MySQL infrastructure", testing2.ModerateThreshold)()
	ret := m.Run()
	os.Exit(ret)
}

func TestMySqlConnect(t *testing.T) {
	tu := tests.NewTestUtil()
	dbName := "test_mysql_connect"
	_, err := tu.CreateTestDB(dbName)
	if err != nil {
		t.Fatal(err)
	}
	defer tu.DeleteTestDB(true, dbName)
	mysql := mysql3.MySql{}
	app, err := infrastructure.Init(common.CLI)
	if err != nil {
		t.Fatal(err)
	}
	DBConfig := app.Configuration.DBconfig
	mysql.Configure(DBConfig.DBUser, DBConfig.DBPass, DBConfig.DBHost, DBConfig.DBPort, dbName)
	err = mysql.Connect()
	if err != nil {
		fmt.Println("TestMySqlConnect error: " + err.Error())
		t.Error("Test failed")
	}
}

func TestMySqlInsert(t *testing.T) {
	testCases := []struct {
		tcname string
		id     string
		name   string
		want   error
	}{
		{"WithId", "1", "testname", nil},
		{"WithoutId", "", "testname2", nil},
		{"WithoutId2", "", "randomname", nil},
		{"WithoutId3", "", "testname3", nil},
		{"SpecialCharacters", "", "Bob's Burgers", nil},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s in %s", tc.id, tc.name), func(t *testing.T) {
			dbName := "TestMySqlInsert_" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)
			err = tu.RunDbMigrations(true, migrationsPath)
			if err != nil {
				t.Fatal(err)
			}

			m := make(map[string]string)
			if tc.id != "" {
				m["foo_id"] = tc.id
			}
			m["name"] = tc.name

			_, err = mysql.Insert(tblName, m)
			if err != tc.want {
				t.Fatal(testing2.TCFailedMsg("MySql Test", tc.tcname, err))

			}
		})
	}
}

func TestMySqlUpdate(t *testing.T) {
	testCases := []struct {
		tcname   string
		pkey     string
		toupdate map[string]string
		want     *mysql3.MySqlError
	}{
		{"UpdateOneField", "10", map[string]string{"name": "testname"}, nil},
		{"NonexistingRecord", "31", map[string]string{"name": "badname"}, nil},
		{"EmptyId", "", map[string]string{"name": "badname"}, new(mysql3.MySqlError)},
		{"NoFields", "10", map[string]string{}, nil},
		{"SpecialChars", "10", map[string]string{"name": "Bob's Name"}, nil},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s in %s", tc.tcname, tc.toupdate), func(t *testing.T) {
			dbName := "TestMySqlUpdate" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)
			err = tu.RunDbMigrations(true, migrationsPath)
			if err != nil {
				t.Fatal(err)
			}

			err = mysql.Update(tblName, tc.pkey, tc.toupdate)
			if tc.want == nil && err != nil {
				t.Errorf("Test case %s failed: %s", tc.tcname, err.Error())
			} else if tc.want != nil {
				testerr := testing2.TestErrorType(err, tc.want)
				if testerr != nil {
					t.Error(testerr)
				}
			}
		})
	}
}

func TestMySqlGetRows(t *testing.T) {
	testCases := []struct {
		tcname      string
		where       string
		params      []interface{}
		fields      []string
		wantedCount int
		wantedErr   error
	}{
		{"GetOneResult", "name=?", []interface{}{"Test Name"}, []string{""}, 1, nil},
		{"GetTwoResults", "name LIKE ?", []interface{}{"%name%"}, []string{"*"}, 2, nil},
		{"GetNoResults", "name=?", []interface{}{"nonsense"}, []string{}, 0, nil},
		{"GetBadColumn", "wat=?", []interface{}{"nonsense"}, []string{}, 0, new(mysql2.MySQLError)},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s where %s", tc.tcname, tc.where), func(t *testing.T) {
			dbName := "TestMySqlGetRows_" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)
			err = tu.RunDbMigrations(true, migrationsPath)
			if err != nil {
				t.Fatal(err)
			}

			results, err := mysql.GetRows(tblName, tc.where, tc.params, tc.fields)
			if tc.wantedErr == nil {
				if err != nil {
					t.Errorf("Unexpected error: %s", err)
				} else {
					rowcount := len(results)
					if tc.wantedCount != rowcount {
						t.Errorf("Expected %d rows, got %d", tc.wantedCount, rowcount)
					}
				}
			} else {
				testerr := testing2.TestErrorType(err, tc.wantedErr)
				if testerr != nil {
					t.Error(testerr)
				}
			}
		})
	}
}

func TestMySqlGetRow(t *testing.T) {
	testCases := []struct {
		tcname      string
		pkeyval     string
		fields      []string
		wantedCount int
		wantedErr   error
	}{
		{"GetOneResult", "10", []string{""}, 1, nil},
		{"GetNoResults", "80", []string{""}, 0, nil},
		{"GetBadColumn", "sdf", []string{}, 0, nil},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s where %s", tc.tcname, tc.pkeyval), func(t *testing.T) {
			dbName := "TestMySqlGetRow_" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)
			err = tu.RunDbMigrations(true, migrationsPath)
			if err != nil {
				t.Fatal(err)
			}

			row, err := mysql.GetRow(tblName, tc.pkeyval, tc.fields)
			if tc.wantedErr == nil {
				if err != nil {
					t.Errorf("Unexpected error: %s", err)
				} else {
					if tc.wantedCount > 0 && row == nil {
						t.Errorf("Expected %d rows, got none", tc.wantedCount)
					} else if tc.wantedCount == 0 && len(row) > 0 {
						t.Errorf("Expected no rows, got one")
					}
				}
			} else {
				testerr := testing2.TestErrorType(err, tc.wantedErr)
				if testerr != nil {
					t.Error(testerr)
				}
			}
		})
	}
}

func TestMySqlGetCount(t *testing.T) {
	testCases := []struct {
		tcname      string
		where       string
		params      []interface{}
		wantedCount int
	}{
		{"GetSome", "name LIKE ?", []interface{}{"%name%"}, 2},
		{"GetNone", "name = ?", []interface{}{"liger"}, 0},
	}

	for _, tc := range testCases {
		t.Run(tc.tcname, func(t *testing.T) {
			dbName := "TestMySqlGetCount_" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)
			err = tu.RunDbMigrations(true, migrationsPath)
			if err != nil {
				t.Fatal(err)
			}
			count, err := mysql.GetCount(tblName, tc.where, tc.params)
			if err != nil {
				t.Errorf("GetCount error: %s", err)
			} else if tc.wantedCount != count {
				msg := fmt.Sprintf("Expected count: %d, actual count: %d", tc.wantedCount, count)
				t.Error(msg)
			}
		})
	}
}

func TestMySqlDeleteRow(t *testing.T) {
	testCases := []struct {
		tcname      string
		pkeyval     string
		wantedCount int64
		wantedErr   error
	}{
		{"DeleteOne", "10", 1, nil},
		{"DeleteNone", "80", 0, nil},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestMySqlGetRows Test case %s.", tc.tcname)
		t.Run(fmt.Sprintf("%s pkeyval %s", tc.tcname, tc.pkeyval), func(t *testing.T) {
			dbName := "TestMySqlDeleteRow_" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)
			err = tu.RunDbMigrations(true, migrationsPath)
			if err != nil {
				t.Fatal(err)
			}
			rowcount, err := mysql.DeleteRow(tblName, tc.pkeyval)
			if tc.wantedErr == nil {
				if err != nil {
					t.Errorf("Unexpected error: %s", err)
				} else {
					if rowcount != tc.wantedCount {
						t.Errorf("Expected %d rows, got none", tc.wantedCount)
					}
				}
			} else {
				testerr := testing2.TestErrorType(err, tc.wantedErr)
				if testerr != nil {
					t.Error(testerr)
				}
			}
		})
	}
}

func TestMySqlDeleteRows(t *testing.T) {
	testCases := []struct {
		tcname      string
		where       string
		params      []interface{}
		wantedCount int64
		wantedErr   error
	}{
		{"DelOneResult", "name=?", []interface{}{"Test Name"}, 1, nil},
		{"DelTwoResults", "name LIKE ?", []interface{}{"%Name%"}, 2, nil},
		{"DelNoResults", "name=?", []interface{}{"nonsense"}, 0, nil},
		{"DelNoWhere", "", []interface{}{"testname2"}, 0, nil},
		{"DelNoParams", "name=?", []interface{}{}, 0, new(mysql3.MySqlError)},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s where %s", tc.tcname, tc.where), func(t *testing.T) {
			dbName := "TestMySqlDeleteRows_" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)
			err = tu.RunDbMigrations(true, migrationsPath)
			if err != nil {
				t.Fatal(err)
			}

			rowcount, err := mysql.DeleteRows(tblName, tc.where, tc.params)
			if tc.wantedErr == nil {
				if err != nil {
					t.Errorf("Unexpected error: %s", err)
				} else {
					if tc.wantedCount == 999 && rowcount <= 1 {
						t.Errorf("Expected more than 1 deleted row, got %d", rowcount)
					} else if tc.wantedCount != 999 && rowcount != tc.wantedCount {
						t.Errorf("Expected %d rows, got %d", tc.wantedCount, rowcount)
					}
				}
			} else {
				testerr := testing2.TestErrorType(err, tc.wantedErr)
				if testerr != nil {
					t.Error(testerr)
				}
			}
		})
	}
}

func TestMySqlCreateTable(t *testing.T) {
	testCases := []struct {
		tcname    string
		tablename string
		pkey      string
		cols      []string
		wantedErr error
	}{
		{"CreateTestTable", "TestSnailTable", "test_id", []string{"test_id INT NOT NULL AUTO_INCREMENT", "user_id VARCHAR(20)"}, nil},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s where table name is %s", tc.tcname, tc.tablename), func(t *testing.T) {
			dbName := "TestMySqlDeleteRows_" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)

			err = mysql.CreateTable(tc.tablename, tc.pkey, tc.cols)
			if tc.wantedErr == nil {
				if err != nil {
					t.Errorf("Unexpected error: %s", err)
				}
			} else {
				testerr := testing2.TestErrorType(err, tc.wantedErr)
				if testerr != nil {
					t.Error(testerr)
				}
			}
		})
	}
}

func TestMySqlTransactions(t *testing.T) {
	testCases := []struct {
		tcname           string
		insertTableName1 string
		insertMap1       map[string]string
		insertTableName2 string
		insertMap2       map[string]string
		expectCommit     bool
	}{
		{
			tcname:           "FailedTransaction",
			insertTableName1: "foo",
			insertMap1:       map[string]string{"name": "Tiger"},
			insertTableName2: "foo",
			insertMap2:       map[string]string{"foo_id": "abc"},
			expectCommit:     false,
		},
		{
			tcname:           "SucceededTransaction",
			insertTableName1: "foo",
			insertMap1:       map[string]string{"name": "Cichlid"},
			insertTableName2: "foo",
			insertMap2:       map[string]string{"name": "Bass"},
			expectCommit:     true,
		},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("TestCase %s", tc.tcname), func(t *testing.T) {
			dbName := "TestMySqltransactions_" + tc.tcname
			tu := tests.NewTestUtil()
			mysql, err := tu.CreateTestDB(dbName)
			if err != nil {
				t.Fatal(err)
			}
			defer tu.DeleteTestDB(true, dbName)
			err = tu.RunDbMigrations(true, migrationsPath)
			if err != nil {
				t.Fatal(err)
			}

			_ = mysql.Transact(func(tx *sql.Tx) error {
				_, err = mysql.Insert(tc.insertTableName1, tc.insertMap1)
				if err != nil {
					return err
				}
				_, err = mysql.Insert(tc.insertTableName2, tc.insertMap2)
				return err
			})

			for k, v := range tc.insertMap2 {
				num, _ := mysql.GetCount(tc.insertTableName2, fmt.Sprintf("%s = ?", k), []interface{}{v})
				if !tc.expectCommit {
					if num > 0 {
						t.Errorf("Expected transaction to have been rolled back but found %d results", num)
					}
				} else {
					if num <= 0 && tc.expectCommit {
						t.Errorf("Expected transaction to have been committed back but found %d results", num)
					}
				}
			}

		})
	}
}
