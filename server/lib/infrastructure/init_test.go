package infrastructure

import (
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"reflect"
	"testing"
)

func TestAppInit(t *testing.T) {
	expected := reflect.TypeOf(&env.Application{})
	actual, err := Init(common.CLI)
	if err != nil {
		t.Fatal(err)
	}
	if reflect.TypeOf(actual) != expected {
		t.Error("AppInit test failed")
	}
}
