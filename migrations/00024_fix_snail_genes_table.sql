-- +goose Up
ALTER TABLE snail_genes CHANGE COLUMN gene_id gene_id varchar(25) NOT NULL;
ALTER TABLE snail_genes MODIFY COLUMN allele_id int UNSIGNED NOT NULL AUTO_INCREMENT;


-- +goose Down
ALTER TABLE snail_genes CHANGE COLUMN gene_id gene_id int;
ALTER TABLE snail_genes MODIFY COLUMN qllele_id int UNSIGNED NOT NULL;
