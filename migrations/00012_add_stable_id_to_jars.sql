-- +goose Up
ALTER TABLE jars ADD COLUMN stable_id int UNSIGNED;

-- +goose Down
ALTER TABLE jars DROP COLUMN stable_id;
