-- +goose Up
ALTER TABLE snails ADD COLUMN pos_z SMALLINT;
ALTER TABLE snails ADD COLUMN target_pos_z SMALLINT;


-- +goose Down
ALTER TABLE snails DROP COLUMN pos_z;
ALTER TABLE snails DROP COLUMN target_pos_z;
