-- +goose Up
CREATE TABLE stables (
  stable_id int UNSIGNED NOT NULL PRIMARY KEY,
  owner_id int UNSIGNED NOT NULL,
  name varchar(50),
  brand varchar(5),
  description varchar(500),
  latitude double,
  longitude double,
  city varchar(50),
  country varchar(50),
  current_temp float,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  tempp_updated_at TIMESTAMP DEFAULT NOW()
);

-- +goose Down
DROP TABLE stables;