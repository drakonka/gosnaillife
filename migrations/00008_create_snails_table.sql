-- +goose Up
CREATE TABLE snails (
  snail_id int UNSIGNED NOT NULL PRIMARY KEY,
  owner_id int UNSIGNED NOT NULL DEFAULT 0,
  name varchar(20) UNIQUE,
  parent_jar_id int UNSIGNED,
  current_jar_id int UNSIGNED,
  birth_stable_id int UNSIGNED,
  pos_x SMALLINT,
  pos_y SMALLINT,
  target_pos_x SMALLINT,
  target_pos_y SMALLINT,
  birth_date DATETIME,
  death_date DATETIME,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
);

-- +goose Down
DROP TABLE snails;