-- +goose Up
ALTER TABLE stables MODIFY COLUMN stable_id INT auto_increment;

-- +goose Down
ALTER TABLE stables MODIFY COLUMN stable_id INT;