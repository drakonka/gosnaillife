-- +goose Up
ALTER TABLE snails ADD COLUMN immunity_perc FLOAT;
ALTER TABLE snails ADD COLUMN immunity_rate_base FLOAT;
ALTER TABLE snails ADD COLUMN immunity_max FLOAT;

-- +goose Down
ALTER TABLE snails DROP COLUMN immunity_perc;
ALTER TABLE snails DROP COLUMN immunity_rate_base;
ALTER TABLE snails DROP COLUMN immunity_max;