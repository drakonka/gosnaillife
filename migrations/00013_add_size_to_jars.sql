-- +goose Up
ALTER TABLE jars ADD COLUMN width_cm smallint UNSIGNED;
ALTER TABLE jars ADD COLUMN height_cm smallint UNSIGNED;
ALTER TABLE jars ADD COLUMN depth_cm smallint UNSIGNED;


-- +goose Down
ALTER TABLE jars DROP COLUMN width_cm;
ALTER TABLE jars DROP COLUMN height_cm;
ALTER TABLE jars DROP COLUMN depth_cm;
