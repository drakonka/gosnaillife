-- +goose Up
ALTER TABLE owners MODIFY COLUMN owner_id INT auto_increment;
ALTER TABLE jars MODIFY COLUMN jar_id INT auto_increment;
ALTER TABLE genes MODIFY COLUMN gene_id INT auto_increment;
ALTER TABLE organs MODIFY COLUMN organ_id INT auto_increment;
ALTER TABLE snails MODIFY COLUMN snail_id INT auto_increment;

-- +goose Down
ALTER TABLE owners MODIFY COLUMN owner_id INT;
ALTER TABLE jars MODIFY COLUMN jar_id INT;
ALTER TABLE genes MODIFY COLUMN gene_id INT;
ALTER TABLE organs MODIFY COLUMN organ_id INT;
ALTER TABLE snails MODIFY COLUMN snail_id INT;
