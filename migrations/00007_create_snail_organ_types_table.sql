-- +goose Up
CREATE TABLE organ_types (
  organ_type_id varchar(25) NOT NULL PRIMARY KEY,
  ideal_quant tinyint
);

INSERT INTO organ_types VALUES
  ('inner_shell', 1),
  ('outer_shell', 1),
  ('eye', 2),
  ('tentacle', 2),
  ('heart', 1),
  ('foot', 1),
  ('kidney', 1),
  ('lung', 1),
  ('gonad', 1),
  ('stomach', 1),
  ('mucus_gland', 1),
  ('vagina', 1),
  ('dart', 1),
  ('brain', 1);


-- +goose Down
DROP TABLE organ_types;