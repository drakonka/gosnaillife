-- +goose Up
ALTER TABLE jars DROP COLUMN owner_id;


-- +goose Down
ALTER TABLE jars ADD COLUMN owner_id int UNSIGNED;
