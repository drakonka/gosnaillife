-- +goose Up
ALTER TABLE organs MODIFY COLUMN weight_mg float unsigned;

-- +goose Down
ALTER TABLE organs MODIFY COLUMN weight_mg smallint unsigned;
