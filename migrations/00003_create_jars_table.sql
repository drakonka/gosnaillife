-- +goose Up
CREATE TABLE jars (
  jar_id int UNSIGNED NOT NULL PRIMARY KEY,
  owner_id int UNSIGNED NOT NULL,
  name varchar(20),
  description varchar(250),
  is_breeding_enabled bool DEFAULT TRUE,
  base_temp float,
  temp_mod float,
  temp_override float,
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW()
);

-- +goose Down
DROP TABLE jars;