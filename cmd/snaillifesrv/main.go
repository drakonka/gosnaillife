package main

import "fmt"
import (
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"gitlab.com/drakonka/gosnaillife/server/lib/interfaces/cli/commands"
	"os"
)

var App env.Application

func main() {
	common.InitLog("")
	defer common.Log.Sync()
	if err := commands.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
