#!/usr/bin/env bash
ls -l
/bin/bash ../setup/deploy/srv/wait-for-it.sh 172.19.0.2:3306 -t 120
echo getting goose status...
goose mysql "root:root@tcp(172.19.0.2:3306)/snaillifego?parseTime=true" status
echo running migrations...
goose mysql "root:root@tcp(172.19.0.2:3306)/snaillifego?parseTime=true" up

echo "Setting up auth config"
AUTH='{
  "Providers": [
  {
    "Name": "MySql",
    "Properties": {
      "database": "snaillifego",
      "table_name": "users",
      "session_length": "3600"
    }
  }]
}'

echo $AUTH >> ../server/config/dev/auth.json

echo starting server...
if [ -n "$SNAILLIFESLV_ARGS" ]; then
  echo found args to pass to snaillifesrv
  ARGS=${SNAILLIFESRV_ARGS}
else
  echo using default args
  ARGS=${SNAILLIFESRV_DEFAULT_ARGS}
fi
echo running snaillifesrv with args: $ARGS
snaillifesrv $ARGS


