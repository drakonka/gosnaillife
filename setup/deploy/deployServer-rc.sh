#!/bin/sh
export PS4='\t '
set -x;
echo "Building and installing SnailLife server"
echo "Current dir" $PWD
go version

echo "Running gofmt"
cd ../../
gofmt -s -w server

cd server/lib/infrastructure/auth/cli

echo "Building SnailLifeSrv"
cd ../../../../../cmd/snaillifesrv;
go install

cd ../../
set -e

echo "Running tests"
# CVPKG=$(go list ./... | grep -v mocks | tr '\n' ',')
CVPKG=$(go list ./... | grep -v -e mocks -e snaillifecli -e snaillifesrv | tr '\n' ',')
go test -race -v -coverpkg $CVPKG -coverprofile allcoverage-rc.out ./...
set +x;