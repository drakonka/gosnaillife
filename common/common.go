package common

type ClientType uint8

const (
	CLI ClientType = iota + 1
	Web
)
