package tests

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"testing"
)

type TValidatable struct {
	Value interface{}
}

func (v *TValidatable) GetRules() []validator.Rule {
	lengthRule := validator.NewLengthRule(2, 5, v.Value, "Value")
	rules := []validator.Rule{}
	rules = append(rules, &lengthRule)
	return rules
}

func (v *TValidatable) Validate() error {
	rules := v.GetRules()
	for _, r := range rules {
		err := r.Validate("teststuct")
		if err != nil {
			return err
		}
	}
	return nil
}

func (v *TValidatable) ValidateSpecific(fields ...string) []error {
	var errors []error
	for _, r := range v.GetRules() {
		fieldName := r.GetFieldName()
		if util.StrIndexOf(fields, fieldName) > -1 {
			err := r.Validate("teststruct")
			if err != nil {
				errors = append(errors, err)
			}
		}
	}
	if len(errors) > 0 {
		return errors
	}
	return nil
}

func TestValidatable(t *testing.T) {
	testCases := []struct {
		tcname string
		val    interface{}
		want   *error
	}{
		{"ValidatbleIntPass", 24, nil},
		{"ValidatableIntFail", 2342523525, new(error)},
		{"ValidatableStrPass", "gull", nil},
		{"ValidatableStrFail", "badger", new(error)},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestValidatable Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("value: %v", tc.val), func(t *testing.T) {
			validatable := TValidatable{Value: tc.val}
			err := validatable.Validate()
			if err == nil && tc.want != nil {
				msg := fmt.Sprintf("Expected an error, didn't get one")
				t.Error(msg)
			} else if err != nil && tc.want == nil {
				msg := fmt.Sprintf("Invalid error: %s", err)
				t.Error(msg)
			}
		})
	}
}

func TestValidateSpecific(t *testing.T) {
	testCases := []struct {
		tcname   string
		val      interface{}
		errCount int
	}{
		{"ValidatbleIntPass", 24, 0},
		{"ValidatableIntFail", 2342523525, 1},
		{"ValidatableStrPass", "gull", 0},
		{"ValidatableStrFail", "badger", 1},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestValidateSpecific Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("value: %v", tc.val), func(t *testing.T) {
			validatable := TValidatable{Value: tc.val}
			errs := validatable.ValidateSpecific("Value")
			if len(errs) != tc.errCount {
				msg := fmt.Sprintf("%s; Expected %d errors, got %d; %v", tc.tcname, tc.errCount, len(errs), errs)
				t.Error(msg)
			}
		})
	}
}

func TestLengthRule(t *testing.T) {
	testCases := []struct {
		tcname string
		min    int
		max    int
		val    interface{}
		want   *error
	}{
		{"IntPass", 2, 5, 70, nil},
		{"IntFail", 2, 5, 9, new(error)},
		{"StrPass", 1, 20, "Test String", nil},
		{"StrFail", 1, 20, "", new(error)},
		{"SlicePass", 5, 7, []int{1, 5, 2, 6, 7, 3}, nil},
		{"SliceFail", 5, 7, []int{6, 2, 1}, new(error)},
		{"MapPss", 5, 8, map[string]string{"a": "a", "b": "b", "c": "c", "d": "d", "e": "e", "f": "f"}, nil},
		{"MapFail", 3, 3, map[string]string{"a": "a", "b": "b"}, new(error)},
		{"StructFail", 5, 10, validator.LengthRule{}, new(error)},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestLengthRule Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("min: %d, max: %d", tc.min, tc.max), func(t *testing.T) {
			rule := validator.NewLengthRule(tc.min, tc.max, tc.val, tc.tcname)
			err := rule.Validate(tc.tcname)
			if err != nil {
				fmt.Println(err.Error())
			}
			if err == nil && tc.want != nil {
				msg := fmt.Sprintf("Expected an error, didn't get one")
				t.Error(msg)
			} else if err != nil && tc.want == nil {
				msg := fmt.Sprintf("Invalid error: %s", err)
				t.Error(msg)
			}
		})
	}
}

func TestEmailRule(t *testing.T) {
	testCases := []struct {
		tcname string
		val    interface{}
		want   *error
	}{
		{"EmailIntFail", 9, new(error)},
		{"EmailStrPass", "test@test.com", nil},
		{"EmailStrNoAt", "testingyo", new(error)},
		{"EmailStrBadDomain", "windex@wat", new(error)},
		{"EmailSliceFail", []int{6, 2, 1}, new(error)},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestLengthRule Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("email: %v", tc.val), func(t *testing.T) {
			rule := validator.NewEmailRule(tc.val, tc.tcname)
			err := rule.Validate(tc.tcname)
			if err != nil {
				fmt.Println(err.Error())
			}
			if err == nil && tc.want != nil {
				msg := fmt.Sprintf("Test case %s; Expected an error, didn't get one", tc.tcname)
				t.Error(msg)
			} else if err != nil && tc.want == nil {
				msg := fmt.Sprintf("Test case %s; Invalid error: %s", tc.tcname, err)
				t.Error(msg)
			}
		})
	}
}

func TestSelectionRule(t *testing.T) {
	testCases := []struct {
		tcname  string
		val     interface{}
		choices []interface{}
		want    *error
	}{
		{tcname: "SelectionStrFail", val: "cat", choices: []interface{}{"dog", "bird", "red cat"}, want: new(error)},
		{tcname: "SelectionStrPass", val: "cat", choices: []interface{}{"dog", "bird", "cat", "red cat"}, want: nil},
		{tcname: "SelectionIntFail", val: 8, choices: []interface{}{88, 9, 10, 11}, want: new(error)},
		{tcname: "SelectionIntPass", val: 3, choices: []interface{}{88, 9, 3, 10, 11}, want: nil},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestSelectionRule Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("val: %v", tc.val), func(t *testing.T) {
			rule := validator.NewSelectionRule(tc.val, tc.choices, tc.tcname)
			err := rule.Validate(tc.tcname)
			if err != nil {
				fmt.Println(err.Error())
			}
			if err == nil && tc.want != nil {
				msg := fmt.Sprintf("Test case %s; Expected an error, didn't get one", tc.tcname)
				t.Error(msg)
			} else if err != nil && tc.want == nil {
				msg := fmt.Sprintf("Test case %s; Invalid error: %s", tc.tcname, err)
				t.Error(msg)
			}
		})
	}
}

func TestMinMaxRule(t *testing.T) {
	testCases := []struct {
		tcname string
		min    float64
		max    float64
		val    interface{}
		want   *error
	}{
		{"IntPass", 2, 5, 3, nil},
		{"IntFail", 2, 5, 9, new(error)},
		{"IntPassWithNoMin", -999, 0, -11123, nil},
		{"IntPassWithNoMax", 2, -999, 24325, nil},
		{"StrFail", 1, 20, "", new(error)},
		{"SliceFail", 5, 7, []int{6, 2, 1}, new(error)},
	}

	for _, tc := range testCases {
		fmt.Printf("Running TestLengthRule Test Case %s.", tc.tcname)
		t.Run(fmt.Sprintf("min: %f, max: %f", tc.min, tc.max), func(t *testing.T) {
			rule := validator.NewMinMaxRule(tc.min, tc.max, tc.val, tc.tcname)
			err := rule.Validate(tc.tcname)
			if err != nil {
				fmt.Println(err.Error())
			}
			if err == nil && tc.want != nil {
				msg := fmt.Sprintf("Expected an error, didn't get one")
				t.Error(msg)
			} else if err != nil && tc.want == nil {
				msg := fmt.Sprintf("Invalid error: %s", err)
				t.Error(msg)
			}
		})
	}
}
