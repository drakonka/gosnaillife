package validator

import (
	"fmt"
	"reflect"
)

type MinMaxRule struct {
	rule
	min float64
	max float64
	val interface{}
}

func NewMinMaxRule(min float64, max float64, val interface{}, fieldName string) MinMaxRule {
	r := MinMaxRule{
		min: min,
		max: max,
		val: val,
	}
	r.fieldName = fieldName
	return r
}

func (r *MinMaxRule) GetMessage(length float64) string {
	return fmt.Sprintf("%s field %s size invalid. Number is %f; min: %f, max: %f", r.objectName, r.fieldName, length, r.min, r.max)
}

func (r *MinMaxRule) Validate(name string) error {
	r.objectName = name

	val, err := getNumericAsFloat(r.val)
	if err != nil {
		v := reflect.ValueOf(r.val)
		return fmt.Errorf("cannot get the size of %v", v.Kind())
	}
	if (r.max != -999 && val > r.max) || (r.min != -999 && val < r.min) {
		return &ValidationError{
			rulename: name,
			Err:      r.GetMessage(val),
		}
	}
	return nil
}
