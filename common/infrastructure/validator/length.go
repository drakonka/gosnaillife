package validator

import (
	"fmt"
	"reflect"
)

type LengthRule struct {
	rule
	min int
	max int
	val interface{}
}

func NewLengthRule(min int, max int, val interface{}, fieldName string) LengthRule {
	r := LengthRule{
		min: min,
		max: max,
		val: val,
	}
	r.fieldName = fieldName
	return r
}

func (r *LengthRule) GetMessage(length int) string {
	return fmt.Sprintf("%s %s length invalid. Length is %d; min: %d, max: %d", r.objectName, r.fieldName, length, r.min, r.max)
}

func (r *LengthRule) Validate(name string) error {
	r.objectName = name
	v := reflect.ValueOf(r.val)

	validtypes := map[reflect.Kind]struct{}{
		reflect.String: {},
		reflect.Slice:  {},
		reflect.Map:    {},
		reflect.Array:  {},
	}

	if _, ok := validtypes[v.Kind()]; ok {
		if v.Len() > r.max || v.Len() < r.min {
			return &ValidationError{
				rulename: name,
				Err:      r.GetMessage(v.Len()),
			}
		}
		return nil
	}

	if isNumeric(r.val) {
		val := fmt.Sprintf("%v", r.val)
		if len(val) > r.max || len(val) < r.min {
			return &ValidationError{
				rulename: name,
				Err:      r.GetMessage(len(val)),
			}
		}
		return nil
	}
	return fmt.Errorf("cannot get the length of %v", v.Kind())
}
