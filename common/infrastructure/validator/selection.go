package validator

import (
	"fmt"
)

type SelectionRule struct {
	rule
	choices []interface{}
	val     interface{}
}

func NewSelectionRule(val interface{}, choices []interface{}, fieldName string) SelectionRule {
	r := SelectionRule{
		choices: choices,
		val:     val,
	}
	r.fieldName = fieldName
	return r
}

func (r *SelectionRule) GetMessage() string {
	return fmt.Sprintf("%s %s invalid. Value is %v, valid choices are %v", r.objectName, r.val, r.val, r.choices)
}

func (r *SelectionRule) Validate(name string) error {
	r.objectName = name
	for _, c := range r.choices {
		if r.val == c {
			return nil
		}
	}
	return &ValidationError{
		rulename: name,
		Err:      r.GetMessage(),
	}
}
