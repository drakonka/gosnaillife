package restapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"io/ioutil"
	http3 "net/http"
)

type snailRes struct {
	HttpStatus http.HttpStatus `json:"http_status"`
	Snails     []snail.Snail   `json:"snails"`
}

type SnailReq struct {
	request
	Snail snail.Snail `json:"snail"`
}

type ExportedSnailRes struct {
	snailRes *snailRes
}

func (r *ExportedSnailRes) JsonEncode(w http3.ResponseWriter) error {
	return json.NewEncoder(w).Encode(r.snailRes)
}

func (r *ExportedSnailRes) HttpStatus() http.HttpStatus {
	return r.snailRes.HttpStatus
}

func (r *ExportedSnailRes) SetHttpStatus(s http.HttpStatus) {
	if r.snailRes == nil {
		r.snailRes = &snailRes{}
	}
	r.snailRes.HttpStatus = s
}

func (r *ExportedSnailRes) SetModels(models []repo.Model) {
	if r.snailRes == nil {
		r.snailRes = &snailRes{}
	}
	for _, m := range models {
		r.snailRes.Snails = append(r.snailRes.Snails, *m.(*snail.Snail))
	}
}

func (r *ExportedSnailRes) Models() []repo.Model {
	var ms = make([]repo.Model, len(r.snailRes.Snails))
	for i, s := range r.snailRes.Snails {
		ms[i] = &s
	}
	return ms
}

func GetSnailRes(res *http3.Response) (*ExportedSnailRes, error) {
	defer res.Body.Close()
	resp := snailRes{}
	eRes := ExportedSnailRes{snailRes: &resp}
	eRes.snailRes = &resp
	body, err := ioutil.ReadAll(res.Body)
	if err == nil {
		json.Unmarshal(body, &resp)
	} else {
		eRes.snailRes.HttpStatus.StatusCode = http3.StatusNotFound
		eRes.snailRes.HttpStatus.Message = err.Error()
	}
	return &eRes, err
}

func GetSnailsFromSnailRes(res *http3.Response) ([]snail.Snail, error) {
	if res.StatusCode != http3.StatusOK {
		msg := fmt.Sprintf("Invalid http status: %d", res.StatusCode)
		return nil, errors.New(msg)
	}

	eRes, err := GetSnailRes(res)
	if err == nil && res.StatusCode == http3.StatusOK {
		return eRes.snailRes.Snails, err
	}
	error2.HandleErr(err, "GetSnailsFromSnailRes")
	return nil, err
}
