package restapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain/user"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"io/ioutil"
	http2 "net/http"
)

type UserRes struct {
	Httpstatus  http.HttpStatus `json:"http_status"`
	Id          string          `json:"_id"`
	Username    string          `json:"username"`
	User        user.User       `json:"user"`
	AccessToken string          `json:"access_token"`
	IdToken     string          `json:"id_token"`
}

func (u *UserRes) SetId(id string) {
	u.Id = id
}

func (u *UserRes) SetHttpStatus(s http.HttpStatus) {
	u.Httpstatus = s
}

func (u *UserRes) SetAccessToken(t string) {
	u.AccessToken = t
}

type UserReq struct {
	request
	Username   string    `json:"username"`
	Password   string    `json:"password"`
	User       user.User `json:"user"`
	Token      string    `json:"authorization"`
	Connection string    `json:"connection"`
}

func (r *UserRes) JsonEncode(w http2.ResponseWriter) error {
	return json.NewEncoder(w).Encode(r)
}

func (r *UserRes) SetModels(models []repo.Model) {

}

func (r *UserRes) Models() []repo.Model {
	return []repo.Model{}
}

func (r *UserRes) HttpStatus() http.HttpStatus {
	return r.Httpstatus
}

func GetUserFromUserRes(res *http2.Response) (*user.User, error) {
	if res.StatusCode != http2.StatusOK {
		msg := fmt.Sprintf("Invalid http status: %d", res.StatusCode)
		return nil, errors.New(msg)
	}
	defer res.Body.Close()
	var body []byte
	var err error
	body, err = ioutil.ReadAll(res.Body)
	if err != nil {
		error2.HandleErr(err, "GetUserFromUserRes Error")
		return nil, err
	}
	if len(body) == 0 {
		err = errors.New("body is empty - has it already been consumed")
		error2.HandleErr(err, "GetUserFromUserRes Error")
		return nil, err
	}
	userResp := UserRes{}

	err = json.Unmarshal(body, &userResp)
	if err != nil {
		return nil, err
	}

	user := user.User{
		Id:          userResp.Id,
		Email:       userResp.Username,
		AuthHeaders: map[string]string{"authorization": "Bearer " + userResp.AccessToken},
	}
	return &user, nil
}
