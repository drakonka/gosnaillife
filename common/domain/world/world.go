package world

import (
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/env"
	"time"
)

type World struct {
	quitChan  chan struct{}
	pauseChan chan bool
	paused    bool
}

func NewWorld(quitChan chan struct{}) (*World, error) {
	w := World{}
	w.quitChan = quitChan
	w.pauseChan = make(chan bool)
	return &w, nil
}

func (w *World) Pause() {
	w.pauseChan <- true
}

func (w *World) Resume() {
	w.pauseChan <- false
}

func (w *World) Start(app *env.Application) error {
	if w.quitChan == nil {
		msg := "This world does not have a quit channel. Initiate the world with NewWorld and give it a quit channel."
		return errors.New(msg)
	}
	// Get all living snails
	r, err := app.GetOrMakeRepo("snail", snail.NewRepo)
	if err != nil {
		error2.HandleErr(err, "startWorld")
		return err
	}

	go func() {
		for i := 0; ; i++ {
			select {
			case <-w.quitChan:
				return
			case paused := <-w.pauseChan:
				if paused != w.paused {
					w.paused = paused
				}
			default:
				if w.paused {
					common.Log.Warn("World is paused - not updating")
					time.Sleep(1 * time.Second)
					continue
				}
				err = updateAllLivingSnails(r.(*snail.Repo))
				error2.HandleErr(err, "update world")
				time.Sleep(5 * time.Second)
			}
		}
	}()
	return nil
}

func updateAllLivingSnails(r *snail.Repo) error {
	common.Log.Debug("Updating living snails")
	alive, err := r.GetAllLivingSnails()
	if err != nil {
		error2.HandleErr(err, "GetAllLivingSnails")
		return err
	}
	for _, s := range alive {
		common.Log.Infof("Updating %d (%s)", s.Id, s.Name)
		err := s.UpdateOrganism(true)
		if err != nil {
			error2.HandleErr(err, "UpdateOrganism")
			return err
		}
	}
	return nil
}
