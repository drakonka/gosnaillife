package domain

import (
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/common/util"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
)

type Model struct {
	repo2.Model
	dbTagMap map[string]string `db:"-"`
	Repo     repo2.Repo        `db:"-"`
}

func (m *Model) GetDbTagMap(t reflect.Type) map[string]string {
	if m.dbTagMap == nil || len(m.dbTagMap) == 0 {
		m.BuildDbTagMap(t)
	}
	return m.dbTagMap
}

func (m *Model) BuildDbTagMap(t reflect.Type) map[string]string {
	ms := make(map[string]string)
	m.buildDbTagToFieldMap(t, ms)
	m.dbTagMap = ms
	return ms
}

func (m *Model) Validate(rules []validator.Rule, name string) error {
	for _, r := range rules {
		err := r.Validate(name)
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *Model) ValidateSpecific(rules []validator.Rule, dbTagMap map[string]string, fields []string, objectName string) []error {
	var errors []error
	for _, r := range rules {
		fieldName := r.GetFieldName()

		doValidate := false
		if util.StrIndexOf(fields, fieldName) > -1 {
			doValidate = true
		} else {
			// the above may b Db tags...let's just try convert to fields just in case
			for _, v := range fields {
				if fn, ok := dbTagMap[v]; ok {
					if fn == fieldName {
						doValidate = true
						break
					}
				}
			}
		}
		if doValidate {
			err := r.Validate(objectName)
			if err != nil {
				errors = append(errors, err)
			}
		}
	}
	if len(errors) > 0 {
		return errors
	}
	return nil
}

func (m *Model) buildDbTagToFieldMap(t reflect.Type, ms map[string]string) {
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		fieldName := field.Name
		dbTag := field.Tag.Get("db")
		if len(dbTag) > 0 {
			ms[dbTag] = fieldName
		}
	}
}

// SetRepo sets a repository for this Model
func (m *Model) SetRepo(r repo2.Repo) {
	m.Repo = r
}
