package tests

import (
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"testing"
)

type repoTest interface {
	prep() error
	createInsertOneTestCases() []insertOneTestCase
	createDeleteOneTestCase() []deleteOneTestCase
	createSaveModelTestCases() []saveModelTestCase
	createUpdateModelTestCases() []updateModelTestCase
	createUpdateOneTestCases() []updateOneTestCase
	createGetManyTestCases() []getManyTestCase
	createGetManyWithDbErrTestCases() []getManyWithErrTestCase
	createGetOneTestCases() []getOneTestCase
	createGetOneWithDbErrTestCases() []getOneWithErrorTestCase
	createRepo(db databases.Database) repo.Repo
	createModel(params map[string]interface{}) (repo.Model, error)
	getIdKey() string
	getAdditionalTests() []func(t *testing.T)
}

type test struct {
	tablename string
	db        databases.Database
	idKey     string
}

func (t *test) getIdKey() string {
	return t.idKey
}

func (t *test) seed(rows []map[string]string) error {
	for _, row := range rows {
		_, err := t.db.Insert(t.tablename, row)
		if err != nil {
			return err
		}
	}
	return nil
}

type insertOneTestCase struct {
	tcname string
	model  interface{} // Make this anything to allow testing bad types
	want   error
}

type deleteOneTestCase struct {
	name  string
	model interface{}
	want  error
}

type saveModelTestCase insertOneTestCase

type updateOneTestCase struct {
	tcname     string
	idToUpdate interface{}
	fields     map[string]string
	want       error
}

type updateModelTestCase struct {
	tcname         string
	model          repo.Model
	fieldsToUpdate []string
	want           error
}

type getManyTestCase struct {
	tcname string
	where  string
	params []interface{}
	fields []string
	want   int
}

type getManyWithErrTestCase struct {
	tcname    string
	where     string
	params    []interface{}
	fields    []string
	wantedErr error
}

type getOneTestCase struct {
	tcname      string
	id          interface{}
	fields      []string
	wantedCount int
}

type getOneWithErrorTestCase struct {
	tcname    string
	id        interface{}
	fields    []string
	wantedErr error
}
