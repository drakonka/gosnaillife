package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/gene"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"testing"
)

type geneTest struct {
	test
}

func newGeneTest(db databases.Database) *geneTest {
	test := geneTest{}
	test.tablename = "snail_genes"
	test.db = db
	test.idKey = "allele_id"
	return &test
}

func (t *geneTest) createInsertOneTestCases() []insertOneTestCase {
	m1 := gene.SizeA1{}
	m1.Init()
	m1.GenerateRandomAlleles()
	a := m1.Alleles[0]
	a.SetOwnerId(500)

	return []insertOneTestCase{
		{"GeneInsert", a, nil},
	}
}

func (t *geneTest) createDeleteOneTestCase() []deleteOneTestCase {
	r, err := gene.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := gene.SizeA1{}
	m1.Init()
	m1.GenerateRandomAlleles()
	a := m1.Alleles[0].(*gene.Allele)
	a.SetRepo(r)

	return []deleteOneTestCase{
		{
			name:  "SuccessfulGeneDelete",
			model: a,
			want:  nil,
		},
	}
}

func (t *geneTest) createSaveModelTestCases() []saveModelTestCase {
	var testCases []saveModelTestCase
	for _, tc := range t.createInsertOneTestCases() {
		if _, ok := tc.model.(organism.Gene); ok {
			testCases = append(testCases, saveModelTestCase(tc))
		}
	}
	return testCases
}

func (t *geneTest) createUpdateOneTestCases() []updateOneTestCase {
	return []updateOneTestCase{
		{"GeneSuccessfulUpdate", "500", map[string]string{"allele_key": "X"}, nil},
		{"GeneValidationFailedUpdate", "500", map[string]string{"gene_id": "notarealgene"}, new(validator.ValidationError)},
	}
}

func (t *geneTest) createUpdateModelTestCases() []updateModelTestCase {
	/* r, err := gene.NewRepo(t.db)
	if err != nil {
		panic(err)
	} */
	m1 := gene.SizeA1{}
	m1.Init()
	m1.GenerateRandomAlleles()

	alleleToUpdate := m1.Alleles[0].(repo.Model)
	return []updateModelTestCase{
		{"GeneModelSuccessfulUpdate", alleleToUpdate, []string{"GeneId"}, &repo.RepoError{Err: "an Allele does not support modification after it is saved"}},
	}
}

func (t *geneTest) createGetManyTestCases() []getManyTestCase {
	return []getManyTestCase{
		{"AlleleGetOneWithAllProperties", "allele_id=?", []interface{}{"500"}, []string{}, 1},
		{"AlleleGetOneWithOneProperty", "allele_id=?", []interface{}{"500"}, []string{"snail_id"}, 1},
		{"AlleleGetManyWithAllProperties", "gene_id LIKE ? AND owner_id LIKE ?", []interface{}{"%size%", "988"}, []string{}, 2},
		{"AlleleGetManyWithOneProperty", "gene_id LIKE ? AND owner_id LIKE ?", []interface{}{"%size%", "988"}, []string{"gene_id"}, 2},
		{"AlleleGetNoneWithAllProperties", "allele_id=?", []interface{}{"303"}, []string{}, 0},
		{"AlleleGetNoneWithOneProperty", "allele_id=?", []interface{}{"303"}, []string{"name"}, 0},
	}
}

func (t *geneTest) createGetManyWithDbErrTestCases() []getManyWithErrTestCase {
	return []getManyWithErrTestCase{
		{"OrganGetManyWithDbErr", "gene_id=?", []interface{}{"500"}, []string{}, errors.New("Something went wrong")},
	}
}

func (t *geneTest) createGetOneTestCases() []getOneTestCase {
	return []getOneTestCase{
		{"OrganGetOneWithAllProperties", "500", []string{}, 1},
		{"OrganGetOneWithOneProperty", "500", []string{"gene_id"}, 1},
		{"OrganGetOneOneInvalid", "1033", []string{"gene_id"}, 0},
	}
}

func (t *geneTest) createGetOneWithDbErrTestCases() []getOneWithErrorTestCase {
	return []getOneWithErrorTestCase{
		{"OrganGetOneWithDbErr", "500", []string{}, errors.New("Something went wrong")},
	}
}

func (t *geneTest) prep() error {
	return t.seed()
}

func (t *geneTest) seed() error {
	fmt.Println("seeding gene")
	rows := []map[string]string{
		{
			"allele_id":  "500",
			"gene_id":    "size_a1",
			"owner_id":   "500",
			"allele_key": "S",
		},
		{
			"allele_id":  "501",
			"gene_id":    "size_a1",
			"owner_id":   "500",
			"allele_key": "s",
		},
		{
			"gene_id":    "size_a1",
			"owner_id":   "500",
			"allele_key": "S",
		},
		{
			"gene_id":    "size_a1",
			"owner_id":   "988",
			"allele_key": "S",
		},
		{
			"gene_id":    "size_a1",
			"owner_id":   "988",
			"allele_key": "s",
		},
	}
	return t.test.seed(rows)
}

func (t *geneTest) createRepo(db databases.Database) repo.Repo {
	repo, _ := gene.NewRepo(db)
	return repo
}

func (t *geneTest) createModel(params map[string]interface{}) (repo.Model, error) {
	model, err := gene.GetAlleleFromRowResult(params)
	return model, err
}

func (t *geneTest) getAdditionalTests() []func(tT *testing.T) {
	tests := []func(tT *testing.T){
		t.createRepoAndModelTest,
	}
	return tests
}

func (t *geneTest) createRepoAndModelTest(tT *testing.T) {
	testCases := []struct {
		tcname string
		db     databases.Database
		want   error
	}{
		{"OrganCreateRepoAndModelSuccess", t.db, nil},
		{"OrganCreateRepoNoDb", nil, new(repo.RepoError)},
	}

	for _, tc := range testCases {
		tT.Run(tc.tcname, func(tT *testing.T) {
			r, err := gene.NewRepo(tc.db)

			testerr := testing2.TestErrorType(tc.want, err)
			if testerr != nil {
				tT.Error(testing2.TCFailedMsg("GeneTest", tc.tcname, testerr))
			} else {
				m := r.NewModel()
				if m == nil {
					tT.Error(testing2.TCFailedMsg("GeneTest", tc.tcname, "model is nil"))
				}
			}
		})
	}
}
