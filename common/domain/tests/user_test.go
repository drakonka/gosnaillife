package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/domain/user"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/common/util"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"testing"
)

type userTest struct {
	test
}

func newUserTest(db databases.Database) *userTest {
	test := userTest{}
	test.tablename = "users"
	test.db = db
	test.idKey = "_id"
	return &test
}

func (t *userTest) createInsertOneTestCases() []insertOneTestCase {
	r, err := user.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &user.User{Id: "10", Email: "test@testtest.com", Password: "testpass"}
	m1.SetRepo(r)

	m2 := &user.User{Email: "rabbit@s.com", Password: "testpass2"}
	m2.SetRepo(r)

	return []insertOneTestCase{
		{"UserInsertWithId", m1, nil},
		{"UserInsertWithoutId", m2, nil},
		{"UserInsertWithBadType", &jar.Jar{Name: "Watajar"}, &repo.RepoError{}},
	}
}

func (t *userTest) createDeleteOneTestCase() []deleteOneTestCase {
	r, err := user.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &user.User{Email: "delete@s.com", Password: "testpass2"}
	m1.SetRepo(r)

	return []deleteOneTestCase{
		{
			name:  "SuccessfulUserDelete",
			model: m1,
			want:  nil,
		},
	}
}

func (t *userTest) createSaveModelTestCases() []saveModelTestCase {
	var testCases []saveModelTestCase
	for _, tc := range t.createInsertOneTestCases() {
		if m, ok := tc.model.(*user.User); ok {
			m.Id = ""

			rand, _ := util.GenRandStr(5)
			m.Email = rand + "@wat.io"
			testCases = append(testCases, saveModelTestCase(tc))
		}
	}
	return testCases
}

func (t *userTest) createUpdateOneTestCases() []updateOneTestCase {
	return []updateOneTestCase{
		{"SuccessfulUpdate", "500", map[string]string{"email": "newemail@test.com"}, nil},
		{"ValidationFailedUpdate", "500", map[string]string{"email": "o"}, new(validator.ValidationError)},
	}
}

func (t *userTest) createUpdateModelTestCases() []updateModelTestCase {
	r, err := user.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &user.User{Id: "500", Email: "reallynewmail@wat.com"}
	m1.SetRepo(r)

	m2 := &user.User{Id: "500", Email: "o"}
	m2.SetRepo(r)

	return []updateModelTestCase{
		{"JarModelSuccessfulUpdate", m1, []string{"Email"}, nil},
		{"JarModelValidationFailedUpdate", m2, []string{"Email"}, new(validator.ValidationError)},
	}
}

func (t *userTest) createGetManyTestCases() []getManyTestCase {
	return []getManyTestCase{
		{"UserGetOneWithAllProperties", "_id=?", []interface{}{"500"}, []string{}, 1},
		{"UserGetOneWithOneProperty", "_id=?", []interface{}{"500"}, []string{"email"}, 1},
		{"UserGetManyWithAllProperties", "email LIKE ?", []interface{}{"%.com%"}, []string{}, 5},
		{"UserGetManyWithOneProperty", "email LIKE ?", []interface{}{"%.com%"}, []string{"email"}, 5},
		{"UserGetNoneWithAllProperties", "_id=?", []interface{}{"303"}, []string{}, 0},
		{"UserGetNoneWithOneProperty", "_id=?", []interface{}{"303"}, []string{"email"}, 0},
	}
}

func (t *userTest) createGetManyWithDbErrTestCases() []getManyWithErrTestCase {
	return []getManyWithErrTestCase{
		{"UserGetManyWithDbErr", "jar_id=?", []interface{}{"500"}, []string{}, errors.New("Something went wrong")},
	}
}

func (t *userTest) createGetOneTestCases() []getOneTestCase {
	return []getOneTestCase{
		{"UserGetOneWithAllProperties", "500", []string{}, 1},
		{"UserGetOneWithOneProperty", "500", []string{"email"}, 1},
		{"UserGetOneOneInvalid", "1033", []string{"email"}, 0},
	}
}

func (t *userTest) createGetOneWithDbErrTestCases() []getOneWithErrorTestCase {
	return []getOneWithErrorTestCase{
		{"UserGetOneWithDbErr", "500", []string{}, errors.New("Something went wrong")},
	}
}

func (t *userTest) prep() error {
	return t.seed()
}

func (t *userTest) seed() error {
	fmt.Println("Seeding users")
	rows := []map[string]string{
		{
			"_id":      "500",
			"email":    "test@mail.com",
			"password": "randompasword",
		},
		{
			"email":    "wat@wat.com",
			"password": "sdfsdfds",
		},
		{
			"email":    "random@ness.com",
			"password": "fadsfsd",
		},
	}
	return t.test.seed(rows)
}

func (t *userTest) createRepo(db databases.Database) repo.Repo {
	repo, _ := user.NewRepo(db)
	return repo
}

func (t *userTest) createModel(params map[string]interface{}) (repo.Model, error) {
	model, err := user.GetUserFromRowResult(params)
	return model, err
}

func (t *userTest) getAdditionalTests() []func(t *testing.T) {
	tests := []func(t *testing.T){
		t.createRepoAndModelTest,
	}
	return tests
}

func (t *userTest) createRepoAndModelTest(tT *testing.T) {
	testCases := []struct {
		tcname string
		db     databases.Database
		want   error
	}{
		{"UserCreateRepoAndModelSuccess", t.db, nil},
		{"UserCreateRepoNoDb", nil, new(repo.RepoError)},
	}

	for _, tc := range testCases {
		tT.Run(tc.tcname, func(tT *testing.T) {
			r, err := user.NewRepo(tc.db)

			testerr := testing2.TestErrorType(tc.want, err)
			if testerr != nil {
				tT.Error(testing2.TCFailedMsg("UserTest", tc.tcname, testerr))
			} else {
				m := r.NewModel()
				if m == nil {
					tT.Error(testing2.TCFailedMsg("UserTest", tc.tcname, "model is nil"))
				}
			}
		})
	}
}
