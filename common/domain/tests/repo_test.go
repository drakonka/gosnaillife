package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"github.com/golang/mock/gomock"
	"gitlab.com/drakonka/gosnaillife/common"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases/mocks"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/tests"
	"os"
	"strconv"
	"testing"
)

var testDbName = "sltestdb"

var database databases.Database
var repoTests []repoTest

func TestMain(m *testing.M) {
	ret := 1
	defer func() {
		os.Exit(ret)
	}()
	defer testing2.TimeElapsed("Repo Tests", testing2.LenientThreshold)()
	tu := tests.NewTestUtil()
	db, err := tu.CreateTestDB(testDbName)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	app, err := infrastructure.Init(common.CLI)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	tu.PrepRepos(app, db)
	database = db
	err = tu.RunDbMigrations(false, "")
	if err != nil {
		fmt.Printf("\nUp error: %v", err)
		os.Exit(-1)
	}
	rtests := []repoTest{
		newOwnerTest(database),
		newStableTest(database),
		newUserTest(database),
		newJarTest(database),
		newSnailTest(database),
		newOrganTest(database),
		newGeneTest(database),
	}

	for _, test := range rtests {
		err := test.prep()
		if err == nil {
			repoTests = append(repoTests, test)
		} else {
			fmt.Printf("\nError when prepping test: %s\n", err.Error())
			tu.DeleteTestDB(true, testDbName)
			return
		}
	}
	fmt.Printf("\nCount of repo tests to run: %d\n", len(repoTests))

	ret = m.Run()

	fmt.Println("Deleting test DB")
	tu.DeleteTestDB(true, testDbName)
}

func TestInsertOne(t *testing.T) {
	for _, test := range repoTests {
		testcases := test.createInsertOneTestCases()
		for _, tc := range testcases {
			fmt.Printf("\nRunning TestInsertOne Test Case %s.", tc.tcname)
			t.Run(fmt.Sprintf(tc.tcname), func(t *testing.T) {
				repo := test.createRepo(database)

				_, err := repo.InsertOne(tc.model)
				testerr := testing2.TestErrorType(tc.want, err)
				if testerr != nil {
					fmt.Println(err)
					t.Fatal(testing2.TCFailedMsg("RepoTest", tc.tcname, testerr.Error()))
				}
			})
		}
	}
}

func TestDeleteOne(t *testing.T) {
	for _, test := range repoTests {
		testCases := test.createDeleteOneTestCase()
		for _, tc := range testCases {
			fmt.Printf("\nRunning TestDeleteOne Test Case %s.", tc.name)
			t.Run(fmt.Sprintf(tc.name), func(t *testing.T) {
				r := test.createRepo(database)

				itemId, err := r.InsertOne(tc.model)
				if err != nil {
					t.Fatal(testing2.TCFailedMsg("RepoTest", tc.name, "Could not prep insertion"))
				}
				err = r.DeleteOne(itemId)
				testerr := testing2.TestErrorType(tc.want, err)
				if testerr != nil {
					fmt.Println(err)
					t.Fatal(testing2.TCFailedMsg("RepoTest", tc.name, testerr.Error()))
				}
				var m repo.Model
				var id string
				i, ok := itemId.(int)
				if ok {
					id = strconv.Itoa(i)
				} else {
					id = itemId.(string)
				}
				m, err = r.GetOne(id, []string{})
				if err != nil {
					t.Fatal(testing2.TCFailedMsg("RepoTest", tc.name, testing2.TCFailedMsg("RepoTest", tc.name, err)))
				}
				if m != nil {
					msg := fmt.Sprintf("Model ID %v should not exist, but we found it", id)
					t.Error(testing2.TCFailedMsg("RepoTest", tc.name, msg))
				}
			})
		}
	}
}

func TestSaveModel(t *testing.T) {
	for _, test := range repoTests {
		testcases := test.createSaveModelTestCases()
		for _, tc := range testcases {
			model := tc.model.(repo.Model)
			fmt.Printf("\nRunning TestSaveModel Test Case %s.", tc.tcname)
			t.Run(fmt.Sprintf(tc.tcname), func(t *testing.T) {
				_, err := model.Save()
				testerr := testing2.TestErrorType(tc.want, err)
				if testerr != nil {
					t.Fatal(testing2.TCFailedMsg("RepoTest", tc.tcname, testerr.Error()))
				}
			})
		}
	}
}

func TestUpdateOne(t *testing.T) {
	for _, test := range repoTests {
		testcases := test.createUpdateOneTestCases()
		for _, tc := range testcases {
			fmt.Printf("\nRunning TestUpdateOne Test Case %s.", tc.tcname)
			t.Run(fmt.Sprintf(tc.tcname), func(t *testing.T) {
				repo := test.createRepo(database)
				id := fmt.Sprintf("%v", tc.idToUpdate)
				err := repo.UpdateOne(id, tc.fields)
				testerr := testing2.TestErrorType(tc.want, err)
				if testerr != nil {
					fmt.Println(err)
					t.Fatal(testing2.TCFailedMsg("RepoTest", tc.tcname, fmt.Sprintf("wanted error %v, got %v", testerr, err)))
				}
			})
		}
	}
}

func TestUpdateModel(t *testing.T) {
	for _, test := range repoTests {
		testcases := test.createUpdateModelTestCases()
		for _, tc := range testcases {
			t.Run(fmt.Sprintf(tc.tcname), func(t *testing.T) {
				err := tc.model.Update(tc.fieldsToUpdate)
				testerr := testing2.TestErrorType(tc.want, err)
				if testerr != nil {
					fmt.Println(err)
					t.Fatalf("Test case %s failed: %s", tc.tcname, testerr.Error())
				}
			})
		}
	}
}

func TestGetMany(t *testing.T) {
	for _, test := range repoTests {
		testCases := test.createGetManyTestCases()
		for _, tc := range testCases {
			fmt.Printf("Running TestGetMany Test Case %s.", tc.tcname)
			t.Run(fmt.Sprintf("%s, want %d results", tc.tcname, tc.want), func(t *testing.T) {
				repo := test.createRepo(database)
				models, err := repo.GetMany(tc.where, tc.params, tc.fields)
				if err != nil {
					t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, err))
					return
				}
				if len(models) != tc.want {
					t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, fmt.Sprintf("expected %d results, got %d", tc.want, len(models))))
				}
			})
		}
	}
}

func TestGetManyWithDbErr(t *testing.T) {
	for _, test := range repoTests {
		testCases := test.createGetManyWithDbErrTestCases()
		for _, tc := range testCases {
			t.Run(fmt.Sprintf(tc.tcname), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				dbMock := mocks.NewMockDatabase(ctrl)
				dbMock.EXPECT().GetRows(
					gomock.AssignableToTypeOf(""),
					gomock.AssignableToTypeOf(""),
					gomock.AssignableToTypeOf([]interface{}{}),
					gomock.AssignableToTypeOf([]string{})).Return(nil, errors.New("Something went wrong"))

				repo := test.createRepo(dbMock)
				_, err := repo.GetMany(tc.where, tc.params, tc.fields)

				testErr := testing2.TestErrorType(tc.wantedErr, err)
				if testErr != nil {
					t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, testErr))
				}
			})
		}
	}
}

func TestGetOne(t *testing.T) {
	for _, test := range repoTests {
		testCases := test.createGetOneTestCases()
		for _, tc := range testCases {
			fmt.Printf("Running TestGetMany Test Case %s.", tc.tcname)
			t.Run(fmt.Sprintf("%s, want %d results", tc.tcname, tc.wantedCount), func(t *testing.T) {
				r := test.createRepo(database)
				id := fmt.Sprintf("%v", tc.id)
				model, err := r.GetOne(id, tc.fields)
				if err != nil {
					t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, err))
					return
				}
				if tc.wantedCount > 0 && model == nil {
					t.Error(testing2.TCFailedMsg("RepoTest", tc.tcname, tc.wantedCount))
				}
			})
		}
	}
}

func TestGetOneWithDbErr(t *testing.T) {
	for _, test := range repoTests {
		testCases := test.createGetOneWithDbErrTestCases()
		for _, tc := range testCases {
			fmt.Printf("Running TestGetMany Test Case %s.", tc.tcname)
			t.Run(fmt.Sprintf(tc.tcname), func(t *testing.T) {
				ctrl := gomock.NewController(t)
				dbMock := mocks.NewMockDatabase(ctrl)
				dbMock.EXPECT().GetRow(
					gomock.AssignableToTypeOf(""),
					gomock.AssignableToTypeOf(""),
					gomock.AssignableToTypeOf([]string{})).Return(nil, errors.New("Something went wrong"))

				repo := test.createRepo(dbMock)
				id := fmt.Sprintf("%v", tc.id)
				_, err := repo.GetOne(id, tc.fields)

				testErr := testing2.TestErrorType(tc.wantedErr, err)
				if testErr != nil {
					t.Errorf("Test case %s failed: %v", tc.tcname, testErr)
				}
			})
		}
	}
}

func TestAdditionalTests(t *testing.T) {
	for _, test := range repoTests {
		testCases := test.getAdditionalTests()
		for _, tc := range testCases {
			tc(t)
		}
	}
}
