package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"testing"
	"time"
)

type organTest struct {
	test
}

func newOrganTest(db databases.Database) *organTest {
	test := organTest{}
	test.tablename = "organs"
	test.db = db
	test.idKey = "organ_id"
	return &test
}

func (t *organTest) createInsertOneTestCases() []insertOneTestCase {
	r, err := organ.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &organ.Organ{}
	m1.OrganTypeId = "outer_shell"
	m1.SnailId = 500
	m1.MaxEfficiency = 90
	m1.SetRepo(r)

	m2 := &organ.Organ{}
	m2.OrganTypeId = "outer_shell"
	m2.SetRepo(r)

	m3 := &organ.Organ{}
	m3.OrganTypeId = "bad_type"
	m3.SnailId = 500
	m3.SetRepo(r)

	return []insertOneTestCase{
		{"OrganInsertWithTypeId", m1, nil},
		{"OrganInsertWithoutSnailId", m2, &validator.ValidationError{}},
		{"OrganInsertWithBadTypeId", m3, &validator.ValidationError{}},
	}
}

func (t *organTest) createDeleteOneTestCase() []deleteOneTestCase {
	r, err := organ.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &organ.Organ{}
	m1.OrganTypeId = "outer_shell"
	m1.SnailId = 200
	m1.MaxEfficiency = 90
	m1.SetRepo(r)

	return []deleteOneTestCase{
		{
			name:  "SuccessfulOrganDelete",
			model: m1,
			want:  nil,
		},
	}
}

func (t *organTest) createSaveModelTestCases() []saveModelTestCase {
	var testCases []saveModelTestCase
	for _, tc := range t.createInsertOneTestCases() {
		if m, ok := tc.model.(*organ.Organ); ok {
			m.Id = 0

			testCases = append(testCases, saveModelTestCase(tc))
		}
	}
	return testCases
}

func (t *organTest) createUpdateOneTestCases() []updateOneTestCase {
	return []updateOneTestCase{
		{"OrganSuccessfulUpdate", "500", map[string]string{"max_efficiency": "5"}, nil},
		{"OrganValidationFailedUpdate", "500", map[string]string{"organ_type_id": "badorgan"}, new(validator.ValidationError)},
	}
}

func (t *organTest) createUpdateModelTestCases() []updateModelTestCase {
	r, err := organ.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &organ.Organ{}
	m1.Id = 500
	m1.SnailId = 2
	m1.CurrentEfficiencyPerc = 10
	m1.LastCheck = time.Now()
	m1.SetRepo(r)

	m2 := &organ.Organ{}
	m2.Id = 500
	m2.OrganTypeId = "magic_organ"
	m2.SetRepo(r)

	m3 := &organ.Organ{}
	m3.Id = 500
	m3.WeightMg = 1000000
	m3.SetRepo(r)

	return []updateModelTestCase{
		{"OrganModelSuccessfulUpdate", m1, []string{"CurrentEfficiencyPerc"}, nil},
		{"OrganModelValidationFailedUpdate", m2, []string{"OrganTypeId"}, new(validator.ValidationError)},
		{"OrganModelWeightValidationFailedUpdate", m3, []string{"WeightMg"}, new(validator.ValidationError)},
		{"OrganModelSuccessfulUpdateAll", m1, nil, nil},
	}
}

func (t *organTest) createGetManyTestCases() []getManyTestCase {
	return []getManyTestCase{
		{"OrganGetOneWithAllProperties", "organ_id=?", []interface{}{"500"}, []string{}, 1},
		{"OrganGetOneWithOneProperty", "organ_id=?", []interface{}{"500"}, []string{"name"}, 1},
		{"OrganGetManyWithAllProperties", "organ_type_id LIKE ?", []interface{}{"%shell%"}, []string{}, 7},
		{"OrganGetManyWithOneProperty", "organ_type_id LIKE ?", []interface{}{"%shell%"}, []string{"organ_id"}, 7},
		{"OrganGetNoneWithAllProperties", "organ_id=?", []interface{}{"303"}, []string{}, 0},
		{"OrganGetNoneWithOneProperty", "organ_id=?", []interface{}{"303"}, []string{"name"}, 0},
	}
}

func (t *organTest) createGetManyWithDbErrTestCases() []getManyWithErrTestCase {
	return []getManyWithErrTestCase{
		{"OrganGetManyWithDbErr", "organ_id=?", []interface{}{"500"}, []string{}, errors.New("Something went wrong")},
	}
}

func (t *organTest) createGetOneTestCases() []getOneTestCase {
	return []getOneTestCase{
		{"OrganGetOneWithAllProperties", "500", []string{}, 1},
		{"OrganGetOneWithOneProperty", "500", []string{"organ_type_id"}, 1},
		{"OrganGetOneOneInvalid", "1033", []string{"organ_type_id"}, 0},
	}
}

func (t *organTest) createGetOneWithDbErrTestCases() []getOneWithErrorTestCase {
	return []getOneWithErrorTestCase{
		{"OrganGetOneWithDbErr", "500", []string{}, errors.New("Something went wrong")},
	}
}

func (t *organTest) prep() error {
	return t.seed()
}

func (t *organTest) seed() error {
	fmt.Println("seeding organ")
	rows := []map[string]string{
		{
			"organ_id":      "500",
			"organ_type_id": "outer_shell",
			"snail_id":      "500",
		},
		{
			"organ_type_id": "outer_shell",
			"snail_id":      "500",
		},
		{
			"organ_type_id": "outer_shell",
			"snail_id":      "500",
		},
	}
	return t.test.seed(rows)
}

func (t *organTest) createRepo(db databases.Database) repo.Repo {
	repo, _ := organ.NewRepo(db)
	return repo
}

func (t *organTest) createModel(params map[string]interface{}) (repo.Model, error) {
	model, err := organ.GetOrganFromRowResult(params)
	return model, err
}

func (t *organTest) getAdditionalTests() []func(tT *testing.T) {
	tests := []func(tT *testing.T){
		t.createRepoAndModelTest,
	}
	return tests
}

func (t *organTest) createRepoAndModelTest(tT *testing.T) {
	testCases := []struct {
		tcname string
		db     databases.Database
		want   error
	}{
		{"OrganCreateRepoAndModelSuccess", t.db, nil},
		{"OrganCreateRepoNoDb", nil, new(repo.RepoError)},
	}

	for _, tc := range testCases {
		tT.Run(tc.tcname, func(tT *testing.T) {
			r, err := organ.NewRepo(tc.db)

			testerr := testing2.TestErrorType(tc.want, err)
			if testerr != nil {
				tT.Error(testing2.TCFailedMsg("OrganTest", tc.tcname, testerr))

			} else {
				m := r.NewModel()
				if m == nil {
					tT.Error(testing2.TCFailedMsg("OrganTest", tc.tcname, "model is nil"))

				}
			}
		})
	}
}
