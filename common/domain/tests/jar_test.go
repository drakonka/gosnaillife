package tests

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"testing"
)

type jarTest struct {
	test
}

func newJarTest(db databases.Database) *jarTest {
	test := jarTest{}
	test.tablename = "jars"
	test.db = db
	test.idKey = "jar_id"
	return &test
}

func (t *jarTest) createInsertOneTestCases() []insertOneTestCase {
	r, err := jar.NewRepo(t.db)
	if err != nil {
		panic(err)
	}

	m1 := &jar.Jar{Id: 509, Name: "Cool Jar", OwnerId: 20}
	m1.SetRepo(r)

	m2 := &jar.Jar{Name: "Cool Jar", OwnerId: 20}
	m2.SetRepo(r)

	m3 := &jar.Jar{Name: "Cool Jar With Size", Width: 10, Height: 10, Depth: 10, OwnerId: 20}
	m3.SetRepo(r)

	return []insertOneTestCase{
		{"JarInsertWithId", m1, nil},
		{"JarInsertWithoutId", m2, nil},
		{"JarInsertWithSize", m3, nil},
		{"JarInsertWithBadType", jar.Jar{Name: "Watajar"}, &repo.RepoError{}},
	}
}

func (t *jarTest) createDeleteOneTestCase() []deleteOneTestCase {
	r, err := jar.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	jar := &jar.Jar{Name: "Deleted Jar"}
	jar.SetRepo(r)
	return []deleteOneTestCase{
		{
			name:  "SuccessfulJarDelete",
			model: jar,
			want:  nil,
		},
	}
}

func (t *jarTest) createSaveModelTestCases() []saveModelTestCase {
	var testCases []saveModelTestCase
	for _, tc := range t.createInsertOneTestCases() {
		if m, ok := tc.model.(*jar.Jar); ok {
			m.Id = 0
			m.OwnerId++
			testCases = append(testCases, saveModelTestCase(tc))
		}
	}
	return testCases
}

func (t *jarTest) createUpdateOneTestCases() []updateOneTestCase {
	return []updateOneTestCase{
		{"JarSuccessfulUpdate", "500", map[string]string{"name": "Bob's Modified Jar"}, nil},
		{"JarValidationFailedUpdate", "500", map[string]string{"name": "o"}, new(validator.ValidationError)},
	}
}

func (t *jarTest) createUpdateModelTestCases() []updateModelTestCase {
	r, err := jar.NewRepo(t.db)
	if err != nil {
		panic(err)
	}
	m1 := &jar.Jar{Id: 500, Name: "Bob's Modified Jar"}
	m1.SetRepo(r)

	m2 := &jar.Jar{Id: 500, Name: "o"}
	m2.SetRepo(r)

	return []updateModelTestCase{
		{"JarModelSuccessfulUpdate", m1, []string{"Name"}, nil},
		{"JarModelValidationFailedUpdate", m2, []string{"Name"}, new(validator.ValidationError)},
	}
}

func (t *jarTest) createGetManyTestCases() []getManyTestCase {
	return []getManyTestCase{
		{"JarGetOneWithAllProperties", "jar_id=?", []interface{}{"500"}, []string{}, 1},
		{"JarGetOneWithOneProperty", "jar_id=?", []interface{}{"500"}, []string{"name"}, 1},
		{"JarGetManyWithAllProperties", "name LIKE ?", []interface{}{"%Bob%"}, []string{}, 3},
		{"JarGetManyWithOneProperty", "name LIKE ?", []interface{}{"%Bob%"}, []string{"name"}, 3},
		{"JarGetNoneWithAllProperties", "jar_id=?", []interface{}{"303"}, []string{}, 0},
		{"JarGetNoneWithOneProperty", "jar_id=?", []interface{}{"303"}, []string{"name"}, 0},
	}
}

func (t *jarTest) createGetManyWithDbErrTestCases() []getManyWithErrTestCase {
	return []getManyWithErrTestCase{
		{"JarGetManyWithDbErr", "jar_id=?", []interface{}{"500"}, []string{}, errors.New("Something went wrong")},
	}
}

func (t *jarTest) createGetOneTestCases() []getOneTestCase {
	return []getOneTestCase{
		{"JarGetOneWithAllProperties", "500", []string{}, 1},
		{"JarGetOneWithOneProperty", "500", []string{"name"}, 1},
		{"JarGetOneOneInvalid", "1033", []string{"name"}, 0},
	}
}

func (t *jarTest) createGetOneWithDbErrTestCases() []getOneWithErrorTestCase {
	return []getOneWithErrorTestCase{
		{"JarGetOneWithDbErr", "500", []string{}, errors.New("Something went wrong")},
	}
}

func (t *jarTest) prep() error {
	return t.seed()
}

func (t *jarTest) seed() error {
	fmt.Println("Seeding jars")
	rows := []map[string]string{
		{
			"jar_id":    "500",
			"stable_id": "1",
			"name":      "Bob's Jar One",
		},
		{
			"name":      "Bob's Jar Two",
			"stable_id": "1",
		},
		{
			"name":      "Bob's Jar Three",
			"stable_id": "1",
		},
	}
	return t.test.seed(rows)
}

func (t *jarTest) createRepo(db databases.Database) repo.Repo {
	repo, _ := jar.NewRepo(db)
	return repo
}

func (t *jarTest) createModel(params map[string]interface{}) (repo.Model, error) {
	model, err := jar.GetJarFromRowResult(params)
	return model, err
}

func (t *jarTest) getAdditionalTests() []func(tT *testing.T) {
	tests := []func(tT *testing.T){
		t.createRepoAndModelTest,
	}
	return tests
}

func (t *jarTest) createRepoAndModelTest(tT *testing.T) {
	testCases := []struct {
		tcname string
		db     databases.Database
		want   error
	}{
		{"JarCreateRepoAndModelSuccess", t.db, nil},
		{"JarCreateRepoNoDb", nil, new(repo.RepoError)},
	}

	for _, tc := range testCases {
		tT.Run(tc.tcname, func(tT *testing.T) {
			r, err := jar.NewRepo(tc.db)

			testerr := testing2.TestErrorType(tc.want, err)
			if testerr != nil {
				tT.Error(testing2.TCFailedMsg("JarTest", tc.tcname, testerr))
				return
			}
			if tc.want != nil {
				return
			}
			m := r.NewModel()
			if m == nil {
				tT.Error("JarTest", tc.tcname, "model is nil")
			}
		})
	}
}
