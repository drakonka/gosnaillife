package owner

import (
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/stable"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"reflect"
)

type Owner struct {
	domain.Model `db:"-"`
	Id           int              `db:"owner_id"`
	UserId       string           `db:"user_id"`
	FirstName    string           `db:"firstname"`
	LastName     string           `db:"lastname"`
	Description  string           `db:"description"`
	Sek          int              `db:"sek"`
	Stables      []*stable.Stable `json:"stables"`
}

func (m *Owner) GetId() interface{} {
	return m.Id
}

func (m *Owner) SetId(id interface{}) {
	m.Id = id.(int)
}

func (m *Owner) LoadStables() []*stable.Stable {
	r := m.Repo.(*Repo)
	if m.Stables == nil && r.Db != nil {
		r.loadOwnerStables(m)
	}
	return m.Stables
}

// Validator

func (m *Owner) GetRules() []validator.Rule {
	fnLength := validator.NewLengthRule(2, 50, m.FirstName, "FirstName")
	lnLength := validator.NewLengthRule(2, 50, m.LastName, "LastName")
	rules := []validator.Rule{
		&fnLength,
		&lnLength,
	}

	return rules
}

func (m *Owner) Validate() error {
	rules := m.GetRules()
	return m.Model.Validate(rules, "Owner")
}

func (m *Owner) ValidateSpecific(fieldsToValidate ...string) []error {
	rules := m.GetRules()
	tagMap := m.BuildDbTagMap(reflect.TypeOf(*m))
	return m.Model.ValidateSpecific(rules, tagMap, fieldsToValidate, "Owner")
}

// End Validator

func (m *Owner) Save() (interface{}, error) {
	return m.Repo.InsertOne(m)
}

func (m *Owner) Update(fields []string) error {
	dbTagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	r := m.Repo.(*Repo)
	return r.updateOneModel(m, dbTagMap, fields)
}

func (m *Owner) setStables(s []*stable.Stable) {
	m.Stables = s
}
