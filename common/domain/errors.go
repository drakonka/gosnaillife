package domain

import "errors"

var (
	ErrOwnerNotAssigned = errors.New("owner not assigned")
)
