package stable

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/jar"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
	"strconv"
)

type Repo struct {
	domain.Repo
}

func NewRepo(database databases.Database) (repo2.Repo, error) {
	r := Repo{}
	if err := r.Init(database); err != nil {
		return nil, err
	}
	r.Tablename = "stables"
	r.Db = database
	return &r, nil
}

func (r *Repo) NewModel() repo2.Model {
	m := Stable{}
	m.Repo = r
	return &m
}

func (r *Repo) GetOne(id string, fields []string) (repo2.Model, error) {
	row, err := r.Db.GetRow(r.Tablename, id, fields)
	if err == nil {
		s, err := GetStableFromRowResult(row)
		r.tryLoadJars(fields, s)
		sId := strconv.Itoa(s.Id)
		if sId == id {
			s.Repo = r
			return s, err
		}
	}
	return nil, err
}

func (r *Repo) GetMany(where string, params []interface{}, fields []string) (stables []interface{}, err error) {
	s := Stable{}
	res, err := r.Repo.GetMany(&s.Model, fields, where, params)
	if err != nil {
		error2.HandleErr(err, "GetMany [Get Stable Rows]")
		return stables, err
	}
	for _, s := range res {
		stable, err := GetStableFromRowResult(s)
		if err != nil {
			error2.HandleErr(err, "GetMany[Decode Stable Map]")
			continue
		}
		r.tryLoadJars(fields, stable)
		stables = append(stables, stable)
	}
	return stables, err
}

func (r *Repo) InsertOne(item interface{}) (id interface{}, err error) {
	id = -1
	if r.Db == nil {
		return id, &repo2.RepoError{Err: "The database associated with this Repo could not be found."}
	}
	model, ok := item.(*Stable)
	if !ok {
		msg := fmt.Sprintf("The provided item is not a stable; it is a %v", reflect.TypeOf(item))
		return id, &repo2.RepoError{Err: msg}
	}

	err, id = r.ValidateAndInsertModel(model)
	if _, ok := id.(int); !ok {
		si := fmt.Sprintf("%v", id)
		id, _ = strconv.Atoi(si)
	}
	return id, err
}

func (r *Repo) UpdateOne(itemId interface{}, fields map[string]string) error {
	// Retrieve stable with above id
	m, err := r.GetOne(itemId.(string), []string{"stable_id"})

	model, ok := m.(*Stable)
	if !ok {
		msg := fmt.Sprintf("The provided item is not an stable; it is a %v", reflect.TypeOf(m))
		return &repo2.RepoError{Err: msg}
	}
	modelMap := domain.MakeModelMap(model, model.GetDbTagMap(reflect.TypeOf(*model)), fields)

	newModel, err := GetStableFromRowResult(modelMap)
	if err != nil {
		msg := fmt.Sprintf("Could not update stable: %s", err.Error())
		return &repo2.RepoError{Err: msg}
	}
	newModel.Id = model.Id

	err = r.ValidateAndUpdateModel(newModel, modelMap)
	return err
}

func (r *Repo) updateOneModel(m repo2.Model, tagMap map[string]string, fields []string) error {
	id := strconv.Itoa(m.GetId().(int))
	mp := r.UpdateOneModelPrep(m, tagMap, fields)
	return r.UpdateOne(id, mp)
}

func GetStableFromRowResult(r databases.RowResult) (m *Stable, err error) {
	var stable Stable
	model, err := domain.GetModelFromRowResult(&stable, r)
	if err == nil {
		m = model.(*Stable)
	}
	return m, err
}

func (r *Repo) loadJars(stable *Stable) {
	sr, _ := jar.NewRepo(r.Db)
	jarsi, err := sr.GetMany("stable_id=?", []interface{}{stable.Id}, []string{})
	error2.HandleErr(err, "getStableJars")
	if err != nil {
		return
	}
	var jars []*jar.Jar
	for _, j := range jarsi {
		jars = append(jars, j.(*jar.Jar))
	}
	stable.setJars(jars)
}

func (r *Repo) tryLoadJars(fields []string, s *Stable) {
	if util.CaselessStrIndexOf(fields, "jars") > -1 || len(fields) == 0 {
		r.loadJars(s)
	}
}
