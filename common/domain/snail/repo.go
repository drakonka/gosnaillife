package snail

import (
	"database/sql"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	gene2 "gitlab.com/drakonka/gosnaillife/common/domain/snail/gene"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
	"strconv"
)

type Repo struct {
	domain.Repo
}

func NewRepo(database databases.Database) (repo2.Repo, error) {
	r := Repo{}
	if err := r.Init(database); err != nil {
		return nil, err
	}
	r.Tablename = "snails"
	r.Db = database

	return &r, nil
}

func (repo *Repo) NewModel() repo2.Model {
	m := Snail{}
	m.Repo = repo
	return &m
}

func (repo *Repo) GetOne(id string, fields []string) (repo2.Model, error) {
	row, err := repo.Db.GetRow(repo.Tablename, id, fields)
	if err != nil {
		return nil, err
	}
	m, err := GetSnailFromRowResult(row)
	if err != nil {
		return nil, err
	}
	err = repo.tryLoadOrgans(fields, m)
	if err != nil {
		return nil, err
	}
	err = repo.tryLoadGenes(fields, m)
	if err != nil {
		return nil, err
	}
	mId := strconv.Itoa(m.Id)
	if mId == id {
		m.Repo = repo
		return m, err
	}
	return nil, err
}

func (repo *Repo) GetMany(where string, params []interface{}, fields []string) (models []interface{}, err error) {
	s := Snail{}
	res, err := repo.Repo.GetMany(&s.Model, fields, where, params)
	if err != nil {
		error2.HandleErr(err, "GetMany [Get Stable Rows]")
		return models, err
	}
	for _, s := range res {
		model, err := GetSnailFromRowResult(s)
		if err != nil {
			error2.HandleErr(err, "GetMany[Decode Stable Map]")
			continue
		}
		model.Repo = repo
		err = repo.tryLoadOrgans(fields, model)
		if err != nil {
			return nil, err
		}
		err = repo.tryLoadGenes(fields, model)
		if err != nil {
			return nil, err
		}
		models = append(models, model)
	}
	return models, err
}

func (repo *Repo) InsertOne(item interface{}) (id interface{}, err error) {
	id = -1
	if repo.Db == nil {
		return id, &repo2.RepoError{Err: "The database associated with this repo could not be found."}
	}
	model, ok := item.(*Snail)
	if !ok {
		msg := fmt.Sprintf("The provided item is not a snail; it is a %v", reflect.TypeOf(item))
		return id, &repo2.RepoError{Err: msg}
	}

	err = repo.Db.Transact(func(tx *sql.Tx) error {
		err, id = repo.ValidateAndInsertModel(model)
		if err != nil {
			return err
		}
		if _, ok := id.(int); !ok {
			si := fmt.Sprintf("%v", id)
			id, _ = strconv.Atoi(si)
		}
		model.Id = id.(int)

		err = repo.insertGenes(model)
		if err != nil {
			return err
		}

		err = repo.insertOrgans(model)
		return err
	})

	return model.Id, err
}

func (repo *Repo) DeleteOne(itemId interface{}) (err error) {
	// First retrieve the snail and its genes/organs/etc
	var id string
	i, ok := itemId.(int)
	if ok {
		id = strconv.Itoa(i)
	} else {
		id = itemId.(string)
	}
	snail, err := repo.GetOne(id, []string{})
	if err != nil {
		return err
	}
	err = repo.Db.Transact(func(tx *sql.Tx) error {
		err := repo.Repo.DeleteOne(itemId)
		if err != nil {
			return err
		}
		s := snail.(*Snail)
		organRepo, err := organ.NewRepo(repo.Db)
		if err != nil {
			return err
		}
		for _, o := range s.Organs {
			err = organRepo.DeleteOne(o.Id)
			if err != nil {
				return err
			}
		}

		geneRepo, err := gene2.NewRepo(repo.Db)
		if err != nil {
			return err
		}
		for geneKey, gene := range s.genes {
			alleles := gene.GetAlleles()
			for _, a := range alleles {
				fmt.Printf("key: %s", geneKey)
				allele := a.(*gene2.Allele)
				err = geneRepo.DeleteOne(allele.Id)
				if err != nil {
					return err
				}
			}
		}
		return err
	})
	return nil
}

func (repo *Repo) UpdateOne(itemId interface{}, fields map[string]string) error {
	m, err := repo.GetOne(itemId.(string), []string{"snail_id"})

	model, ok := m.(*Snail)
	if !ok {
		msg := fmt.Sprintf("The provided item is not a snail; it is a %v", reflect.TypeOf(m))
		return &repo2.RepoError{Err: msg}
	}
	modelMap := domain.MakeModelMap(model, model.GetDbTagMap(reflect.TypeOf(*model)), fields)

	newModel, err := GetSnailFromRowResult(modelMap)
	if err != nil {
		msg := fmt.Sprintf("Could not update snail: %s", err.Error())
		return &repo2.RepoError{Err: msg}
	}
	newModel.Id = model.Id

	err = repo.ValidateAndUpdateModel(newModel, modelMap)
	return err
}

func (repo *Repo) GetAllLivingSnails() ([]*Snail, error) {
	allModels, err := repo.GetMany("death_date IS NULL", nil, nil)
	if err != nil {
		return nil, err
	}
	var allSnails []*Snail
	for _, m := range allModels {
		s := m.(*Snail)
		allSnails = append(allSnails, s)
	}
	return allSnails, err
}

func GetSnailFromRowResult(r databases.RowResult) (s *Snail, err error) {
	var m Snail
	model, err := domain.GetModelFromRowResult(&m, r)
	if err == nil {
		s = model.(*Snail)
	}
	return s, err
}

func (repo *Repo) updateOneModel(m repo2.Model, tagMap map[string]string, fields []string) error {
	id := strconv.Itoa(m.GetId().(int))
	mp := repo.UpdateOneModelPrep(m, tagMap, fields)
	return repo.UpdateOne(id, mp)
}

func (repo *Repo) tryLoadOrgans(fields []string, m *Snail) error {
	if util.CaselessStrIndexOf(fields, "organs") > -1 || len(fields) == 0 {
		return repo.loadOrgans(m)
	}
	return nil
}

func (repo *Repo) loadOrgans(m *Snail) error {
	or, err := organ.NewRepo(repo.Db)
	if err != nil {
		return err
	}
	organsi, err := or.GetMany("snail_id=?", []interface{}{m.Id}, []string{})
	error2.HandleErr(err, "getSnailOrgans")
	if err != nil {
		return err
	}
	var organs []*organ.Organ
	for _, o := range organsi {
		or := o.(*organ.Organ)
		or.SetOwner(m)
		organs = append(organs, or)
	}
	m.setOrgans(organs)
	return nil
}

func (repo *Repo) tryLoadGenes(fields []string, m *Snail) error {
	if util.CaselessStrIndexOf(fields, "genes") > -1 || len(fields) == 0 {
		return repo.loadGenes(m)
	}
	return nil
}

func (repo *Repo) loadGenes(m *Snail) error {
	gr, _ := gene2.NewRepo(repo.Db)
	allelesi, err := gr.GetMany("owner_id=?", []interface{}{m.Id}, []string{})
	error2.HandleErr(err, "loadGenes")
	if err != nil {
		return err
	}

	if m.genes == nil {
		m.genes = make(map[string]organism.Gene)
	}
	common.Log.Infof("Getting genes for snail %d: %v", m.Id, allelesi)
	for _, ai := range allelesi {
		allele := ai.(organism.AlleleReaderWriter)
		gene := m.genes[allele.GetGeneId()]
		if gene == nil {
			genetype := gene2.GeneRegistry[allele.GetGeneId()]
			newgene, err := gene2.InstantiateGene(genetype)
			if err != nil {
				return err
			}
			err = newgene.Init()
			if err != nil {
				return err
			}
			m.genes[allele.GetGeneId()] = newgene
		}
		alleles := m.genes[allele.GetGeneId()].GetAlleles()
		alleles = append(alleles, allele)
		err = m.genes[allele.GetGeneId()].SetAlleles(alleles)
		if err != nil {
			return err
		}
	}
	return nil
}

func (repo *Repo) insertOrgans(model *Snail) error {
	organRepo, _ := organ.NewRepo(repo.Db)
	for _, o := range model.Organs {
		o.SnailId = model.Id
		organId, organErr := organRepo.InsertOne(o)
		if organErr != nil {
			return &repo2.RepoError{Err: fmt.Sprintf("Failed saving organ: %v", organErr)}
		}
		if organId.(int) <= 0 {
			return &repo2.RepoError{Err: "Got invalid organ ID"}
		}
	}
	return nil
}

func (repo *Repo) insertGenes(model *Snail) error {
	geneRepo, _ := gene2.NewRepo(repo.Db)
	for _, g := range model.genes {
		for _, a := range g.GetAlleles() {
			a.SetOwnerId(model.Id)
			alleleId, alleleErr := geneRepo.InsertOne(a)
			if alleleErr != nil {
				return &repo2.RepoError{Err: fmt.Sprintf("Failed saving allele: %v", alleleErr)}
			}
			if alleleId.(int) <= 0 {
				return &repo2.RepoError{Err: "Got invalid allele ID"}
			}
		}

	}
	return nil
}
