package tests

import (
	"fmt"
	"github.com/golang/mock/gomock"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/gene"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	mock_repo "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo/mocks"
	"math"
	"testing"
)

/* func TestImmunity(t *testing.T) {
	testCases := [] struct {
		name    string
		iA1 gene.Allele
		iA2 gene.Allele
		idA1 gene.Allele
		idA2 gene.Allele
		expectedImmunityRate  float64
		expectedImmunityMax   float64
		expectedImmunityPerc  float64
		ageIncrement     int
	}{
		{
			name: "IncrementFrom0to5Standard",
			iA1: gene.Allele{ Key: "S" },
			iA2: gene.Allele{ Key: "S" },
			idA1: gene.Allele{ Key: "S" },
			idA2: gene.Allele{ Key:"S" },
			expectedImmunityRate: 1,
			expectedImmunityMax: 100,
			expectedImmunityPerc: 100,
			ageIncrement: 5,
		},

	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			snail, err := snail.GenerateRandomSnail(0, nil)
			if err != nil {
				t.Fatal(testing2.TCFailedMsg("TestImmunity", tc.name, err))
			}
			iGene := snail.GetGene("immunity_a1")
			idGene := snail.GetGene("immunity_degradation_a1")

		})
	}

} */

func TestGenerateRandomSnail(t *testing.T) {
	testCases := []struct {
		name        string
		age         int
		hasNameDupe bool
		err         error
	}{
		{
			name: "0DaysOldSnail",
			age:  0,
			err:  nil,
		},
		{
			name: "5DaysOldSnail",
			age:  5,
			err:  nil,
		},
		{
			name:        "SnailWithDupeName",
			age:         0,
			hasNameDupe: true,
			err:         nil,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {

			var repo repo2.Repo
			if tc.hasNameDupe {
				c := gomock.NewController(t)
				repo := mock_repo.NewMockRepo(c)
				repo.EXPECT().GetMany(gomock.Any(), gomock.Any(), gomock.Any()).Return([]interface{}{snail.Snail{Name: tc.name}}, nil)
			}
			s, err := snail.GenerateRandomSnail(tc.age, repo)
			e := testing2.TestErrorType(tc.err, err)
			if e != nil {
				t.Errorf(e.Error())
				return
			}

			rActual := math.Round(s.AgeDays()*100) / 100
			rWanted := math.Round(float64(tc.age)*100) / 100 // 12.35 (round to nearest)

			if rActual != rWanted {
				ctx := fmt.Sprintf("Expected age %f, actual age %f", rActual, rWanted)
				t.Error(testing2.TCFailedMsg("CreationTest", tc.name, ctx))
			}
			if len(s.Organs) == 0 {
				t.Error(testing2.TCFailedMsg("CreationTest", tc.name, "generated snail has no organs"))
			}
			if len(s.Genes()) == 0 {
				t.Error(testing2.TCFailedMsg("CreationTest", tc.name, "generated snail has no genes"))

				return
			}

			wantedGenes := len(gene.GeneRegistry)
			for _, g := range s.Genes() {
				alleles := g.GetAlleles()
				if alleles == nil || len(alleles) == 0 {
					t.Error(testing2.TCFailedMsg("CreationTest", tc.name, fmt.Sprintf("gene %s has no alleles", g.GeneId())))
				}
				wantedGenes -= 1
			}

			if wantedGenes > 0 {
				msg := fmt.Sprintf("Some genes were missing. Found: %v", s.Genes())
				t.Error(testing2.TCFailedMsg("CreationTest", tc.name, msg))
			}
		})
	}
}
