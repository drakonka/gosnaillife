package snail

import (
	"errors"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"time"
)

func (m *Snail) UpdateOrganism(updateDb bool) error {
	err := m.UpdateOrgans(updateDb)
	return err
}

func (m *Snail) UpdateOrgans(updateDb bool) error {
	for _, o := range m.Organs {
		return m.UpdateOrgan(o, updateDb)
	}
	return nil
}

func (m *Snail) UpdateOrgan(o *organ.Organ, updateDb bool) error {
	common.Log.Infof("Updating organ %s for snail %d", o.OrganTypeId, m.Id)
	// Loop through each gene to find its expesser interface
	for _, v := range m.genes {
		err := o.TryExpressGene(v)
		if err != nil {
			return err
		}
	}
	o.UpdateMaturity(time.Now)

	o.LastCheck = time.Now()
	if updateDb {
		if m.Repo == nil {
			return errors.New("snail model has no repo")
		}
		db := m.Repo.GetDb()
		common.Log.Infof("Updating organ ID %d in db", o.Id)
		repo, err := organ.NewRepo(db)
		if err != nil {
			return err
		}
		o.Repo = repo
		err = o.Update(nil)
		if err != nil {
			return err
		}
	}
	return nil
}
