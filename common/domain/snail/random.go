package snail

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/gene"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"time"
)

func GenerateRandomSnail(ageInDays int, repo repo.Repo) (*Snail, error) {
	// I think we need to _age_ the snail!
	snail := Snail{}
	err := snail.generateRandomGenes()
	if err != nil {
		return nil, err
	}
	err = snail.generateOrgans()
	now := time.Now()
	snail.BirthDate = now.AddDate(0, 0, -ageInDays)
	snail.Repo = repo
	snail.Name = snail.generateRandomName()
	lastCheck := snail.BirthDate
	// Age the organs according to days
	for i := 0; i < ageInDays; i++ {
		err = snail.UpdateOrganism(false)
		if err != nil {
			return nil, err
		}
		lastCheck := lastCheck.Add(time.Hour * 24)
		for _, o := range snail.Organs {
			o.LastCheck = lastCheck
		}

	}
	return &snail, err
}

// This is super ugly, make it come up with a "nice" name later
func (s *Snail) generateRandomName() string {
	n := time.Now()
	// Check for duplicates!
	name := fmt.Sprintf("%s %d", n.Weekday(), n.Unix())

	if s.isDupeName(name) {
		time.Sleep(1 * time.Millisecond) // this should generate a unique timestamp on next go
		return s.generateRandomName()
	}

	return name
}

func (s *Snail) isDupeName(name string) bool {
	if s.Repo == nil {
		return false
	}
	dupes, _ := s.Repo.GetMany("name = ?", []interface{}{name}, []string{"Id"})
	if len(dupes) > 0 {
		return true
	}
	return false
}

func (s *Snail) generateRandomGenes() error {
	s.genes = make(map[string]organism.Gene)
	for id, t := range gene.GeneRegistry {
		gene, err := gene.InstantiateGene(t)
		if err != nil {
			error2.HandleErr(err, "generateGenes")
			return err
		}
		gene.Init()
		gene.GenerateRandomAlleles()
		s.genes[id] = gene
	}
	return nil

}

func (s *Snail) generateOrgans() error {
	for _, i := range organ.OrganInstantiators {
		o := i()
		o.SnailId = s.Id
		o.SetOwner(s)
		o.MaturityRate = s.generateBaseOrganMaturityRate()
		s.Organs = append(s.Organs, &o)
	}
	return nil
}

func (s *Snail) generateBaseOrganMaturityRate() float64 {
	g := s.genes["maturity_a1"]
	mod, err := g.GetModifier()
	if err != nil {
		common.Log.Error(err)
	}
	return mod
}

func (s *Snail) generateImmunity() error {
	g := s.genes["immunity_a1"]
	mod, err := g.GetModifier()
	if err != nil {
		return err
	}
	s.ImmunityMax = 100 * mod
	s.ImmunityPerc = s.ImmunityMax

	gd := s.genes["immunity_degradation_a1"]
	mod, err = gd.GetModifier()
	if err != nil {
		return err
	}
	s.ImmunityRateBase = mod
	return nil
}
