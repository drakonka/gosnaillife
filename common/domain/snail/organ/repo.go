package organ

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
	"strconv"
)

type Repo struct {
	domain.Repo
}

func NewRepo(database databases.Database) (*Repo, error) {
	r := Repo{}
	if err := r.Init(database); err != nil {
		return nil, err
	}
	r.Tablename = "organs"
	r.Db = database
	return &r, nil
}

func (r *Repo) NewModel() repo.Model {
	m := Organ{}
	m.Repo = r
	return &m
}

func (r *Repo) GetOne(id string, fields []string) (repo.Model, error) {
	row, err := r.Db.GetRow(r.Tablename, id, fields)
	if err == nil {
		m, err := GetOrganFromRowResult(row)
		mId := strconv.Itoa(m.Id)
		if mId == id {
			m.Repo = r
			return m, err
		}
	}
	return nil, err
}

func (r *Repo) GetMany(where string, params []interface{}, fields []string) (models []interface{}, err error) {
	s := Organ{}
	res, err := r.Repo.GetMany(&s.Model, fields, where, params)
	if err != nil {
		error2.HandleErr(err, "GetMany [Get Organ Rows]")
		return models, err
	}
	for _, s := range res {
		model, err := GetOrganFromRowResult(s)
		if err != nil {
			error2.HandleErr(err, "GetMany[Decode Organ Map]")
			return nil, err
		}
		model.Repo = r
		models = append(models, model)
	}
	return models, err
}

func (r *Repo) InsertOne(item interface{}) (id interface{}, err error) {
	id = -1
	if r.Db == nil {
		return id, &repo.RepoError{Err: "The database associated with this repo could not be found."}
	}
	model, ok := item.(*Organ)
	if !ok {
		msg := fmt.Sprintf("The provided item is not an organ; it is a %v", reflect.TypeOf(item))
		return id, &repo.RepoError{Err: msg}
	}

	err, id = r.ValidateAndInsertModel(model)
	if _, ok := id.(int); !ok {
		si := fmt.Sprintf("%v", id)
		id, _ = strconv.Atoi(si)
	}
	return id, err
}

func (r *Repo) UpdateOne(itemId interface{}, fields map[string]string) error {
	m, err := r.GetOne(itemId.(string), []string{"snail_id"})

	model, ok := m.(*Organ)
	if !ok {
		msg := fmt.Sprintf("The provided item is not an organ; it is a %v", reflect.TypeOf(m))
		return &repo.RepoError{Err: msg}
	}
	modelMap := domain.MakeModelMap(model, model.GetDbTagMap(reflect.TypeOf(*model)), fields)

	newModel, err := GetOrganFromRowResult(modelMap)
	if err != nil {
		msg := fmt.Sprintf("Could not update organ: %s", err.Error())
		return &repo.RepoError{Err: msg}
	}
	newModel.Id = model.Id

	err = r.ValidateAndUpdateModel(newModel, modelMap)
	return err
}

func (r *Repo) updateOneModel(m repo.Model, tagMap map[string]string, fields []string) error {
	id := strconv.Itoa(m.GetId().(int))
	mp := r.UpdateOneModelPrep(m, tagMap, fields)
	return r.UpdateOne(id, mp)
}

func GetOrganFromRowResult(r databases.RowResult) (s *Organ, err error) {
	var m Organ
	model, err := domain.GetModelFromRowResult(&m, r)
	if err == nil {
		s = model.(*Organ)
	}
	//	loadOrganKind(&m)
	return s, err
}
