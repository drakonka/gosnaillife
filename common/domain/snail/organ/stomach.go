package organ

type stomach struct {
	SnailBaseOrganKind
}

func NewStomach() Organ {
	t := stomach{}
	t.Init()
	o := newOrgan(&t)
	return o
}

func (t *stomach) Init() error {
	t.IdealQuant = 1
	t.Id = "stomach"
	return nil
}
