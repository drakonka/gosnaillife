package organ

import (
	"fmt"
	"github.com/go-errors/errors"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"math"
	"reflect"
	"sort"
	"strconv"
	"time"
)

type Organ struct {
	domain.Model          `db:"-"`
	Id                    int       `db:"organ_id"`
	OrganTypeId           string    `db:"organ_type_id"`
	SnailId               int       `db:"snail_id"`
	MaxEfficiency         int       `db:"max_efficiency"`
	CurrentEfficiencyPerc int       `db:"current_efficiency_perc"`
	WeightMg              float64   `db:"weight_mg"`
	LastCheck             time.Time `db:"last_check_date"`
	MaturityRate          float64   `db:"maturity_rate"`    // Per Day
	MaturityLevel         float64   `db:"current_maturity"` // 0 - 1
	owner                 organism.Organism
	kind                  organism.OrganKindReaderInitializer `db:"-"`
}

func newOrgan(t organism.OrganKindReaderInitializer) Organ {
	o := Organ{}
	o.OrganTypeId = t.GetId()
	o.kind = t
	o.WeightMg = 0
	o.LastCheck = time.Now()
	return o
}

// Kind returns the organ kind of the owner
func (m *Organ) Kind() (organism.OrganKindReaderInitializer, error) {
	if len(m.OrganTypeId) == 0 {
		return nil, errors.New("this organ has no OrganTypeId set")
	}
	if m.kind == nil {
		err := loadOrganKind(m)
		if err != nil {
			return nil, err
		}
	}
	return m.kind, nil
}

// SetOwner sets the owner of the organism, but does not explicitly save it to the database
func (m *Organ) SetOwner(o organism.Organism) {
	m.owner = o
}

// Owner returns the owner of the organism, loading it if necessary.
func (m *Organ) Owner() (organism.Organism, error) {
	if m.SnailId == 0 && m.owner == nil {
		return nil, domain.ErrOwnerNotAssigned
	}
	if m.owner == nil {
		return m.loadOwner()
	}
	return m.owner, nil
}

func (m *Organ) loadOwner() (organism.Organism, error) {
	if m.Repo == nil {
		return nil, errors.New("Organ repository does not exist")
	}
	s, err := m.Repo.GetOne(strconv.Itoa(m.SnailId), nil)
	if err != nil {
		return nil, err
	}
	if s == nil {
		return nil, errors.New("organ must have an owner")
	}
	o := s.(organism.Organism)
	m.owner = o
	return o, nil
}

// Validator

func (m *Organ) GetRules() []validator.Rule {
	var validTypeIds []interface{}
	for k := range OrganKindTypeRegistry {
		validTypeIds = append(validTypeIds, k)
	}

	validOrganTypeId := validator.NewSelectionRule(m.OrganTypeId, validTypeIds, "OrganTypeId")
	organWeight := validator.NewMinMaxRule(0.00, 32767.00, m.WeightMg, "WeightMg")
	snailId := validator.NewMinMaxRule(1.00, -999.00, m.SnailId, "SnailId")
	rules := []validator.Rule{
		&validOrganTypeId,
		&organWeight,
		&snailId,
	}
	return rules
}

func (m *Organ) Validate() error {
	rules := m.GetRules()
	return m.Model.Validate(rules, "Organ")
}

func (m *Organ) ValidateSpecific(fieldsToValidate ...string) []error {
	rules := m.GetRules()
	tagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	return m.Model.ValidateSpecific(rules, tagMap, fieldsToValidate, "Organ")
}

// End Validator

func (m *Organ) GetId() interface{} {
	return m.Id
}

func (m *Organ) SetId(id interface{}) {
	m.Id = id.(int)
}

func (m *Organ) Save() (interface{}, error) {
	return m.Repo.InsertOne(m)
}

func (m *Organ) Update(fields []string) error {
	dbTagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	r := m.Repo.(*Repo)
	return r.updateOneModel(m, dbTagMap, fields)
}

func (m *Organ) IsMature() bool {
	if m.MaturityLevel < 1 {
		return false
	}
	return true
}

func (m *Organ) UpdateMaturity(now func() time.Time) {
	if m.IsMature() {
		return
	}
	lastCheck := m.LastCheck

	diff := now().Sub(lastCheck)
	minsSinceLastCheck := diff.Minutes()
	// One 'full' tick is 1 day
	rate := m.MaturityRate
	growth := (rate * minsSinceLastCheck) / 1440
	m.MaturityLevel += growth
	m.MaturityLevel = math.Round(m.MaturityLevel*10000) / 10000
}

func (m *Organ) TryExpressGene(gene organism.Gene) error {
	kind, err := m.Kind()
	if err != nil {
		return err
	}
	organKindType := reflect.TypeOf(kind)
	expresser := gene.GetExpresser()
	if !organKindType.Implements(expresser) {
		return nil
	}
	for i := 0; i < expresser.NumMethod(); i++ {
		methodName := expresser.Method(i).Name
		method := reflect.ValueOf(kind).MethodByName(methodName)

		valOfOrgan := reflect.ValueOf(m)
		valOfTime := reflect.ValueOf(time.Now)
		method.Call([]reflect.Value{valOfOrgan, valOfTime})
	}
	return nil
}

func loadOrganKind(o *Organ) error {
	to := OrganKindTypeRegistry[o.OrganTypeId]
	if to == nil {
		return &repo.RepoError{Err: fmt.Sprintf("Organ type %s could not be found in type registry.", o.OrganTypeId)}
	}
	typer, err := InstantiateOrganKind(to)
	if err != nil {
		error2.HandleErr(err, "loadOrganKind")
		return err
	}
	err = typer.Init()
	o.kind = typer
	return err
}

func InstantiateOrganKind(t reflect.Type) (organism.OrganKindReaderInitializer, error) {
	e := reflect.New(t)
	kind := e.Interface().(organism.OrganKindReaderInitializer)
	return kind, nil
}

var OrganKindTypeRegistry = map[string]reflect.Type{
	"brain":       reflect.TypeOf(brain{}),
	"dart":        reflect.TypeOf(dart{}),
	"eye":         reflect.TypeOf(eye{}),
	"foot":        reflect.TypeOf(foot{}),
	"gonad":       reflect.TypeOf(gonad{}),
	"heart":       reflect.TypeOf(heart{}),
	"inner_shell": reflect.TypeOf(innershell{}),
	"jaw":         reflect.TypeOf(jaw{}),
	"kidney":      reflect.TypeOf(kidney{}),
	"lung":        reflect.TypeOf(lung{}),
	"mucus_gland": reflect.TypeOf(mucusgland{}),
	"outer_shell": reflect.TypeOf(outershell{}),
	"radula":      reflect.TypeOf(radula{}),
	"rectum":      reflect.TypeOf(rectum{}),
	"stomach":     reflect.TypeOf(stomach{}),
	"tentacle":    reflect.TypeOf(tentacle{}),
	"vagina":      reflect.TypeOf(vagina{}),
}

var OrganInstantiators = map[string]func() Organ{
	"brain":       NewBrain,
	"dart":        NewDart,
	"eye":         NewEye,
	"foot":        NewFoot,
	"gonad":       NewGonad,
	"heart":       NewHeart,
	"inner_shell": NewInnerShell,
	"jaw":         NewJaw,
	"kidney":      NewKidney,
	"lung":        NewLung,
	"mucus_gland": NewMucusGland,
	"outer_shell": NewOuterShell,
	"radula":      NewRadula,
	"rectum":      NewRectum,
	"stomach":     NewStomach,
	"tentacle":    NewTentacle,
	"vagina":      NewVagina,
}

type SnailBaseOrganKind struct {
	organism.BaseOrganKind
}

func (m *SnailBaseOrganKind) ExpressSizeA1(o *Organ, now func() time.Time) error {
	// We need to get time since last organ check
	owner, err := o.Owner()
	if err != nil {
		return err
	}
	if o.IsMature() {
		return nil
	}
	// Find both SizeA1 genes and express according to dominance
	gene := owner.Genes()["size_a1"]
	alleles := gene.GetAlleles()
	// Sort alleles by dominance
	sort.Sort(alleles)

	// Handle different dominance types
	// The organ type shouldn't really have to care what TYPE
	// of dominance the gene is set to..it should handle whatever cases
	// are possible..maybe...not sure yet.
	switch gene.GetDominanceType() {
	case organism.Complete:
		lastCheck := o.LastCheck

		now := now()
		diff := now.Sub(lastCheck)
		minsSinceLastCheck := diff.Minutes()
		// One 'full' tick is 1 day
		modifier := alleles[0].GetModifier()
		growth := (modifier * minsSinceLastCheck) / 1440
		o.WeightMg += growth
		o.WeightMg = math.Round(o.WeightMg*1000) / 1000
		return nil
	default:
		msg := fmt.Sprintf("Unexpected dominance type: %v", gene.GetDominanceType())
		return errors.New(msg)
	}
}
