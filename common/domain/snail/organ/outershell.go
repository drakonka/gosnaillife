package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type outershell struct {
	SnailBaseOrganKind
}

func NewOuterShell() Organ {
	t := outershell{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewOuterShell")
	}
	o := newOrgan(&t)
	return o
}

func (t *outershell) Init() error {
	t.IdealQuant = 1
	t.Id = "outer_shell"
	return nil
}
