package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type lung struct {
	SnailBaseOrganKind
}

func NewLung() Organ {
	t := lung{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewEye")
	}
	o := newOrgan(&t)
	return o
}

func (t *lung) Init() error {
	t.IdealQuant = 1
	t.Id = "lung"
	return nil
}
