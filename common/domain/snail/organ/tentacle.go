package organ

type tentacle struct {
	SnailBaseOrganKind
}

func NewTentacle() Organ {
	t := tentacle{}
	t.Init()
	o := newOrgan(&t)
	return o
}

func (t *tentacle) Init() error {
	t.IdealQuant = 2
	t.Id = "tentacle"
	return nil
}
