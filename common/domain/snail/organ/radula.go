package organ

type radula struct {
	SnailBaseOrganKind
}

func NewRadula() Organ {
	t := radula{}
	t.Init()
	o := newOrgan(&t)
	return o
}

func (t *radula) Init() error {
	t.IdealQuant = 1
	t.Id = "radula"
	return nil
}
