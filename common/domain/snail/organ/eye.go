package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type eye struct {
	SnailBaseOrganKind
}

func NewEye() Organ {
	t := eye{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewEye")
	}
	o := newOrgan(&t)
	return o
}

func (t *eye) Init() error {
	t.IdealQuant = 2
	t.Id = "eye"
	return nil
}
