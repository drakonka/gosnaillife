package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type lungTest struct {
}

func newLungTest() organTest {
	test := lungTest{}
	return &test
}

func (t *lungTest) prep() error {
	return nil
}

func (t *lungTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Lung",
			f:    organ.NewLung,
			want: nil,
		},
	}
}

func (t *lungTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
