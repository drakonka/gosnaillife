package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type rectumTest struct {
}

func newRectumTest() organTest {
	test := rectumTest{}
	return &test
}

func (t *rectumTest) prep() error {
	return nil
}

func (t *rectumTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Rectum",
			f:    organ.NewRectum,
			want: nil,
		},
	}
}

func (t *rectumTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
