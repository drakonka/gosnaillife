package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type stomachTest struct {
}

func newStomachTest() organTest {
	test := stomachTest{}
	return &test
}

func (t *stomachTest) prep() error {
	return nil
}

func (t *stomachTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Stomach",
			f:    organ.NewStomach,
			want: nil,
		},
	}
}

func (t *stomachTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
