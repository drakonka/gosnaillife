package tests

import (
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo/mocks"
	"os"
	"strconv"
	"testing"
)

var organTests []organTest

func TestMain(m *testing.M) {
	tests := []organTest{
		newBrainTest(),
		newDartTest(),
		newEyeTest(),
		newFootTest(),
		newGonadTest(),
		newHeartTest(),
		newInnerShellTest(),
		newJawTest(),
		newKidneyTest(),
		newLungTest(),
		newMucusGlandTest(),
		newOuterShellTest(),
		newRadulaTest(),
		newRectumTest(),
		newStomachTest(),
		newTentacleTest(),
		newVaginaTest(),
	}

	for _, test := range tests {
		err := test.prep()
		if err == nil {
			organTests = append(organTests, test)
		} else {
			fmt.Printf("\nError when prepping test: %s\n", err.Error())
			return
		}
	}
	fmt.Printf("\nCount of repo tests to run: %d\n", len(organTests))

	ret := m.Run()
	fmt.Println("Deleting test DB")
	os.Exit(ret)
}

func TestNewOrgan(t *testing.T) {
	for _, test := range organTests {
		testcases := test.getNewOrganTest()
		for _, tc := range testcases {
			t.Run(tc.name, func(t *testing.T) {
				tc.f()
			})
		}
	}
}

func TestGetLabel(t *testing.T) {
	testCases := []struct {
		name  string
		organ organ.Organ
		want  string
	}{
		{name: "lung", organ: organ.NewLung(), want: "Lung"},
		{name: "mucus_gland", organ: organ.NewMucusGland(), want: "Mucus Gland"},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ot, err := tc.organ.Kind()
			if err != nil {
				t.Error(testing2.TCFailedMsg("OrganTest", tc.name, err))
				return
			}
			label := ot.GetLabel()
			if label != tc.want {
				ctx := fmt.Sprintf("Expected label %s, got %s", tc.want, label)
				t.Error(testing2.TCFailedMsg("OrganTest", tc.name, ctx))

			}
		})
	}
}

func TestGetOwner(t *testing.T) {
	testCases := []struct {
		name          string
		makeSnailFunc func(t *testing.T) (*snail.Snail, error)
		want          error
	}{
		{name: "GetOwnerUnassigned", makeSnailFunc: makeRandomSnailWithoutId, want: domain.ErrOwnerNotAssigned},
		{name: "GetOwnerWithOwnerId", makeSnailFunc: makeRandomSnailWithId, want: nil},
		{name: "GetOwnerWithOrganOwnerSet", makeSnailFunc: makeRandomSnailWithOrganOwnerSet, want: nil},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s, err := tc.makeSnailFunc(t)
			if err != nil {
				t.Error(testing2.TCFailedMsg("OrganTest", tc.name, err))

				return
			}
			o := s.Organs[0]
			owner, err := o.Owner()
			if e2 := testing2.TestErrorType(tc.want, err); e2 != nil {
				t.Fatal(e2)
			} else if owner != nil && owner != s {
				t.Error(testing2.TCFailedMsg("OrganTest", tc.name, fmt.Sprintf("expected owner %v, got %v", s, owner)))
			}
		})
	}
}

func TestGetOrganKind(t *testing.T) {
	testCases := []struct {
		name  string
		organ organ.Organ
		want  error
	}{
		{name: "OrganWithLungType", organ: organ.Organ{OrganTypeId: "lung"}, want: nil},
		{name: "OrganWithoutType", organ: organ.Organ{}, want: errors.New("this organ has no OrganTypeId set")},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			kind, err := tc.organ.Kind()
			if tc.want != nil {
				if err == nil {
					t.Error(testing2.TCFailedMsg("OrganTest", tc.name, fmt.Sprintf("expected error %v, got nil", tc.want)))
				} else if err.Error() != tc.want.Error() {
					t.Error(testing2.TCFailedMsg("OrganTest", tc.name, fmt.Sprintf("expected error %v, got %v", tc.want, err)))
				}
			} else if kind.GetId() != tc.organ.OrganTypeId {
				ctx := fmt.Sprintf("expected kind id '%s', got '%s'", kind.GetId(), tc.organ.OrganTypeId)
				t.Error(testing2.TCFailedMsg("OrganTest", tc.name, ctx))
			}
		})
	}
}

func makeRandomSnailWithoutId(t *testing.T) (*snail.Snail, error) {
	snail, err := snail.GenerateRandomSnail(0, nil)
	snail.Organs[0].SnailId = 0
	snail.Organs[0].SetOwner(nil)
	return snail, err
}

func makeRandomSnailWithId(t *testing.T) (*snail.Snail, error) {
	snail, err := snail.GenerateRandomSnail(0, nil)
	snail.Id = 10
	snail.Organs[0].SnailId = snail.Id
	snail.Organs[0].SetOwner(nil)
	c := gomock.NewController(t)
	repo := mocks.NewMockRepo(c)
	snail.Organs[0].Repo = repo
	repo.EXPECT().GetOne(strconv.Itoa(snail.Id), nil).Return(snail, nil)
	return snail, err
}

func makeRandomSnailWithOrganOwnerSet(t *testing.T) (*snail.Snail, error) {
	snail, err := snail.GenerateRandomSnail(0, nil)
	return snail, err
}
