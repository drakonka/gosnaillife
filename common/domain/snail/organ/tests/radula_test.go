package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type radulaTest struct {
}

func newRadulaTest() organTest {
	test := radulaTest{}
	return &test
}

func (t *radulaTest) prep() error {
	return nil
}

func (t *radulaTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Radula",
			f:    organ.NewRadula,
			want: nil,
		},
	}
}

func (t *radulaTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
