package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type eyeTest struct {
}

func newEyeTest() organTest {
	test := eyeTest{}
	return &test
}

func (t *eyeTest) prep() error {
	return nil
}

func (t *eyeTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Eye",
			f:    organ.NewEye,
			want: nil,
		},
	}
}

func (t *eyeTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
