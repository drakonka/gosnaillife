package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type vaginaTest struct {
}

func newVaginaTest() organTest {
	test := vaginaTest{}
	return &test
}

func (t *vaginaTest) prep() error {
	return nil
}

func (t *vaginaTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Vagina",
			f:    organ.NewVagina,
			want: nil,
		},
	}
}

func (t *vaginaTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
