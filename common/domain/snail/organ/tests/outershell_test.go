package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type outerShellTest struct {
}

func newOuterShellTest() organTest {
	test := outerShellTest{}
	return &test
}

func (t *outerShellTest) prep() error {
	return nil
}

func (t *outerShellTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "outershell",
			f:    organ.NewOuterShell,
			want: nil,
		},
	}
}

func (t *outerShellTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
