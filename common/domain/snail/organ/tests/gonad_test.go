package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type gonadTest struct {
}

func newGonadTest() organTest {
	test := gonadTest{}
	return &test
}

func (t *gonadTest) prep() error {
	return nil
}

func (t *gonadTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Gonad",
			f:    organ.NewGonad,
			want: nil,
		},
	}
}

func (t *gonadTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
