package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type mucusGlandTest struct {
}

func newMucusGlandTest() organTest {
	test := mucusGlandTest{}
	return &test
}

func (t *mucusGlandTest) prep() error {
	return nil
}

func (t *mucusGlandTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "MucusGland",
			f:    organ.NewMucusGland,
			want: nil,
		},
	}
}

func (t *mucusGlandTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
