package tests

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"testing"
)

type jawTest struct {
}

func newJawTest() organTest {
	test := jawTest{}
	return &test
}

func (t *jawTest) prep() error {
	return nil
}

func (t *jawTest) getNewOrganTest() []newOrganTestCase {
	return []newOrganTestCase{
		{
			name: "Jaw",
			f:    organ.NewJaw,
			want: nil,
		},
	}
}

func (t *jawTest) getAdditionalTests() []func(t *testing.T) {
	return nil
}
