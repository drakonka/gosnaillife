package organ

import error2 "gitlab.com/drakonka/gosnaillife/common/error"

type heart struct {
	SnailBaseOrganKind
}

func NewHeart() Organ {
	t := heart{}
	err := t.Init()
	if err != nil {
		error2.HandleErr(err, "NewEye")
	}
	o := newOrgan(&t)
	return o
}

func (t *heart) Init() error {
	t.IdealQuant = 1
	t.Id = "heart"
	return nil
}
