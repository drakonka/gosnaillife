package gene

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
	"strconv"
)

type Repo struct {
	domain.Repo
}

func NewRepo(database databases.Database) (*Repo, error) {
	r := Repo{}
	if err := r.Init(database); err != nil {
		return nil, err
	}
	r.Tablename = "snail_genes"
	r.Db = database
	return &r, nil
}

func (r *Repo) NewModel() repo2.Model {
	m := Allele{}
	m.Repo = r
	return &m
}

func (repo *Repo) GetOne(id string, fields []string) (repo2.Model, error) {
	row, err := repo.Db.GetRow(repo.Tablename, id, fields)
	if err == nil {
		m, err := GetAlleleFromRowResult(row)
		mId := strconv.Itoa(m.Id)
		if mId == id {
			m.Repo = repo
			if m.GeneId != "" {
				err = m.Init()
				if err != nil {
					return nil, err
				}
			}
			return m, err
		}
	}
	return nil, err
}

func (repo *Repo) GetMany(where string, params []interface{}, fields []string) (models []interface{}, err error) {
	s := Allele{}
	res, err := repo.Repo.GetMany(&s.Model, fields, where, params)
	if err != nil {
		error2.HandleErr(err, "GetMany [Get Stable Rows]")
		return models, err
	}
	for _, s := range res {
		model, err := GetAlleleFromRowResult(s)
		if err != nil {
			error2.HandleErr(err, "GetMany[Decode Stable Map]")
			continue
		}
		model.Repo = repo
		model.Init()
		models = append(models, model)
	}
	return models, err
}

func (repo *Repo) InsertOne(item interface{}) (id interface{}, err error) {
	id = -1
	if repo.Db == nil {
		return id, &repo2.RepoError{Err: "The database associated with this repo could not be found."}
	}
	model, ok := item.(*Allele)
	if !ok {
		msg := fmt.Sprintf("The provided item is not an allele; it is a %v", reflect.TypeOf(item))
		return id, &repo2.RepoError{Err: msg}
	}

	err, id = repo.ValidateAndInsertModel(model)
	if _, ok := id.(int); !ok {
		si := fmt.Sprintf("%v", id)
		id, _ = strconv.Atoi(si)
	}
	return id, err
}

func (repo *Repo) UpdateOne(itemId interface{}, fields map[string]string) error {
	m, err := repo.GetOne(itemId.(string), []string{"allele_id"})

	model, ok := m.(*Allele)
	if !ok {
		msg := fmt.Sprintf("The provided item is not an allele; it is a %v", reflect.TypeOf(m))
		return &repo2.RepoError{Err: msg}
	}
	modelMap := domain.MakeModelMap(model, model.GetDbTagMap(reflect.TypeOf(*model)), fields)

	newModel, err := GetAlleleFromRowResult(modelMap)
	if err != nil {
		msg := fmt.Sprintf("Could not update allele: %s", err.Error())
		return &repo2.RepoError{Err: msg}
	}
	newModel.Id = model.Id

	err = repo.ValidateAndUpdateModel(newModel, modelMap)
	return err
}

func GetAlleleFromRowResult(r databases.RowResult) (s *Allele, err error) {
	var m Allele
	model, err := domain.GetModelFromRowResult(&m, r)
	if err == nil {
		s = model.(*Allele)
	}
	return s, err
}
