package gene

import (
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
)

type Allele struct {
	organism.Allele
	domain.Model
	Id      int    `db:"allele_id"`
	GeneId  string `db:"gene_id"`
	OwnerId int    `db:"owner_id"`
	Key     string `db:"allele_key"`
}

func (m *Allele) Init() error {
	genetype := GeneRegistry[m.GeneId]
	newgene, err := InstantiateGene(genetype)
	if err != nil {
		return err
	}
	err = newgene.Init()
	if err != nil {
		return err
	}
	for _, variation := range newgene.GetPossibleAlleles() {
		if variation.GetKey() == m.Key {
			m.Modifier = variation.GetModifier()
			m.DominanceLevel = variation.GetDominanceLevel()
			break
		}
	}
	return nil
}

func (m *Allele) GetId() interface{} {
	return m.Id
}

// Validator

func (m *Allele) GetRules() []validator.Rule {
	var validTypeIds []interface{}
	for k := range GeneRegistry {
		validTypeIds = append(validTypeIds, k)
	}

	validGeneId := validator.NewSelectionRule(m.GeneId, validTypeIds, "GeneId")

	rules := []validator.Rule{
		&validGeneId,
	}
	return rules
}

func (m *Allele) Validate() error {
	rules := m.GetRules()
	return m.Model.Validate(rules, "Organ")
}

func (m *Allele) ValidateSpecific(fieldsToValidate ...string) []error {
	rules := m.GetRules()
	tagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	return m.Model.ValidateSpecific(rules, tagMap, fieldsToValidate, "Jar")
}

// End Validator

func (m *Allele) SetOwnerId(id int) {
	m.OwnerId = id
}

func (m *Allele) GetKey() string {
	return m.Key
}

func (m *Allele) GetGeneId() string {
	return m.GeneId
}

func (m *Allele) GetModifier() float64 {
	return m.Modifier
}

func (m *Allele) GetDominanceLevel() int {
	return m.DominanceLevel
}

func (m *Allele) Update(fields []string) error {
	return &repo.RepoError{Err: "an Allele does not support modification after it is saved"}
}

func (m *Allele) Save() (interface{}, error) {
	return m.Repo.InsertOne(m)
}
