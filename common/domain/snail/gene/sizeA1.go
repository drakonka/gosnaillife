package gene

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"reflect"
	"time"
)

type SizeA1 struct {
	organism.GeneBase
}

func (g *SizeA1) Init() error {
	g.Id = "size_a1"
	g.DominanceType = organism.Complete

	var e *SizeA1Expresser
	g.Expresser = reflect.TypeOf(e).Elem()
	g.PossibleAlleles = g.getVariations()
	return nil
}

func (g *SizeA1) GetGeneId() string {
	return g.Id
}

func (g *SizeA1) getVariations() organism.Alleles {
	// Variations are "Big" and "Small"
	bigA := Allele{
		GeneId: g.Id,
		Key:    "S",
	}
	bigA.Modifier = 1.00
	bigA.DominanceLevel = 1

	smallA := Allele{
		GeneId: g.Id,
		Key:    "s",
	}
	smallA.Modifier = 0.05
	smallA.DominanceLevel = 0

	return organism.Alleles{&bigA, &smallA}
}

type SizeA1Expresser interface {
	ExpressSizeA1(o *organ.Organ, now func() time.Time) error
}
