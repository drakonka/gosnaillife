package gene

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"reflect"
)

var GeneRegistry = map[string]reflect.Type{
	"size_a1":                 reflect.TypeOf(SizeA1{}),
	"maturity_a1":             reflect.TypeOf(MaturityA1{}),
	"immunity_a1":             reflect.TypeOf(ImmunityA1{}),
	"immunity_degradation_a1": reflect.TypeOf(ImmunityDegradationA1{}),
}

func InstantiateGene(t reflect.Type) (organism.Gene, error) {
	e := reflect.New(t)
	typer := e.Interface().(organism.Gene)
	return typer, nil
}
