package gene

import (
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"reflect"
	"time"
)

type ImmunityA1 struct {
	organism.GeneBase
}

func (g *ImmunityA1) Init() error {
	g.Id = "immunity_a1"
	g.DominanceType = organism.Complete

	var e *ImmunityA1Expresser
	g.Expresser = reflect.TypeOf(e).Elem()
	g.PossibleAlleles = g.getVariations()
	return nil
}

func (g *ImmunityA1) GetGeneId() string {
	return g.Id
}

func (g *ImmunityA1) getVariations() organism.Alleles {
	// Variations are "Suppressed", "Standard", and "Enhanced"

	// Standard
	standardA := Allele{
		GeneId: g.Id,
		Key:    "S",
	}
	standardA.Modifier = 1.00
	standardA.DominanceLevel = 2

	suppressedA := Allele{
		GeneId: g.Id,
		Key:    "s",
	}
	suppressedA.Modifier = 0.5
	suppressedA.DominanceLevel = 1

	enhancedA := Allele{
		GeneId: g.Id,
		Key:    "e",
	}
	enhancedA.Modifier = 2.00
	enhancedA.DominanceLevel = 0
	return organism.Alleles{&standardA, &suppressedA, &enhancedA}
}

type ImmunityA1Expresser interface {
	ExpressImmunityA1(o *organ.Organ, now func() time.Time) error
}
