package snail

import (
	"gitlab.com/drakonka/gosnaillife/common/domain"
	"gitlab.com/drakonka/gosnaillife/common/domain/organism"
	"gitlab.com/drakonka/gosnaillife/common/domain/snail/organ"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"reflect"
	"time"
)

type Snail struct {
	domain.Model     `db:"-"`
	Id               int                      `db:"snail_id"`
	Name             string                   `db:"name"`
	ParentJarId      int                      `db:"parent_jar_id"`
	CurrentJarId     int                      `db:"current_jar_id"`
	BirthStableId    int                      `db:"birth_stable_id"`
	OwnerId          int                      `db:"owner_id"`
	Organs           []*organ.Organ           `db:"-"`
	genes            map[string]organism.Gene `db:"-"`
	ImmunityPerc     float64                  `db:"immunity_perc"`
	ImmunityRateBase float64                  `db:"immunity_rate_base"`
	ImmunityMax      float64                  `db:"immunity_max"`
	BirthDate        time.Time                `db:"birth_date"`
	DeathDate        time.Time                `db:"death_date"`
	CreatedAt        time.Time                `db:"created_at"`
	PosX             int                      `db:"pos_x"`
	PosY             int                      `db:"pos_y"`
	PosZ             int                      `db:"pos_z"`
	LastCheck        time.Time                `db:"last_check_date"`
}

func (m *Snail) InitOrganism() error {
	m.genes = make(map[string]organism.Gene)
	return nil
}

func (m *Snail) GetId() interface{} {
	return m.Id
}

func (m *Snail) SetId(id interface{}) {
	m.Id = id.(int)
}

func (m *Snail) Genes() map[string]organism.Gene {
	return m.genes
}

// Validator

func (m *Snail) GetRules() []validator.Rule {
	nameLength := validator.NewLengthRule(5, 50, m.Name, "Name")

	rules := []validator.Rule{
		&nameLength,
	}
	return rules
}

func (m *Snail) Validate() error {
	rules := m.GetRules()
	return m.Model.Validate(rules, "Snail")
}

func (m *Snail) ValidateSpecific(fieldsToValidate ...string) []error {
	rules := m.GetRules()
	tagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	return m.Model.ValidateSpecific(rules, tagMap, fieldsToValidate, "Snail")
}

// End Validator
func (m *Snail) Save() (interface{}, error) {
	return m.Repo.InsertOne(m)
}

func (m *Snail) Update(fields []string) error {
	dbTagMap := m.GetDbTagMap(reflect.TypeOf(*m))
	r := m.Repo.(*Repo)
	return r.updateOneModel(m, dbTagMap, fields)
}

func (m *Snail) AgeDays() float64 {
	age := time.Since(m.BirthDate).Hours() / 24.00
	return age
}

func (m *Snail) IsMature() bool {
	for _, o := range m.Organs {
		if o.MaturityLevel < 1 {
			return false
		}
	}
	return true
}

func (m *Snail) PutInJar(jarId int, width, depth uint16) error {
	m.CurrentJarId = jarId
	m.PosZ = 0
	w := int(width / 2)
	d := int(depth / 2)
	m.PosX = util.GenRandInt(-w, w)
	m.PosY = util.GenRandInt(-d, d)
	return nil
}

func (m *Snail) GetOrgan(organType string) *organ.Organ {
	for _, organ := range m.Organs {
		if organ.OrganTypeId == organType {
			return organ
		}
	}
	return nil
}

func (m *Snail) GetGene(geneType string) *organism.Gene {
	for geneKey, gene := range m.genes {
		if geneKey == geneType {
			return &gene
		}
	}
	return nil
}

func (m *Snail) setOrgans(o []*organ.Organ) {
	m.Organs = o
}
