package user

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/drakonka/gosnaillife/common/domain"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
)

type Repo struct {
	domain.Repo
}

func NewRepo(database databases.Database) (*Repo, error) {
	r := Repo{}
	if err := r.Init(database); err != nil {
		return nil, err
	}
	r.Tablename = "users"
	r.Db = database
	return &r, nil
}

func (r *Repo) NewModel() repo2.Model {
	m := User{}
	m.Repo = r
	return &m
}

func (repo *Repo) GetOne(id string, fields []string) (repo2.Model, error) {
	row, err := repo.Db.GetRow(repo.Tablename, id, fields)
	if err == nil {
		u, err := GetUserFromRowResult(row)
		uId := u.Id
		if uId == id {
			u.Repo = repo
			return u, err
		}
	}
	return nil, err
}

func (repo *Repo) GetMany(where string, params []interface{}, fields []string) (models []interface{}, err error) {
	s := User{}
	res, err := repo.Repo.GetMany(&s.Model, fields, where, params)
	if err != nil {
		error2.HandleErr(err, "GetMany [Get Stable Rows]")
		return models, err
	}
	for _, s := range res {
		var model User
		err = mapstructure.Decode(s, &model)
		if err != nil {
			error2.HandleErr(err, "GetMany")
			continue
		}
		models = append(models, model)
	}
	return models, err
}

func (repo *Repo) InsertOne(item interface{}) (id interface{}, err error) {
	id = -1
	if repo.Db == nil {
		return id, &repo2.RepoError{Err: "The database associated with this Repo could not be found."}
	}
	model, ok := item.(*User)
	if !ok {
		msg := fmt.Sprintf("The provided item is not a *User; it is a %v", reflect.TypeOf(item))
		return id, &repo2.RepoError{Err: msg}
	}

	err, id = repo.ValidateAndInsertModel(model)
	if _, ok := id.(string); !ok {
		id = fmt.Sprintf("%v", id)
	}
	return id, err
}

func (repo *Repo) UpdateOne(itemId interface{}, fields map[string]string) error {
	// Retrieve stable with above id
	m, err := repo.GetOne(itemId.(string), []string{})

	model, ok := m.(*User)
	if !ok {
		msg := fmt.Sprintf("The provided item is not a User; it is a %v", reflect.TypeOf(model))
		return &repo2.RepoError{Err: msg}
	}
	modelMap := domain.MakeModelMap(model, model.GetDbTagMap(reflect.TypeOf(*model)), fields)
	newModel, err := GetUserFromRowResult(modelMap)
	if err != nil {
		msg := fmt.Sprintf("Could not update stable: %s", err.Error())
		return &repo2.RepoError{Err: msg}
	}
	newModel.Id = model.Id

	err = repo.ValidateAndUpdateModel(newModel, modelMap)
	return err
}

func (r *Repo) updateOneModel(m repo2.Model, tagMap map[string]string, fields []string) error {
	id := m.GetId().(string)
	mp := r.UpdateOneModelPrep(m, tagMap, fields)
	return r.UpdateOne(id, mp)
}

func GetUserFromRowResult(r databases.RowResult) (m *User, err error) {
	var user User
	model, err := domain.GetModelFromRowResult(&user, r)
	if err == nil {
		m = model.(*User)
	}
	return m, err
}
