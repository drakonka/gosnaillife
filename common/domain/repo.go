package domain

import (
	"fmt"
	"github.com/fatih/structs"
	"github.com/go-errors/errors"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/drakonka/gosnaillife/common"
	error2 "gitlab.com/drakonka/gosnaillife/common/error"
	"gitlab.com/drakonka/gosnaillife/common/infrastructure/validator"
	"gitlab.com/drakonka/gosnaillife/common/util"
	"gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/databases"
	repo2 "gitlab.com/drakonka/gosnaillife/server/lib/infrastructure/repo"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type Repo struct {
	repo2.Repo
	Db        databases.Database
	Tablename string
}

func (r *Repo) Init(db databases.Database) error {
	if db == nil {
		return &repo2.RepoError{Err: "Repo Db can't be nil"}
	}
	return nil
}

func (r *Repo) GetDb() databases.Database {
	return r.Db
}

func GetModelFromRowResult(m repo2.Model, r databases.RowResult) (repo2.Model, error) {
	if r == nil {
		return nil, &repo2.RepoError{Err: "Row result is empty"}
	}
	stringToDateTimeHook := func(
		f reflect.Type,
		t reflect.Type,
		data interface{}) (interface{}, error) {
		if t == reflect.TypeOf(time.Time{}) && f == reflect.TypeOf("") {
			return time.Parse("2006-01-02 15:04:05", data.(string))
		}

		return data, nil
	}

	dc := mapstructure.DecoderConfig{
		DecodeHook:       stringToDateTimeHook,
		TagName:          "db",
		Result:           &m,
		WeaklyTypedInput: true,
	}
	decoder, err := mapstructure.NewDecoder(&dc)
	if err == nil {
		err = decoder.Decode(&r)
	}
	return m, err
}

func (r *Repo) GetMany(m repo2.Model, fields []string, where string, params []interface{}) ([]databases.RowResult, error) {
	t := reflect.TypeOf(m).Elem()
	tagMap := m.BuildDbTagMap(t)
	fieldsToGet := fieldsToDbCols(tagMap, fields)

	res, err := r.getRows(where, params, fieldsToGet)
	if err != nil {
		error2.HandleErr(err, "GetMany [Get Stable Rows]")
		return nil, err
	}
	return res, err
}

func (r *Repo) getRows(where string, params []interface{}, fields []string) (res []databases.RowResult, err error) {
	if r.Db == nil {
		return nil, errors.New("Could not find a database for this repository")
	}
	res, err = r.Db.GetRows(r.Tablename, where, params, fields)
	return res, err
}

func (r *Repo) ValidateAndUpdateModel(model repo2.Model, fields map[string]interface{}) error {
	if validatable, ok := model.(validator.Validatable); ok {

		keys := make([]string, 0, len(fields))
		for k := range fields {
			keys = append(keys, k)
		}

		// Only Validate the fields we are actually trying to update.
		err := validatable.ValidateSpecific(keys...)
		if len(err) > 0 {
			var allErrs string
			for _, e := range err {
				allErrs += fmt.Sprintf(" %s", e.Error())
			}
			return &validator.ValidationError{Err: allErrs}
		}
	}
	id := fmt.Sprintf("%v", model.GetId())

	fieldsToUpdate := map[string]string{}
	for k, v := range fields {
		fieldsToUpdate[k] = fmt.Sprintf("%v", v)
	}

	err := r.Db.Update(r.Tablename, id, fieldsToUpdate)
	return err
}

func (r *Repo) ValidateAndInsertModel(model repo2.Model) (error, interface{}) {
	if validatable, ok := model.(validator.Validatable); ok {
		err := validatable.Validate()
		if err != nil {
			return err, nil
		}
	}
	id, err := r.prepAndInsert(model)
	return err, id
}

func (r *Repo) prepAndInsert(model repo2.Model) (id int64, err error) {
	ms := structToDbMap(model)
	id, err = r.Db.Insert(r.Tablename, ms)
	return id, err
}

func (r *Repo) DeleteOne(itemId interface{}) error {
	var id string
	i, ok := itemId.(int)
	if ok {
		id = strconv.Itoa(i)
	} else {
		id = itemId.(string)
	}
	rowcount, err := r.Db.DeleteRow(r.Tablename, id)
	if rowcount == 0 && err == nil {
		return &repo2.RepoError{Err: "Something went wrong! Row count is 0"}
	}
	return err
}

func (r *Repo) UpdateOneModelPrep(m repo2.Model, tagMap map[string]string, fields []string) map[string]string {
	mp := make(map[string]string)
	rval := reflect.ValueOf(m)

	// if a nil slice of fields is given, update EVERY field
	if fields == nil {
		for dbTagName, fieldName := range tagMap {
			if string(fieldName[0]) == strings.ToLower(string(fieldName[0])) {
				continue
			}
			val := reflect.Indirect(rval).FieldByName(fieldName)
			if val.IsValid() {
				value := val.Interface()
				switch v := value.(type) {
				case time.Time:
					value = v.UTC().Format("2006-01-02 15:04:05")
				}

				mp[dbTagName] = fmt.Sprintf("%v", value)
			}
		}
		return mp
	}
	for _, field := range fields {
		val := reflect.Indirect(rval).FieldByName(field)
		if val.IsValid() {
			dbTagName := util.FindInStrMap(tagMap, field)
			mp[dbTagName] = fmt.Sprintf("%v", val.Interface())
		}
	}
	return mp
}

func (r *Repo) FilterFields(fieldsToFilter []string, fields []string) []string {
	for _, fieldToFilter := range fieldsToFilter {
		i := util.CaselessStrIndexOf(fields, fieldToFilter)
		if i != -1 {
			msg := fmt.Sprintf("Removing %s from fields list", fieldToFilter)
			common.Log.Debug(msg)
			fields = util.RemoveFromStrSlice(fields, i)
		}
	}
	return fields
}

func structToDbMap(model repo2.Model) map[string]string {
	st := structs.New(model)
	m := st.Map()
	ms := make(map[string]string)
	for k, v := range m {
		field := st.Field(k)

		if t, ok := v.(time.Time); ok {
			if field.IsZero() {
				continue
			}
			v = t.UTC().Format("2006-01-02 03:04:05")
		}
		s := fmt.Sprintf("%v", v)
		dbname := field.Tag("db")
		if len(s) > 0 && len(dbname) > 0 && dbname != "-" {
			if field.Kind() == reflect.Bool {
				if v == true {
					s = "1"
				} else {
					s = "0"
				}
			}
			ms[dbname] = s
		}
	}
	return ms
}

func MakeModelMap(model repo2.Model, dbTagMap map[string]string, fields map[string]string) map[string]interface{} {
	s := structs.New(model)
	s.TagName = "db"
	modelMap := s.Map()
	essentialModelMap := map[string]interface{}{}
	for k, v := range fields {
		if _, ok := modelMap[k]; !ok {
			// Seems this is not in a db tag format, try to convert
			k = fieldsToDbCols(dbTagMap, []string{k})[0]
		}
		if len(v) > 0 {
			essentialModelMap[k] = v
		}

	}
	return essentialModelMap
}

func fieldsToDbCols(dbTagMap map[string]string, fields []string) []string {
	var dbCols []string
	for _, f := range fields {
		// keys are Db tags, values are field names
		_, ok := dbTagMap[f]
		if ok {
			dbCols = append(dbCols, f)
		} else {
			dbCols = append(dbCols, util.FindInStrMap(dbTagMap, f))
		}
	}
	return dbCols
}
