package organism

import (
	"errors"
	"math/rand"
	"reflect"
)

// Gene is an interface implementing two interfaces to read and initialize genes
type Gene interface {
	GeneReader
	GeneInitializer
}

// GeneReader reads gene information
type GeneReader interface {
	GeneId() string
	GetExpresser() reflect.Type
	GetAlleles() Alleles
	GetDominanceType() DominanceType
	GetPossibleAlleles() Alleles
}

// GeneInitializer initializes a gene
type GeneInitializer interface {
	Init() error
	SetAlleles(a Alleles) error
	GenerateRandomAlleles() error
	GetModifier() (float64, error)
}

// GeneBase is a base struct representing a gene
type GeneBase struct {
	Id              string
	Label           string
	Alleles         Alleles
	DominanceType   DominanceType
	Expresser       reflect.Type
	PossibleAlleles Alleles
}

// Init runs some initialization steps for a gene
func (g *GeneBase) Init() error {
	return nil
}

// GeneId returns the ID of the gene
func (g *GeneBase) GeneId() string {
	return g.Id
}

// GetExpresser returns the interface type an organ would have to implement to express the gene
func (g *GeneBase) GetExpresser() reflect.Type {
	return g.Expresser
}

func (g *GeneBase) GetPossibleAlleles() Alleles {
	return g.PossibleAlleles
}

// GetAlleles returns all alleles/variations for the gene
func (g *GeneBase) GetAlleles() Alleles {
	return g.Alleles
}

func (g *GeneBase) SetAlleles(a Alleles) error {
	g.Alleles = a
	return nil
}

// GetDominanceType returns the dominance type for this gene (eg Complete, incomplete, co-dominance)
func (g *GeneBase) GetDominanceType() DominanceType {
	return g.DominanceType
}

// GenerateRandomAlleles sets two random alleles for this gene
func (g *GeneBase) GenerateRandomAlleles() error {
	r := rand.Intn(len(g.PossibleAlleles))
	g.Alleles = append(g.Alleles, g.PossibleAlleles[r])
	r = rand.Intn(len(g.PossibleAlleles))
	g.Alleles = append(g.Alleles, g.PossibleAlleles[r])
	return nil
}
func (g *GeneBase) GetModifier() (float64, error) {
	a1 := g.Alleles[0]
	a2 := g.Alleles[1]
	switch g.DominanceType {
	case Complete:
		if a1.GetDominanceLevel() > a2.GetDominanceLevel() {
			return a1.GetModifier(), nil
		}
		return a2.GetModifier(), nil
	case Co:
		fallthrough
	case Incomplete:
		return (a1.GetModifier() + a2.GetModifier()) / 2, nil
	}
	return 0, errors.New("could not determine dominant allele")
}

// DominanceType represents the type of dominance associated with the gene
type DominanceType int

const (
	// Complete dominance means the dominant gene will be expressed fully and the recessive gene not expressed at all
	Complete DominanceType = iota

	// Incomplete dominance means traits blend (eg white petunia + blue petunia = light blue petunia)
	Incomplete DominanceType = iota

	// Co dominance means two dominant traits are expressed at the same time but remain discrete (eg blotchy cow)
	Co DominanceType = iota
)
