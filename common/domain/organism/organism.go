package organism

// Organism represents a single organism
type Organism interface {
	Initializer
	Reader
}

// Initializer initializes an organism
type Initializer interface {
	InitOrganism() error
}

// Reader retrieves properties of an organism
type Reader interface {
	IsMature() bool
	Genes() map[string]Gene
}
