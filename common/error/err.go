package error

import (
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common"
)

func HandleErr(err error, note string) {
	if err == nil {
		return
	}
	if common.Log == nil {
		ie := common.InitLog("")
		if ie != nil {
			fmt.Println(ie)
		}
	}
	common.Log.With(err).Error(note)
}
