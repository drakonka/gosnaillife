package http

import (
	"io/ioutil"
	"net/http"
	"strings"
)

type HttpStatus struct {
	Message    string `json:"message"`
	StatusCode int    `json:"status_code"`
}

func PostAndGetResBody(url, spec string) (body []byte, err error) {
	payload := strings.NewReader(spec)

	req, err := http.NewRequest("POST", url, payload)
	req.Close = true
	if err != nil {
		return body, err
	}
	req.Header.Add("content-type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return body, err
	}
	defer res.Body.Close()

	body, err = ioutil.ReadAll(res.Body)
	return body, err
}

func PostAndGetRes(url, spec string) (res *http.Response, err error) {
	payload := strings.NewReader(spec)

	req, err := http.NewRequest("POST", url, payload)
	req.Close = true
	if err != nil {
		return res, err
	}
	req.Header.Add("content-type", "application/json")
	res, err = http.DefaultClient.Do(req)
	return res, err
}

func PostAuthenticatedAndGetRes(url, spec, ap string, authHeaders map[string]string) (res *http.Response, err error) {
	payload := strings.NewReader(spec)

	req, err := http.NewRequest("POST", url, payload)
	req.Close = true
	if err != nil {
		return res, err
	}
	req.Header.Add("auth_provider", ap)

	for k, v := range authHeaders {
		req.Header.Add(k, v)
	}
	req.Header.Add("content-type", "application/json")
	res, err = http.DefaultClient.Do(req)
	return res, err
}

func GetAndGetResBody(url string) (body []byte, err error) {
	res, err := http.Get(url)
	if err != nil {
		return body, err
	}
	defer res.Body.Close()
	body, err = ioutil.ReadAll(res.Body)
	return body, err
}

func GetAuthenticatedAndGetRes(url, ap string, authHeaders map[string]string) (res *http.Response, err error) {
	req, _ := http.NewRequest("GET", url, nil)
	req.Close = true
	req.Header.Add("auth_provider", ap)
	for k, v := range authHeaders {
		req.Header.Add(k, v)
	}
	res, err = http.DefaultClient.Do(req)
	return res, err
}

func Delete(url, authHeader string) (body []byte, err error) {
	req, err := http.NewRequest("DELETE", url, nil)
	req.Close = true
	if err != nil {
		return body, err
	}

	req.Header.Set("Authorization", authHeader)
	req.Header.Add("content-type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return body, err
	}
	defer res.Body.Close()

	body, err = ioutil.ReadAll(res.Body)
	return body, err
}
