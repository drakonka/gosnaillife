package testing

import (
	"bufio"
	"os"
)

type FileScanBundle struct {
	Scanner *bufio.Scanner
	File    *os.File
}
