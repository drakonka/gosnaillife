package testing

import (
	"fmt"
)

func TCFailedMsg(testName, tcName string, context interface{}) string {
	return fmt.Sprintf("\nTest case %s (%s) failed - %v", tcName, testName, context)
}
