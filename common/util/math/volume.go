package math

import "math"

func CmToL(w, h, d float64) float64 {
	cm3 := w * h * d
	return Round(cm3/1000, 0.006)
}

func Round(x, unit float64) float64 {
	r := math.Ceil(x*1000) / 1000
	return r
}

type VolumeF64 struct {
	W     float64
	H     float64
	D     float64
	Units string
}

type VolumeSmallInt struct {
	W     uint16
	H     uint16
	D     uint16
	Units string
}
