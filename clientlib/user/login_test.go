package user

import (
	"encoding/json"
	"fmt"
	"gitlab.com/drakonka/gosnaillife/common/restapi"
	testing2 "gitlab.com/drakonka/gosnaillife/common/util/testing"
	"testing"
)

func TestPrepUserReq(t *testing.T) {
	testCases := []struct {
		name               string
		want               restapi.UserReq
		wantedAuthProvider string
	}{
		{"PrepRequest",
			restapi.UserReq{Username: "test@test.com", Password: "testpass", Connection: "Username-Password-Authentication"},
			"mysql",
		},
	}

	for _, tc := range testCases {
		fmt.Printf("Running ClientLibPrepUserReq Test Case %s.", tc.name)
		t.Run(fmt.Sprintf("%s", tc.name), func(t *testing.T) {
			tc.want.AuthProvider = tc.wantedAuthProvider
			req, err := prepUserReq("mysql", tc.want.Username, tc.want.Password)
			if err != nil {
				msg := testing2.TCFailedMsg("PrepUserReq", tc.name, err)
				t.Errorf(msg)
				return
			}
			wanted, err := json.Marshal(tc.want)
			if err != nil {
				msg := testing2.TCFailedMsg("PrepUserReq", tc.name, err)
				t.Errorf(msg)
				return
			}
			if req != string(wanted) {
				testing2.TCFailedMsg("PrepUserReq", tc.name, fmt.Sprintf("Expected %s, got %s", wanted, req))
			}
		})
	}
}
