package user

import (
	"gitlab.com/drakonka/gosnaillife/common/util/http"
	http2 "net/http"
)

func Authenticate(serverUrl, authProvider string, authHeaders map[string]string) (res *http2.Response, err error) {
	url := serverUrl + "/api/session/status"
	return http.GetAuthenticatedAndGetRes(url, authProvider, authHeaders)
}
