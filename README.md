# SnailLife Go

An experimental rewrite of the PHP version of a snail simulation to learn Go.

[![pipeline status](https://gitlab.com/drakonka/gosnaillife/badges/master/pipeline.svg)](https://gitlab.com/drakonka/gosnaillife/commits/master)

[![coverage report](https://gitlab.com/drakonka/gosnaillife/badges/master/coverage.svg)](https://gitlab.com/drakonka/gosnaillife/commits/master)